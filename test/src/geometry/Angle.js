QUnit.module( "Angle" )

QUnit.test( "default constructor", function( assert )
{
    var an = new SVGRX.Angle()
    // console.log('an', an)
    // console.log('an', "" + an)

    assert.equal( an.angle, 0, "default angle should be 0" )

    an = new SVGRX.Angle('4')
    assert.equal( an.angle, 0, "default angle should be 0 for unsupported parameter types" )
})

QUnit.test( "constructor accepts number as parameter", function( assert )
{
    var an = new SVGRX.Angle( 75.5 )
    
    assert.equal( an.angle, 75.5, "angle should be set from parameter if it's number" )
})

QUnit.test( "constructor accepts Point as parameter", function( assert )
{
    // 45deg angle from point vector
    var an = new SVGRX.Angle( new SVGRX.Point( 1, 1 ) )
    
    assert.almostEqual( an.angle, 45, "angle should be set from parameter if it's Point" )
})

QUnit.test( "default constructor parameter", function( assert )
{
    var an = new SVGRX.Angle()
    
    assert.almostEqual( an.angle, 0, "default angle should be 0" )
})

QUnit.test( "sin/cos", function( assert )
{
    let angle_of_60_degrees = Math.PI / 3
    var an = new SVGRX.Angle( 60 )
    
    assert.almostEqual( an.cos, Math.cos( angle_of_60_degrees ), "'cos' property should be set to angle cosine" )
    assert.almostEqual( an.sin, Math.sin( angle_of_60_degrees ), "'sin' property should be set to angle sine" )
})

QUnit.test( "angle property", function( assert )
{
    let angle_of_60_degrees = Math.PI / 3
    var an = new SVGRX.Angle()
    an.angle = 60
    
    assert.almostEqual( an.angle, 60, "angle property should be set from assignment" )
    assert.almostEqual( an.cos, Math.cos( angle_of_60_degrees ), "'cos' property should be set to angle cosine" )
    assert.almostEqual( an.sin, Math.sin( angle_of_60_degrees ), "'sin' property should be set to angle sine" )
})

QUnit.test( "negating angle", function( assert )
{
    let angle_of_60_degrees = Math.PI / 3
    var an = new SVGRX.Angle( 60 )

    var negAngle = an.neg()
    
    assert.almostEqual( negAngle.angle, -an.angle, "negating angle should give 180 deg bigger angle" )
    assert.almostEqual( negAngle.cos, an.cos, "'cos' property should be set to angle cosine" )
    assert.almostEqual( negAngle.sin, -an.sin, "'sin' property should be set to negative angle sin" )
})

QUnit.test( "360 angle", function( assert )
{
    let angle_of_360_degrees = 360
    let an

    an = new SVGRX.Angle( angle_of_360_degrees )
    assert.equal( an.angle, angle_of_360_degrees, "+360 angle should be kept as +360" )

    an = new SVGRX.Angle( 2 * angle_of_360_degrees )
    assert.equal( an.angle, 0, "multiples of +360 angle should be interpreted as 0" )

    an = new SVGRX.Angle( -angle_of_360_degrees )
    assert.equal( an.angle, -angle_of_360_degrees, "-360 angle should be kept as -360" )

    an = new SVGRX.Angle( 2 * (-angle_of_360_degrees) )
    assert.equal( an.angle, 0, "multiples of -360 angle should be interpreted as 0" )
})
