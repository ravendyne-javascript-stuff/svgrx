QUnit.module( "Bezier2" )


QUnit.test( "toString", function( assert )
{
    var bez

    bez = new SVGRX.Bezier2()
    assert.equal("" + bez,
        "Bezier L : (0.000000, 0.000000), (0.000000, 0.000000)",
        "default bezier2 tostring")

    bez = new SVGRX.Bezier2([ SVGRX.Point( 1, 2 ), SVGRX.Point( 3, 4 ) ])
    assert.equal("" + bez,
        "Bezier L : (1.000000, 2.000000), (3.000000, 4.000000)",
        "bezier2 tostring")
})

QUnit.test( "trasformations", function( assert )
{
    var p0 = new SVGRX.Point(1,1)
    var p1 = new SVGRX.Point(2,2)

    var matrix = new SVGRX.Matrix([ 1, 2, 3, 4, 5, 6 ])

    var bez = new SVGRX.Bezier2( [ p0, p1 ] )
    var transformed = bez.transform( matrix )
    var expected = [ matrix.mul( p0 ), matrix.mul( p1 ) ]
    assert.deepEqual( transformed.points, SVGRX.pointArrayToNumeric( expected ), "transform all points using given matrix and return new Bezier2")
    assert.deepEqual( bez.points, SVGRX.pointArrayToNumeric( [ p0, p1 ] ), "original Bezier2 should be unchanged")
})
