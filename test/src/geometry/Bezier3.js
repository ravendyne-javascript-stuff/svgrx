QUnit.module( "Bezier3" )


QUnit.test( "toString", function( assert )
{
    var bez

    bez = new SVGRX.Bezier3()
    assert.equal("" + bez,
        "Bezier Q : (0.000000, 0.000000), (0.000000, 0.000000), (0.000000, 0.000000)",
        "default bezier3 tostring")

    bez = new SVGRX.Bezier3([ SVGRX.Point( 1, 2 ), SVGRX.Point( 3, 4 ), SVGRX.Point( 5, 6 ) ])
    assert.equal("" + bez,
        "Bezier Q : (1.000000, 2.000000), (3.000000, 4.000000), (5.000000, 6.000000)",
        "bezier3 tostring")
})

QUnit.test( "trasformations", function( assert )
{
    var p0 = new SVGRX.Point(1,1)
    var p1 = new SVGRX.Point(2,2)
    var p2 = new SVGRX.Point(3,3)

    var matrix = new SVGRX.Matrix([ 1, 2, 3, 4, 5, 6 ])

    var bez = new SVGRX.Bezier3( [ p0, p1, p2 ] )
    var transformed = bez.transform( matrix )
    var expected = [ matrix.mul( p0 ), matrix.mul( p1 ), matrix.mul( p2 ) ]
    assert.deepEqual( transformed.points, SVGRX.pointArrayToNumeric( expected ), "transform all points using given matrix and return new Bezier3")
    assert.deepEqual( bez.points, SVGRX.pointArrayToNumeric( [ p0, p1, p2 ] ), "original Bezier3 should be unchanged")
})
