QUnit.module( "Point" )

QUnit.test( "constructor accepts two parameters: X, Y", function( assert )
{
    var pt = new SVGRX.Point( 10, 20 )
    // console.log('pt', pt)
    // console.log('pt', "" + pt)
    
    assert.equal( pt.x, 10, "X should be set from first parameter if it's number" )
    assert.equal( pt.y, 20, "y should be set from second parameter if it's number" )
})

QUnit.test( "constructor accepts array of length 2: [X, Y]", function( assert )
{
    var pt = new SVGRX.Point( [20, 10] )
    
    assert.equal( pt.x, 20, "X should be set from first element of array parameter, if it's number" )
    assert.equal( pt.y, 10, "y should be set from second element of array parameter, if it's number" )
})

QUnit.test( "constructor accepts array of length 2: [X, Y]", function( assert )
{
    var other = new SVGRX.Point(30, 40)
    var pt = new SVGRX.Point( other )
    
    assert.equal( pt.x, other.x, "X should be set from X of Point parameter" )
    assert.equal( pt.y, other.y, "y should be set from Y of Point parameter" )
})

QUnit.test( "Invalid parameters default to (0, 0) point", function( assert )
{
    var zeroPoint = new SVGRX.Point(0, 0)
    
    assert.propEqual( new SVGRX.Point('foo'), zeroPoint, "parameters should be numbers" )
    assert.propEqual( new SVGRX.Point([1, 2, 3]), zeroPoint, "array parameter should have 2 elements" )
    assert.propEqual( new SVGRX.Point(['1', 2]), zeroPoint, "array parameter elements should be numbers" )
})

QUnit.test( "point with no arguments defaults to (0, 0)", function( assert )
{
    var pt = new SVGRX.Point()
    var expected = new SVGRX.Point(0, 0)

    assert.equal( pt.x, 0, "default X should be 0" )
    assert.equal( pt.y, 0, "default Y should be 0" )
})

QUnit.test( "precision", function( assert )
{
    assert.equal( SVGRX.Point.precision, 6, "precision should be set to 6 by default" )
    SVGRX.Point.precision = 3
    assert.equal( SVGRX.Point.precision, 3, "precision should be set to passed in value if it is a number" )
    SVGRX.Point.precision = '17'
    assert.equal( SVGRX.Point.precision, 3, "precision should NOT be set to passed in value if the value is NOT a number" )

    // reset to default value
    SVGRX.Point.precision = 6
})

QUnit.test( "toString", function( assert )
{
    var pt = new SVGRX.Point( 12.45, 33.33 )

    SVGRX.Point.precision = 6
    assert.equal( "" + pt, "(12.450000, 33.330000)", "precision 6 should output 6 decimals for (X, Y)" )

    SVGRX.Point.precision = 3
    assert.equal( "" + pt, "(12.450, 33.330)", "precision 3 should output 3 decimals for (X, Y)" )

    // reset to default value
    SVGRX.Point.precision = 6
})

QUnit.test( "coord", function( assert )
{
    var pt = new SVGRX.Point( 12, 13 )

    // deepEqual for arrays
    assert.deepEqual(pt.coord(), [12, 13], "coord should return [x, y] array")
})

QUnit.test( "length", function( assert )
{
    var pt = new SVGRX.Point( 1, 1 )
    var expected = Math.sqrt( 2.0 )

    assert.almostEqual(pt.length(), expected, "length shold be point distance from (0, 0)")
})

QUnit.test( "angle", function( assert )
{
    let pt

    pt = new SVGRX.Point( 1, 0 )
    assert.almostEqual(pt.angle(), 0, "0 degrees angle")

    pt = new SVGRX.Point( 1, 1 )
    assert.almostEqual(pt.angle(), 45, "45 degrees angle")

    pt = new SVGRX.Point( 0, 1 )
    assert.almostEqual(pt.angle(), 90, "90 degrees angle")

    pt = new SVGRX.Point( -1, 1 )
    assert.almostEqual(pt.angle(), 135, "135 degrees angle")

    pt = new SVGRX.Point( -1, 0 )
    assert.almostEqual(pt.angle(), 180, "180 degrees angle")

    pt = new SVGRX.Point( -1, -1 )
    assert.almostEqual(pt.angle(), -135, "-135 degrees angle")

    pt = new SVGRX.Point( 0, -1 )
    assert.almostEqual(pt.angle(), -90, "-90 degrees angle")

    pt = new SVGRX.Point( 1, -1 )
    assert.almostEqual(pt.angle(), -45, "-45 degrees angle")
})

QUnit.test( "rotate", function( assert )
{
    var pt = new SVGRX.Point( 1, 1 )
    var angle = 45
    
    rotated = pt.rotate( angle )

    result = pt.angle( rotated )

    assert.almostEqual(result, angle, "point should be rotated by angle given as parameter")
})

QUnit.test( "math", function( assert )
{
    var ptLeft = new SVGRX.Point( 1, 1 )
    var ptRight = new SVGRX.Point( 1, 1 )
    var result

    result = ptLeft.add( ptRight )
    assert.deepEqual( result, new SVGRX.Point( 2, 2 ), "add should add x,y coords" )

    result = ptLeft.sub( ptRight )
    assert.deepEqual( result, new SVGRX.Point( 0, 0 ), "sub should subtract x,y coords" )

    result = ptLeft.mul( 5 )
    assert.deepEqual( result, new SVGRX.Point( 5, 5 ), "mul should multiply x,y coords with number" )

    result = ptLeft.mul( '5' )
    assert.deepEqual( result, ptLeft, "mul should only accept number as parameter" )

    result = ptLeft.equals( ptRight )
    assert.ok( result, "equals should accept points with same coords" )

    result = ptLeft.equals( new SVGRX.Point( 9, 9 ) )
    assert.ok( ! result, "equals should reject points with different coords" )
})
