QUnit.module( "Matrix" )

QUnit.test( "default constructor returns unity matrix", function( assert )
{
    var mat = new SVGRX.Matrix()
    
    assert.deepEqual( mat.vect, [1, 0, 0, 1, 0, 0], "default matrix for parameterless constructor should be unity" )
})

QUnit.test( "constructor accepts 6-item array", function( assert )
{
    var unity = [1, 0, 0, 1, 0, 0]
    var param = [ 1, 2, 3, 4, 5, 6 ]

    var mat = new SVGRX.Matrix( param )
    assert.deepEqual( mat.vect, param, "constructor should set matrix parameter vector from passed in value" )

    mat = new SVGRX.Matrix( [ '1', 2, 3, 4, 5, 6 ] )
    assert.deepEqual( mat.vect, unity, "constructor should only accept arrays with numeric elements" )

    mat = new SVGRX.Matrix( [ 1, 2, 3, 4, 5, 6, 7 ] )
    assert.deepEqual( mat.vect, unity, "constructor should only accept arrays with 6 elements" )
})

QUnit.test( "toString", function( assert )
{
    var mat = new SVGRX.Matrix()

    assert.equal( "" + mat, "[1,0,0,1,0,0]", "string representation of matrix should be its vector array")
})

QUnit.test( "mul", function( assert )
{
    // test subject values
    var matVect = [ 6, 5, 4, 3, 2, 1 ]
    // others
    var otherMatrix = [ 1, 2, 3, 4, 5, 6 ]
    var otherPoint = [ 7, 8 ]
    // expected results
    // https://www.w3.org/TR/SVG/coords.html#TransformMatrixDefined
    var otherPointResult = new SVGRX.Point( [
        otherPoint[0] * matVect[0] + otherPoint[1] * matVect[2] + matVect[4],
        otherPoint[0] * matVect[1] + otherPoint[1] * matVect[3] + matVect[5],
    ] )
    // https://www.w3.org/TR/SVG/coords.html#NestedTransformations
    var otherMatrixResult = new SVGRX.Matrix( [
        matVect[0] * otherMatrix[0] + matVect[2] * otherMatrix[1],
        matVect[1] * otherMatrix[0] + matVect[3] * otherMatrix[1],
        matVect[0] * otherMatrix[2] + matVect[2] * otherMatrix[3],
        matVect[1] * otherMatrix[2] + matVect[3] * otherMatrix[3],
        matVect[0] * otherMatrix[4] + matVect[2] * otherMatrix[5]  + matVect[4],
        matVect[1] * otherMatrix[4] + matVect[3] * otherMatrix[5]  + matVect[5],
    ] )

    // test subject
    var mat = new SVGRX.Matrix( matVect )

    var result = mat.mul( new SVGRX.Point( otherPoint ) )
    assert.deepEqual( result, otherPointResult, "mul by point should return point with correct values")

    result = mat.mul( new SVGRX.Matrix( otherMatrix ) )
    assert.deepEqual( result.vect, otherMatrixResult.vect, "mul by matrix should return matrix with correct values")
})

// QUnit.test( "xlength/ylength", function( assert )
// {
//     var result
//     var sx = 2, sy = 3
//     // scaling matrix with ohter coefficients set so that they produce erroroneous calculation
//     var mat = new SVGRX.Matrix( [ sx, -1, -1, sy, -1, -1 ] )

//     result = mat.xlength( 10 )
//     assert.equal( result, 20, "xlength should return value scaled by X-axis")

//     result = mat.ylength( 10 )
//     assert.equal( result, 30, "xlength should return value scaled by X-axis")
// })
