QUnit.module( "Arc" )

QUnit.test( "default constructor", function( assert )
{
    let arc = new SVGRX.Arc()

    assert.deepEqual( [ arc.center.x, arc.center.y ], [ 0, 0 ], "default center should be at origin" )
    assert.deepEqual( [ arc.rx, arc.ry ], [ 1, 1 ], "default radius should be unit circle" )
    assert.deepEqual( [ arc.start.angle, arc.delta.angle ], [ 0, 90 ], "default arc should be quarter circle starting at 0 degrees" )
    assert.equal( arc.xAxisRotation.angle, 0, "default rotation to x-axis should be 0" )
})

QUnit.test( "constructor", function( assert )
{
    let arc
    let defaultArcValues = [ 0, 0, 1, 1, 0, 90, 0 ]

    arc = new SVGRX.Arc( 1, 2, 3, 4 )
    assert.deepEqual( arc.params, defaultArcValues, "constructor should use default values if parameter is not an array" )

    arc = new SVGRX.Arc([ '1', '2', '3' ])
    assert.deepEqual( arc.params, defaultArcValues, "constructor should use default values if parameter is not a numeric array" )

    arc = new SVGRX.Arc([ 1, 2, 3, 4, 5, 6 ])
    assert.deepEqual( arc.params, defaultArcValues, "constructor should use default values if parameter has less than 7 elements" )

    arc = new SVGRX.Arc([ 10, 10, 5, 7, 15, 135, 20 ])
    assert.deepEqual( [
        arc.center.x, arc.center.y,
        arc.rx, arc.ry,
        arc.start.angle, arc.delta.angle,
        arc.xAxisRotation.angle
    ], [ 10, 10, 5, 7, 15, 135, 20 ], "constructor should accept array parameter which has 7 elements or more" )
})

QUnit.test( "constructor", function( assert )
{
    let x = 10, y = 20
    let rx = 5, ry = 7
    let startAngle = 20, deltaAngle = 150
    let xAxisAngle = 35

    let arc = new SVGRX.Arc([ x, y, rx, ry, startAngle, deltaAngle, xAxisAngle ])

    assert.deepEqual( [
        arc.center.x, arc.center.y,
        arc.rx, arc.ry,
        arc.start.angle, arc.delta.angle,
        arc.xAxisRotation.angle
    ], [ x, y, rx, ry, startAngle, deltaAngle, xAxisAngle ], "parameter order should be: x, y, rx, ry, start angle, delta angle, x-axis angle" )
})

QUnit.test( "convert", function( assert )
{
    let arc = new SVGRX.Arc()

    let converted = arc.convert()

    assert.isArrayOfBezier( converted, "convert() should return an array of Bezier elements" )
})
