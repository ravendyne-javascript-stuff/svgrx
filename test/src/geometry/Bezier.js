QUnit.module( "Bezier" )

QUnit.test( "default constructor", function( assert )
{
    var bez = new SVGRX.Bezier()
    // console.log('bez', bez)
    // console.log('bez', "" + bez)

    assert.deepEqual(bez.points, [], "default points is empty array")
    assert.equal(bez.order, 0, "default order is 0")
})

QUnit.test( "constructors", function( assert )
{
    // bezier with default values
    var zeroBezier = new SVGRX.Bezier()
    var bez

    bez = new SVGRX.Bezier( 1, 3, 5 )
    assert.deepEqual(bez, zeroBezier, "constructor rejects non-array parameters")

    bez = new SVGRX.Bezier([ 1, 3, 5 ])
    assert.deepEqual(bez, zeroBezier, "constructor rejects non-Point arrays")

    bez = new SVGRX.Bezier([ SVGRX.Point() ])
    assert.deepEqual(bez, zeroBezier, "constructor rejects 1-element array parameters")

    var pointArray2 = [ SVGRX.Point(), SVGRX.Point() ]
    bez = new SVGRX.Bezier( pointArray2 )
    assert.deepEqual(bez.order, 2, "constructor accepts 2-element array parameters")

    var pointArray3 = [ SVGRX.Point(), SVGRX.Point(), SVGRX.Point() ]
    bez = new SVGRX.Bezier( pointArray3 )
    assert.deepEqual(bez.points, SVGRX.pointArrayToNumeric( pointArray3 ), "constructor accepts 3-element array parameters")
    assert.equal(bez.order, 3, "constructor accepts 3-element array parameters")

    var pointArray4 = [ SVGRX.Point(), SVGRX.Point(), SVGRX.Point(), SVGRX.Point() ]
    bez = new SVGRX.Bezier( pointArray4 )
    assert.deepEqual(bez.points, SVGRX.pointArrayToNumeric( pointArray4 ), "constructor accepts 4-element array parameters")
    assert.equal(bez.order, 4, "constructor accepts 4-element array parameters")

    bez = new SVGRX.Bezier([ SVGRX.Point(), SVGRX.Point(), SVGRX.Point(), SVGRX.Point(), SVGRX.Point() ])
    assert.deepEqual(bez, zeroBezier, "constructor rejects 5-element array parameters")
})

QUnit.test( "toString", function( assert )
{
    var bez

    bez = new SVGRX.Bezier()
    assert.equal("" + bez, "Bezier0 : ", "default bezier tostring")

    bez = new SVGRX.Bezier([ SVGRX.Point(), SVGRX.Point() ])
    assert.equal("" + bez,
        "Bezier2 : (0.000000, 0.000000), (0.000000, 0.000000)",
        "linear bezier tostring")

    bez = new SVGRX.Bezier([ SVGRX.Point(), SVGRX.Point(), SVGRX.Point() ])
    assert.equal("" + bez,
        "Bezier3 : (0.000000, 0.000000), (0.000000, 0.000000), (0.000000, 0.000000)",
        "quadratic bezier tostring")

    bez = new SVGRX.Bezier([ SVGRX.Point(), SVGRX.Point(), SVGRX.Point(), SVGRX.Point() ])
    assert.equal("" + bez,
        "Bezier4 : (0.000000, 0.000000), (0.000000, 0.000000), (0.000000, 0.000000), (0.000000, 0.000000)",
        "cubic bezier tostring")
})

QUnit.test( "cpoints", function( assert )
{
    var controlPoints = [ SVGRX.Point(1,1), SVGRX.Point(2,2), SVGRX.Point(3,3) ]
    var bez = new SVGRX.Bezier( controlPoints )
    var result

    result = bez.cpoints[ 5 ]
    assert.equal( result, undefined, "returns undefined for index out of bounds")

    result = bez.cpoints[ 0 ]
    assert.deepEqual( result, controlPoints[1], "returns control point for given index ")
})
