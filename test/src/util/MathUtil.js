QUnit.module( "MathUtil" )

QUnit.test( "min/max X/Y", function( assert )
{
    var array = [ new SVGRX.Point( 10, 1 ), new SVGRX.Point( -2, 0 ), new SVGRX.Point( 3, -5 ), new SVGRX.Point( 1, 1 ) ]
    var result

    result = SVGRX.minX(  )
    assert.equal( result, 0, "min x of empty array is 0" )
    result = SVGRX.minY(  )
    assert.equal( result, 0, "min y of empty array is 0" )
    result = SVGRX.maxX(  )
    assert.equal( result, 0, "max x of empty array is 0" )
    result = SVGRX.maxY(  )
    assert.equal( result, 0, "max y of empty array is 0" )

    result = SVGRX.minX( array )
    assert.equal( result, -2, "min x should return smallest x" )
    result = SVGRX.minY( array )
    assert.equal( result, -5, "min y should return smallest y" )
    result = SVGRX.maxX( array )
    assert.equal( result, 10, "max x should return biggest x" )
    result = SVGRX.maxY( array )
    assert.equal( result, 1, "max y should return biggest y" )
})

QUnit.test( "roundToPrecision", function( assert )
{
    let precision = SVGRX.Point.precision
    
    SVGRX.Point.precision = 6

    assert.equal( SVGRX.roundToPrecision( 0.001 ), 0.001, "small values" )
    assert.equal( SVGRX.roundToPrecision( 0.0000001 ), 0, "epsilon values" )
    assert.equal( SVGRX.roundToPrecision( -0.0000001 ), 0, "negative epsilon values" )
    assert.equal( SVGRX.roundToPrecision( -0.001 ), -0.001, "negative small values" )
    assert.equal( SVGRX.roundToPrecision( 0.123456789012345 ), 0.123456789012, "rounding should be to twice the number of decimals specified by Point.precision" )

    SVGRX.Point.precision = precision
})
