QUnit.module( "GeneralUtil" )

QUnit.test( "GUID", function( assert )
{
    var guidFormatRegEx = /[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/g
    var guid = SVGRX.GUID()

    assert.equal( guid.length, 36, "guid string should be 36 chars long" )

    var match = guidFormatRegEx.exec( guid )
    assert.equal( match[0], guid, "must match xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx pattern" )

    var parts = guid.split('-')
    var part0 = parseInt( parts[0], 16 )
    var part1 = parseInt( parts[1], 16 )
    var part2 = parseInt( parts[2], 16 )
    var part3 = parseInt( parts[3], 16 )
    var part4 = parseInt( parts[4], 16 )
    assert.equal( part2 & 0xC000, 0x4000, "must have version field set to v4" )
    assert.equal( part3 & 0xC000, 0x8000, "must have variant field set to RFC4122" )
})
