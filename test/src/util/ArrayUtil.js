QUnit.module( "ArrayUtil" )

QUnit.test( "isNumericArray", function( assert )
{
    assert.ok( SVGRX.isNumericArray([  ]), "empty array is numeric" )
    assert.ok( ! SVGRX.isNumericArray([ 'a', 'b' ]), "array of strings is not numeric" )
    assert.ok( ! SVGRX.isNumericArray([ 1, 'b' ]), "mixed array is not numeric" )

    assert.ok( SVGRX.isNumericArray([ 1, 2, 3 ]), "array of primitive ints is numeric" )
    assert.ok( SVGRX.isNumericArray([ 1.01, 2.01, 3.05 ]), "array of primitive floats is numeric" )
    assert.ok( SVGRX.isNumericArray([ 1, 2.01, 3 ]), "array of primitive ints and floats is numeric" )
    assert.ok( SVGRX.isNumericArray([ Number(1), Number(2.01), Number(3) ]), "array of Number objects is numeric" )
    assert.ok( SVGRX.isNumericArray([ new Number(1), new Number(2.01), new Number(3) ]), "array of Number objects is numeric" )
})

QUnit.test( "isBaseElementArray", function( assert )
{
    assert.ok( SVGRX.isBaseElementArray([  ]), "empty array is an aray of BaseElement" )
    assert.ok( ! SVGRX.isBaseElementArray([ 1, '3' ]), "non-empty array with no BaseElement is not an aray of BaseElement" )
    assert.ok( ! SVGRX.isBaseElementArray([ SVGRX.BaseElement(), 'b' ]), "mixed array is not an aray of BaseElement" )

    assert.ok( SVGRX.isBaseElementArray([ SVGRX.BaseElement(), SVGRX.BaseElement() ]), "array of BaseElement objects is an an aray of BaseElement" )
    assert.ok( SVGRX.isBaseElementArray([ new SVGRX.BaseElement(), new SVGRX.BaseElement() ]), "array of BaseElement objects as functions is an aray of BaseElement" )
})

QUnit.test( "isPointArray", function( assert )
{
    assert.ok( SVGRX.isPointArray([  ]), "empty array is an aray of Points" )
    assert.ok( ! SVGRX.isPointArray([ 1, '3' ]), "non-empty array with no Points is not an aray of Points" )
    assert.ok( ! SVGRX.isPointArray([ SVGRX.Point(), 'b' ]), "mixed array is not an aray of Points" )

    assert.ok( SVGRX.isPointArray([ SVGRX.Point(), SVGRX.Point() ]), "array of Point objects is an an aray of Points" )
    assert.ok( SVGRX.isPointArray([ new SVGRX.Point(), new SVGRX.Point() ]), "array of Point objects as functions is an aray of Points" )
})

QUnit.test( "isTransformableElementArray", function( assert )
{
    assert.ok( SVGRX.isTransformableElementArray([  ]), "empty array is an aray of TransformableElement" )
    assert.ok( ! SVGRX.isTransformableElementArray([ 1, '3' ]), "non-empty array with no TransformableElement is not an aray of TransformableElement" )
    assert.ok( ! SVGRX.isTransformableElementArray([ SVGRX.TransformableElement(), 'b' ]), "mixed array is not an aray of TransformableElement" )

    assert.ok( SVGRX.isTransformableElementArray([ SVGRX.TransformableElement(), SVGRX.TransformableElement() ]), "array of TransformableElement objects is an an aray of TransformableElement" )
    assert.ok( SVGRX.isTransformableElementArray([ new SVGRX.TransformableElement(), new SVGRX.TransformableElement() ]), "array of TransformableElement objects as functions is an aray of TransformableElement" )
})

QUnit.test( "flattenArray", function( assert )
{
    assert.deepEqual( SVGRX.flattenArray([ ]), [ ], "empty array" )
    assert.deepEqual( SVGRX.flattenArray([ [ ] ]), [ ], "array of empty array" )
    assert.deepEqual( SVGRX.flattenArray([ 1 ]), [ 1 ], "one element array" )
    assert.deepEqual( SVGRX.flattenArray([ 2, 3 ]), [ 2, 3 ], "two element array" )
    assert.deepEqual( SVGRX.flattenArray([ 1, [ ] ]), [ 1 ], "empty array element disappears" )
    assert.deepEqual( SVGRX.flattenArray([ 1, [ 2 ] ]), [ 1, 2 ], "array element is flattened" )
    assert.deepEqual( SVGRX.flattenArray([ 1, [ [ 2 ], 3 ] ]), [ 1, 2, 3 ], "array in array element is flattened" )
})
