QUnit.module( "TransformableElement" )

QUnit.test( "constructor", function( assert )
{
    let tr

    tr = new SVGRX.TransformableElement()
    assert.ok( tr instanceof SVGRX.TransformableElement, "TransformableElement can be created with new" )

    tr = SVGRX.TransformableElement()
    assert.ok( tr instanceof SVGRX.TransformableElement, "TransformableElement can be created with function call" )
})

QUnit.test( "default constructor", function( assert )
{
    var tr = new SVGRX.TransformableElement()

    assert.equal( tr.transform, '', "default transform should be empty" )
})

QUnit.test( "attributes constructor", function( assert )
{
    var attributes = {
        transform: 'scale(2)',
        attrA: 'some attribute',
        attrB: 37,
    }
    var tr = new SVGRX.TransformableElement( attributes )

    assert.equal( tr.transform, attributes.transform, "object transform should be set from attribute 'transform'" )
    assert.deepEqual( tr.attrs, attributes, "object attributes should be set from parameter" )
})

QUnit.test( "object constructor", function( assert )
{
    var defaulValues = new SVGRX.TransformableElement()
    var attributes = {
        transform: 'skewX(0.5)',
        attrA: 'some attribute',
        attrB: 37,
    }
    var expected = new SVGRX.TransformableElement( attributes )
    var tr

    tr = new SVGRX.TransformableElement( expected )
    assert.deepEqual( tr, expected, "object should be initialized to the same values as passed object parameter" )

    tr = new SVGRX.TransformableElement( new Number( 890 ) )
    assert.deepEqual( tr, defaulValues, "constructor should use default values if parameter is not TransformableElement or object" )
})

QUnit.test( "single transforms", function( assert )
{
    let tr

    // matrix
    tr = new SVGRX.TransformableElement( { transform: 'matrix( 1, 2, 3, 4, 5, 6 )' } )

    assert.equal( tr.transformData[ 0 ].op, 'matrix', "'matrix' should produce matrix operation" )
    assert.deepEqual( tr.transformData[ 0 ].args, [ 1, 2, 3, 4, 5, 6 ], "all 'matrix' arguments should be picek up in order" )
    assert.deepEqual( tr.matrix.vect, [ 1, 2, 3, 4, 5, 6 ], "transformation matrix should match passed in 'matrix' arguments" )


    // translate
    tr = new SVGRX.TransformableElement( { transform: 'translate( 10, 20 )' } )

    assert.equal( tr.transformData[ 0 ].op, 'translate', "'translate' should produce translate operation" )
    assert.deepEqual( tr.transformData[ 0 ].args, [ 10, 20 ], "all 'translate' arguments should be picek up in order" )
    assert.deepEqual( tr.matrix.vect, [ 1, 0, 0, 1, 10, 20 ], "transformation matrix should match translation style matrix with passed in 'translate' arguments" )


    // scale
    tr = new SVGRX.TransformableElement( { transform: 'scale( 10, 20 )' } )

    assert.equal( tr.transformData[ 0 ].op, 'scale', "'scale' should produce scale operation" )
    assert.deepEqual( tr.transformData[ 0 ].args, [ 10, 20 ], "all 'scale' arguments should be picek up in order" )
    assert.deepEqual( tr.matrix.vect, [ 10, 0, 0, 20, 0, 0 ], "transformation matrix should match scale style matrix with passed in 'scale' arguments" )

    // scale with one parameter
    tr = new SVGRX.TransformableElement( { transform: 'scale( 25 )' } )

    assert.deepEqual( tr.matrix.vect, [ 25, 0, 0, 25, 0, 0 ], "'scale' with one parameter should apply the same parameter value to both X and Y directions" )


    // rotate
    tr = new SVGRX.TransformableElement( { transform: 'rotate( -62 )' } )

    assert.equal( tr.transformData[ 0 ].op, 'rotate', "'rotate' should produce scale operation" )
    assert.deepEqual( tr.transformData[ 0 ].args, [ -62 ], "all 'rotate' arguments should be picek up in order" )
    let cosa = Math.cos( Math.radians( -62 ) )
    let sina = Math.sin( Math.radians( -62 ) )
    assert.deepEqual( tr.matrix.vect, [ cosa, sina, -sina, cosa, 0, 0 ], "transformation matrix should match rotate style matrix with passed in 'rotate' arguments" )


    // skewX
    tr = new SVGRX.TransformableElement( { transform: 'skewX( 22 )' } )

    assert.equal( tr.transformData[ 0 ].op, 'skewX', "'skewX' should produce scale operation" )
    assert.deepEqual( tr.transformData[ 0 ].args, [ 22 ], "all 'skewX' arguments should be picek up in order" )
    let tanx = Math.tan( Math.radians( 22 ) )
    assert.deepEqual( tr.matrix.vect, [ 1, 0, tanx, 1, 0, 0 ], "transformation matrix should match skewX style matrix with passed in 'skewX' arguments" )


    // skewY
    tr = new SVGRX.TransformableElement( { transform: 'skewY( 17 )' } )

    assert.equal( tr.transformData[ 0 ].op, 'skewY', "'skewY' should produce scale operation" )
    assert.deepEqual( tr.transformData[ 0 ].args, [ 17 ], "all 'skewY' arguments should be picek up in order" )
    let tany = Math.tan( Math.radians( 17 ) )
    assert.deepEqual( tr.matrix.vect, [ 1, tany, 0, 1, 0, 0 ], "transformation matrix should match skewY style matrix with passed in 'skewX' arguments" )
})

QUnit.test( "combining transforms", function( assert )
{
    // transform A
    let trA = new SVGRX.TransformableElement( { transform: 'translate( 10, 20 )' } )
    // transform B
    let trB = new SVGRX.TransformableElement( { transform: 'scale( 10 )' } )
    // transform C
    let trC = new SVGRX.TransformableElement( { transform: 'rotate( -15 )' } )
    // combining matrices from different transforms
    let mt = trA.matrix.mul( trB.matrix.mul( trC.matrix ) )


    // combined transform
    let trD = new SVGRX.TransformableElement( { transform: 'translate( 10, 20 ) scale( 10 ) rotate( -15 )' } )

    assert.ok( ( trD.transformData[ 0 ].op == 'translate' &&
        trD.transformData[ 1 ].op == 'scale' &&
        trD.transformData[ 2 ].op == 'rotate' ) , "combined transform operations should be picked up in the same order" )
    assert.deepEqual( trD.matrix.vect, mt.vect, "combined transform operations should produce transformation matrix that preserves order of operations" )
})
