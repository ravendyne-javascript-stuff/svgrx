QUnit.module( "BaseElement" )

QUnit.test( "constructor", function( assert )
{
    let el

    el = new SVGRX.BaseElement()
    assert.ok( el instanceof SVGRX.BaseElement, "BaseElement can be created with new" )

    el = SVGRX.BaseElement()
    assert.ok( el instanceof SVGRX.BaseElement, "BaseElement can be created with function call" )
})

QUnit.test( "default constructor", function( assert )
{
    var el = new SVGRX.BaseElement()

    assert.equal( el.class, '', "default class should be empty" )
    assert.equal( el.style, '', "default style should be empty" )
    assert.deepEqual( el.attrs, {}, "default attributes should be empty" )
    assert.equal( el.children.length, 0, "default children array should be empty" )
})

QUnit.test( "attributes constructor", function( assert )
{
    var attributes, el

    attributes = {
        id: 'element_4',
        class: 'my-class',
        style: 'my-style',
        attrA: 'some attribute',
        attrB: 37,
    }
    el = new SVGRX.BaseElement( attributes )

    assert.equal( el.id, attributes.id, "object id should be set from attribute 'id'" )
    assert.equal( el.class, attributes.class, "object class should be set from attribute 'class'" )
    assert.equal( el.style, attributes.style, "object style should be set from attribute 'style'" )
    assert.deepEqual( el.attrs, attributes, "object attributes should be set from parameter" )

    attributes = {
        attrA: 'some attribute',
        attrB: 37,
    }
    el = new SVGRX.BaseElement( attributes )

    assert.ok( el.id != '', "if attribute 'id' is not set, id should have default value" )
    assert.equal( el.class, '', "if attribute 'class' is not set, class should have default value" )
    assert.equal( el.style, '', "if attribute 'style' is not set, style should have default value" )
})

QUnit.test( "object constructor", function( assert )
{
    var defaulValues = new SVGRX.BaseElement()
    var attributes = {
        id: 'element_4',
        class: 'my-class',
        style: 'my-style',
        attrA: 'some attribute',
        attrB: 37,
    }
    var children = [ new SVGRX.BaseElement(), new SVGRX.BaseElement() ]
    var expected = new SVGRX.BaseElement( attributes )
    expected.children = children
    var el

    el = new SVGRX.BaseElement( expected )
    assert.deepEqual( el, expected, "object should be initialized to the same values as passed object parameter" )
    assert.deepEqual( el.children, children, "object's children should be initialized to the children from passed object parameter" )

    el = new SVGRX.BaseElement( new Number( 890 ) )
    assert.deepEqual( el, defaulValues, "constructor should use default values if parameter is not BaseElement or object" )
})

QUnit.test( "children attribute", function( assert )
{
    var arrayOfBaseElements = [ new SVGRX.BaseElement(), new SVGRX.BaseElement() ]
    var anArray = [ 3, new SVGRX.BaseElement(), 'test' ]
    var el

    el = new SVGRX.BaseElement()
    el.children = arrayOfBaseElements
    assert.deepEqual( el.children, arrayOfBaseElements, "should accept array of BaseElements" )

    el = new SVGRX.BaseElement()
    el.children = anArray
    assert.deepEqual( el.children, [], "should not accept array which has at least one element that is not BaseElement" )
})
