QUnit.module( "Svg" )

QUnit.test( "constructor", function( assert )
{
    let svg

    svg = new SVGRX.Svg()
    assert.ok( svg instanceof SVGRX.Svg, "Svg can be created with new" )

    svg = SVGRX.Svg()
    assert.ok( svg instanceof SVGRX.Svg, "Svg can be created with function call" )
})

QUnit.test( "default constructor", function( assert )
{
    let svg = new SVGRX.Svg()

    assert.ok( svg.x == 0, "'x' should be 0 by default" )
    assert.ok( svg.y == 0, "'y' should be 0 by default" )
    assert.ok( svg.width !== undefined && svg.width != 0, "'width' should have default value set" )
    assert.ok( svg.height !== undefined && svg.height != 0, "'height' should have default value set" )
    assert.ok( svg.version == '1.1', "'version' should be '1.1' by default" )
})

QUnit.test( "parameters", function( assert )
{
    let svg
    let svg_source = 'mockup svg source'
    let attributes = {
        x: 100,
        y: 200,
        width: 800,
        height: 600,
        version: '1.0',
    }

    svg = new SVGRX.Svg( {}, svg_source )
    assert.equal( svg.source, svg_source, "Second parameter should be set as 'source' property" )

    svg = new SVGRX.Svg( attributes )
    assert.equal( svg.x, attributes.x, "Should pick up 'x' from passed in attributes" )
    assert.equal( svg.y, attributes.y, "Should pick up 'y' from passed in attributes" )
    assert.equal( svg.width, attributes.width, "Should pick up 'width' from passed in attributes" )
    assert.equal( svg.height, attributes.height, "Should pick up 'height' from passed in attributes" )
    assert.equal( svg.version, attributes.version, "Should pick up 'version' from passed in attributes" )
})
