QUnit.module( "Group" )

QUnit.test( "constructor", function( assert )
{
    let gr

    gr = new SVGRX.Group()
    assert.ok( gr instanceof SVGRX.Group, "Group can be created with new" )

    gr = SVGRX.Group()
    assert.ok( gr instanceof SVGRX.Group, "Group can be created with function call" )
})
