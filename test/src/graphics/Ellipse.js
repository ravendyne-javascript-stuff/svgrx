QUnit.module( "Ellipse" )

QUnit.test( "constructor", function( assert )
{
    let ell

    ell = new SVGRX.Ellipse()
    assert.ok( ell instanceof SVGRX.Ellipse, "Ellipse can be elleated with new" )

    ell = SVGRX.Ellipse()
    assert.ok( ell instanceof SVGRX.Ellipse, "Ellipse can be elleated with function call" )
})

QUnit.test( "default constructor", function( assert )
{
    let ell = new SVGRX.Ellipse()

    assert.ok( ell.cx == 0, "'cx' should be 0 by default" )
    assert.ok( ell.cy == 0, "'cy' should be 0 by default" )
    assert.ok( ell.rx == 0, "'rx' should be 0 by default" )
    assert.ok( ell.ry == 0, "'ry' should be 0 by default" )
    assert.ok( ell.isEllipse, "isEllipse property should be set to true" )
})

QUnit.test( "parameters", function( assert )
{
    let attributes = {
        cx: 100,
        cy: 200,
        rx: 50,
        ry: 70,
    }
    let center = new SVGRX.Point( attributes.cx, attributes.cy )

    let ell = new SVGRX.Ellipse( attributes )
    assert.equal( ell.cx, attributes.cx, "Should pick up 'cx' from passed in attributes" )
    assert.equal( ell.cy, attributes.cy, "Should pick up 'cy' from passed in attributes" )
    assert.equal( ell.rx, attributes.rx, "Should pick up 'rx' from passed in attributes" )
    assert.equal( ell.ry, attributes.ry, "Should pick up 'ry' from passed in attributes" )

    assert.deepEqual( ell.center, center, "Property 'center' should be a Point that represents circle center" )
})

QUnit.test( "convert", function( assert )
{
    let ell = new SVGRX.Ellipse({ cx: 100, cy: 200, rx: 50, ry: 70 })

    let converted = ell.convert()

    assert.ok( converted.length != 0, "convert() should return non empty array when object is defined" )
    assert.isArrayOfBezier( converted, "convert() should return an array of Bezier elements" )
})
