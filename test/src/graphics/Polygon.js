QUnit.module( "Polygon" )

QUnit.test( "constructor", function( assert )
{
    let pg

    pg = new SVGRX.Polygon()
    assert.ok( pg instanceof SVGRX.Polygon, "Polygon can be created with new" )

    pg = SVGRX.Polygon()
    assert.ok( pg instanceof SVGRX.Polygon, "Polygon can be created with function call" )
})

QUnit.test( "default constructor", function( assert )
{
    var pg = new SVGRX.Polygon()

    assert.equal( pg.points, '', "default 'points' attribute value should be empty string" )
    assert.deepEqual( pg.pointsData, [], "default parsed points data array should be empty" )
    assert.ok( pg.isPolygon, "isPolygon property should be set to true" )
})

QUnit.test( "parse points data", function( assert )
{
    let pg, data

    data = '600,350 50,-25 42,52'
    pg = new SVGRX.Polygon( { points: data } )
    assert.equal(pg.points, data, "polygon's 'points' attribute should be collected in 'points' property")
    assert.deepEqual(pg.pointsData, [ [600, 350], [50, -25], [42, 52] ], "pointsData should collect all coordinate pairs as array of arrays")

    pg = new SVGRX.Polygon( { points: '600,300' } )
    assert.deepEqual(pg.pointsData, [ [600, 300] ], "pointsData should convert all numeric strings to numbers")
    assert.notDeepEqual(pg.pointsData, [ ['600', '300'] ], "pointsData should convert all numeric strings to numbers")
})

QUnit.test( "convert", function( assert )
{
    let pg = new SVGRX.Polygon({ points: '600,350 50,-25 42,52' })

    let converted = pg.convert()

    assert.ok( converted.length != 0, "convert() should return non empty array when object is defined" )
    assert.isArrayOfBezier( converted, "convert() should return an array of Bezier elements" )
})
