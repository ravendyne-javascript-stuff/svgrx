QUnit.module( "Rect" )

QUnit.test( "constructor", function( assert )
{
    let rc

    rc = new SVGRX.Rect()
    assert.ok( rc instanceof SVGRX.Rect, "Rect can be created with new" )

    rc = SVGRX.Rect()
    assert.ok( rc instanceof SVGRX.Rect, "Rect can be created with function call" )
})

QUnit.test( "default constructor", function( assert )
{
    var rc = new SVGRX.Rect()
    
    assert.ok( rc.x == 0, "'x' should be 0 by default" )
    assert.ok( rc.y == 0, "'y' should be 0 by default" )
    assert.ok( rc.width == 0, "'width' should be 0 by default" )
    assert.ok( rc.height == 0, "'height' should be 0 by default" )
    assert.ok( rc.rx == 0, "'rx' should be 0 by default" )
    assert.ok( rc.ry == 0, "'ry' should be 0 by default" )
    assert.ok( rc.isRect, "isRect property should be set to true" )
})

QUnit.test( "parameters", function( assert )
{
    let attributes = {
        x: 10,
        y: 20,
        width: 100,
        height: 70,
        rx: 5,
        ry: 5,
    }
    let bottomLeft = new SVGRX.Point( attributes.x, attributes.y )
    let topRight = new SVGRX.Point( attributes.x + attributes.width, attributes.y + attributes.height )

    let rc = new SVGRX.Rect( attributes )
    assert.equal( rc.x, attributes.x, "Should pick up 'cx' from passed in attributes" )
    assert.equal( rc.y, attributes.y, "Should pick up 'cy' from passed in attributes" )
    assert.equal( rc.width, attributes.width, "Should pick up 'width' from passed in attributes" )
    assert.equal( rc.height, attributes.height, "Should pick up 'height' from passed in attributes" )
    assert.equal( rc.rx, attributes.rx, "Should pick up 'rx' from passed in attributes" )
    assert.equal( rc.ry, attributes.ry, "Should pick up 'ry' from passed in attributes" )

    assert.deepEqual( rc.topRight, topRight, "Property 'topRight' should be a Point that represents rectangle's top-left corner" )
    assert.deepEqual( rc.bottomLeft, bottomLeft, "Property 'bottomLeft' should be a Point that represents rectangle's bottom-right corner" )
})

QUnit.test( "convert", function( assert )
{
    let rc = SVGRX.Rect({ width: 50, height: 20 })

    let converted = rc.convert()

    assert.ok( converted.length != 0, "convert() should return non empty array when object is defined" )
    assert.isArrayOfBezier( converted, "convert() should return an array of Bezier elements" )
})
