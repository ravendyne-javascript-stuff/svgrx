QUnit.module( "GraphicsElement" )

QUnit.test( "constructor", function( assert )
{
    let ell

    ell = new SVGRX.GraphicsElement()
    assert.ok( ell instanceof SVGRX.GraphicsElement, "GraphicsElement can be created with new" )

    ell = SVGRX.GraphicsElement()
    assert.ok( ell instanceof SVGRX.GraphicsElement, "GraphicsElement can be created with function call" )
})

QUnit.test( "default constructor", function( assert )
{
    var ge = new SVGRX.GraphicsElement()

    assert.ok( ge.isGraphicsElement, "isGraphicsElement property should be set to true" )
})
