QUnit.module( "Path" )

QUnit.test( "constructor", function( assert )
{
    let path

    path = new SVGRX.Path()
    assert.ok( path instanceof SVGRX.Path, "Path can be created with new" )

    path = SVGRX.Path()
    assert.ok( path instanceof SVGRX.Path, "Path can be created with function call" )
})

QUnit.test( "default constructor", function( assert )
{
    let path = new SVGRX.Path()
    
    assert.equal( path.d, '', "default 'd' attribute value should be empty string" )
    assert.deepEqual( path.pathData, [], "default parsed path data array should be empty" )
    assert.ok( path.isPath, "isPath property should be set to true" )
})

QUnit.test( "parse path data", function( assert )
{
    let path, data

    data = 'M600,350 l 50,-25'
    path = new SVGRX.Path( { d: data } )
    assert.equal(path.d, data, "path 'd' attribute should be collected in 'd' property")
    assert.deepEqual(path.pathData, [ 'M', 600, 350, 'l', 50, -25 ], "pathData should collect all operations and their parameters from 'd' in the original order")

    path = new SVGRX.Path( { d: 'M600,300' } )
    assert.deepEqual(path.pathData, [ 'M', 600, 300 ], "pathData should convert all numeric strings to numbers")
    assert.notDeepEqual(path.pathData, [ 'M', '600', '300' ], "pathData should convert all numeric strings to numbers")
})

QUnit.test( "convert", function( assert )
{
    let path = new SVGRX.Path({ d: 'M600,350 l 50,-25' })

    let converted = path.convert()

    assert.ok( converted.length != 0, "convert() should return non empty array when object is defined" )
    assert.isArrayOfBezier( converted, "convert() should return an array of Bezier elements" )
})
