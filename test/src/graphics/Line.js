QUnit.module( "Line" )

QUnit.test( "constructor", function( assert )
{
    let line

    line = new SVGRX.Line()
    assert.ok( line instanceof SVGRX.Line, "Line can be created with new" )

    line = SVGRX.Line()
    assert.ok( line instanceof SVGRX.Line, "Line can be created with function call" )
})

QUnit.test( "default constructor", function( assert )
{
    let line = new SVGRX.Line()

    assert.ok( line.x1 == 0, "'x1' should be 0 by default" )
    assert.ok( line.y1 == 0, "'y1' should be 0 by default" )
    assert.ok( line.x2 == 0, "'x2' should be 0 by default" )
    assert.ok( line.y2 == 0, "'y2' should be 0 by default" )
    assert.ok( line.isLine, "isLine property should be set to true" )
})

QUnit.test( "parameters", function( assert )
{
    let attributes = {
        x1: 100,
        y1: 200,
        x2: 50,
        y2: 70,
    }
    let P1 = new SVGRX.Point( attributes.x1, attributes.y1 )
    let P2 = new SVGRX.Point( attributes.x2, attributes.y2 )

    let line = new SVGRX.Line( attributes )
    assert.equal( line.x1, attributes.x1, "Should pick up 'x1' from passed in attributes" )
    assert.equal( line.y1, attributes.y1, "Should pick up 'y1' from passed in attributes" )
    assert.equal( line.x2, attributes.x2, "Should pick up 'x2' from passed in attributes" )
    assert.equal( line.y2, attributes.y2, "Should pick up 'y2' from passed in attributes" )

    assert.deepEqual( line.P1, P1, "Property 'P1' should be a Point that represents start point x1,y1" )
    assert.deepEqual( line.P2, P2, "Property 'P2' should be a Point that represents end point x2,y2" )
})

QUnit.test( "convert", function( assert )
{
    let line = new SVGRX.Line({ x1: 100, y1: 200, x2: 50, y2: 70 })

    let converted = line.convert()

    assert.ok( converted.length != 0, "convert() should return non empty array when object is defined" )
    assert.isArrayOfBezier( converted, "convert() should return an array of Bezier elements" )
})
