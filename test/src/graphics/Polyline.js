QUnit.module( "Polyline" )

QUnit.test( "constructor", function( assert )
{
    let pl

    pl = new SVGRX.Polyline()
    assert.ok( pl instanceof SVGRX.Polyline, "Polyline can be created with new" )

    pl = SVGRX.Polyline()
    assert.ok( pl instanceof SVGRX.Polyline, "Polyline can be created with function call" )
})

QUnit.test( "default constructor", function( assert )
{
    var pl = new SVGRX.Polyline()

    assert.equal( pl.points, '', "default 'points' attribute value should be empty string" )
    assert.deepEqual( pl.pointsData, [], "default parsed points data array should be empty" )
    assert.ok( pl.isPolyline, "isPolyline property should be set to true" )
})

QUnit.test( "parse points data", function( assert )
{
    let pl, data

    data = '600,350 50,-25 42,52'
    pl = new SVGRX.Polyline( { points: data } )
    assert.equal(pl.points, data, "polygon's 'points' attribute should be collected in 'points' property")
    assert.deepEqual(pl.pointsData, [ [600, 350], [50, -25], [42, 52] ], "pointsData should collect all coordinate pairs as array of arrays")

    pl = new SVGRX.Polyline( { points: '600,300' } )
    assert.deepEqual(pl.pointsData, [ [600, 300] ], "pointsData should convert all numeric strings to numbers")
    assert.notDeepEqual(pl.pointsData, [ ['600', '300'] ], "pointsData should convert all numeric strings to numbers")
})

QUnit.test( "convert", function( assert )
{
    let pl = new SVGRX.Polyline({ points: '600,350 50,-25 42,52' })

    let converted = pl.convert()

    assert.ok( converted.length != 0, "convert() should return non empty array when object is defined" )
    assert.isArrayOfBezier( converted, "convert() should return an array of Bezier elements" )
})
