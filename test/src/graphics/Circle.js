QUnit.module( "Circle" )

QUnit.test( "constructor", function( assert )
{
    let cr

    cr = new SVGRX.Circle()
    assert.ok( cr instanceof SVGRX.Circle, "Circle can be created with new" )

    cr = SVGRX.Circle()
    assert.ok( cr instanceof SVGRX.Circle, "Circle can be created with function call" )
})

QUnit.test( "default constructor", function( assert )
{
    let cr = new SVGRX.Circle()

    assert.ok( cr.cx == 0, "'cx' should be 0 by default" )
    assert.ok( cr.cy == 0, "'cy' should be 0 by default" )
    assert.ok( cr.r == 0, "'r' should be 0 by default" )
    assert.ok( cr.isCircle, "isCircle property should be set to true" )
})

QUnit.test( "parameters", function( assert )
{
    let attributes = {
        cx: 100,
        cy: 200,
        r: 50,
    }
    let center = new SVGRX.Point( attributes.cx, attributes.cy )

    let cr = new SVGRX.Circle( attributes )
    assert.equal( cr.cx, attributes.cx, "Should pick up 'cx' from passed in attributes" )
    assert.equal( cr.cy, attributes.cy, "Should pick up 'cy' from passed in attributes" )
    assert.equal( cr.r, attributes.r, "Should pick up 'r' from passed in attributes" )

    assert.ok( cr.center instanceof SVGRX.Point, "Property 'center' should be a Point" )
    assert.deepEqual( cr.center, center, "Property 'center' should represent circle center" )
})

QUnit.test( "convert", function( assert )
{
    let cr = new SVGRX.Circle({ cx: 100, cy: 200, r: 50 })

    let converted = cr.convert()

    assert.ok( converted.length != 0, "convert() should return non empty array when object is defined" )
    assert.isArrayOfBezier( converted, "convert() should return an array of Bezier elements" )
})
