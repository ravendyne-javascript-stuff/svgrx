/**
 * Compare numbers taking in account an error
 *
 * @param  {Float} number
 * @param  {Float} expected
 * @param  {Float} error    Optional
 * @param  {String} message  Optional
 */

var QUnitUtil = QUnitUtil || {}

QUnitUtil.almostEqual = function( number, expected, error )
{
    var _defaultError = 0.0000001 // 10e-7

    if( error === void 0 || error === null )
    {
        error = _defaultError
    }

    if( typeof error === 'string' )
    {
        message = error
        error = _defaultError
    }

    var result = number == expected || (number < expected + error && number > expected - error) || false

    return result
}

QUnitUtil.isArrayOfBezier = function( array )
{
    if( array instanceof Array )
    {
        if( array.length == 0 )
        {
            return true
        }

        for( let idx = 0; idx < array.length; idx++ )
        {
            if( typeof array[idx] !== 'object'
                && ! ( array[idx] instanceof SVGRX.Bezier ) )
            {
                return false
            }
        }

        return true
    }

    return false
}


QUnit.assert.almostEqual = function( number, expected, error, message )
{
    var _defaultError = 0.0000001 // 10e-7

    if( error === void 0 || error === null )
    {
        error = _defaultError
    }

    if( typeof error === 'string' )
    {
        message = error
        error = _defaultError
    }

    var result = QUnitUtil.almostEqual( number, expected, error )

    this.pushResult( {
        result: result,
        actual: number,
        expected: expected,
        message: message,
    } )
}


QUnit.assert.isArrayOfBezier = function( array, message )
{
    var result = QUnitUtil.isArrayOfBezier( array )

    if( typeof message !== 'string' )
    {
        message = "array should contain Bezier geometry elements"
    }

    this.pushResult( {
        result: result,
        actual: result,
        expected: true,
        message: message,
    } )
}
