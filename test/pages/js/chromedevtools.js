document.addEventListener('DOMContentLoaded', function()
{

    function PointFormatter(){}
    function BezierFormatter(){}
    function MatrixFormatter(){}

    var headerStyle = "background-color: #74B71B; padding: 3px; font-weight: bold;"
    var tdStyle = "text-align: center; border: 1px solid #aaa; padding: 5px 15px 5px 15px;"
    var objectDivStyle = "border-width: 0px 1px 1px 1px; border-style: solid; border-color: #aaa; padding: 5px 15px 5px 15px;"

    Object.assign(PointFormatter.prototype,
    {
        header: function(value, config) {
            if(config && config.useNative === true) return null
            if (!(value instanceof SVGRX.Point)) {
                return null;
            }
            return ["span", {style: headerStyle}, value.constructor.name]
        },
        hasBody: function(value, config) {
            if(config && config.useNative === true) return null
            return value instanceof SVGRX.Point
        },
        body: function(value, config) {
            if(config && config.useNative === true) return null
            if (!(value instanceof SVGRX.Point)) {
                return null;
            }

            // div, span, ol, li, table, tr, td
            return [
                "span", {},
                ["table", {style: "border: 1px solid #aaa; border-collapse: collapse;"},
                    ["tr", {style: "background-color: #ddd;"},
                        ["td", {style: tdStyle}, "x"],
                        ["td", {style: tdStyle}, "y"],
                    ],
                    ["tr", {}, 
                        ["td", {style: tdStyle}, value.x],
                        ["td", {style: tdStyle}, value.y],
                    ],
                ],
                ["div", {style: objectDivStyle},
                    ["object", {"object": value, "config": {useNative: true} }]
                ],
            ]
        },
    })

    Object.assign(BezierFormatter.prototype,
    {
        header: function(value, config) {
            if(config && config.useNative === true) return null
            if (!(value instanceof SVGRX.Bezier)) {
                return null;
            }
            return ["span", {style: headerStyle}, value.constructor.name]
        },
        hasBody: function(value, config) {
            if(config && config.useNative === true) return null
            return value instanceof SVGRX.Bezier
        },
        body: function(value, config) {
            if(config && config.useNative === true) return null
            if (!(value instanceof SVGRX.Bezier)) {
                return null;
            }

            let headerRowCells = [
                ["td", {style: tdStyle}, "P0"],
                ["td", {style: tdStyle}, "P1"],
            ]
            let valueRowCells = [
                ["td", {style: tdStyle}, ""+value.P0],
                ["td", {style: tdStyle}, ""+value.P1],
            ]
            if(value.order >= 3)
            {
                headerRowCells.push(
                    ["td", {style: tdStyle}, "P2"]
                )
                valueRowCells.push(
                    ["td", {style: tdStyle}, ""+value.P2]
                )
            }
            if(value.order >= 4)
            {
                headerRowCells.push(
                    ["td", {style: tdStyle}, "P3"]
                )
                valueRowCells.push(
                    ["td", {style: tdStyle}, ""+value.P3]
                )
            }

            // div, span, ol, li, table, tr, td
            return [
                "span", {},
                ["table", {style: "border: 1px solid #aaa; border-collapse: collapse;"},
                    ["tr", {style: "background-color: #ddd;"}].concat( headerRowCells ),
                    ["tr", {}].concat( valueRowCells ),
                ],
                ["div", {style: objectDivStyle},
                    ["object", {"object": value, "config": {useNative: true} }]
                ],
            ]
        },
    })

    Object.assign(MatrixFormatter.prototype,
    {
        header: function(value, config) {
            if(config && config.useNative === true) return null
            if (!(value instanceof SVGRX.Matrix)) {
                return null;
            }
            return ["span", {style: headerStyle}, value.constructor.name]
        },
        hasBody: function(value, config) {
            if(config && config.useNative === true) return null
            return value instanceof SVGRX.Matrix
        },
        body: function(value, config) {
            if(config && config.useNative === true) return null
            if (!(value instanceof SVGRX.Matrix)) {
                return null;
            }

            let headerRowCells = [
                ["td", {style: tdStyle}, "1"],
                ["td", {style: tdStyle}, "2"],
                ["td", {style: tdStyle}, "3"],
            ]
            let valueRowCells1 = [
                ["td", {style: tdStyle}, ""+value.vect[0]],
                ["td", {style: tdStyle}, ""+value.vect[2]],
                ["td", {style: tdStyle}, ""+value.vect[4]],
            ]
            let valueRowCells2 = [
                ["td", {style: tdStyle}, ""+value.vect[1]],
                ["td", {style: tdStyle}, ""+value.vect[3]],
                ["td", {style: tdStyle}, ""+value.vect[5]],
            ]
            let valueRowCells3 = [
                ["td", {style: tdStyle}, "0"],
                ["td", {style: tdStyle}, "0"],
                ["td", {style: tdStyle}, "1"],
            ]

            // div, span, ol, li, table, tr, td
            return [
                "span", {},
                ["table", {style: "border: 1px solid #aaa; border-collapse: collapse;"},
                    // ["tr", {style: "background-color: #ddd;"}].concat( headerRowCells ),
                    ["tr", {}].concat( valueRowCells1 ),
                    ["tr", {}].concat( valueRowCells2 ),
                    ["tr", {}].concat( valueRowCells3 ),
                ],
                ["div", {style: objectDivStyle},
                    ["object", {"object": value, "config": {useNative: true} }]
                ],
            ]
        },
    })

    window.devtoolsFormatters = [ new PointFormatter(), new BezierFormatter(), new MatrixFormatter() ]

})

