var SVGRXTestTools = function( SVGRXTestTools )
{
    var createTableRow = function( testNr )
    {
        var rowSource = '<table><tr id="testRow"><td><canvas id="drawingA"></canvas></td><td><canvas id="drawingB"></canvas></td><td><canvas id="drawingC"></canvas></td><td><div id="resultText"></div></td></tr></table>'

        var parser = new DOMParser()
        var doc = parser.parseFromString( rowSource, "text/html" )

        doc.getElementById( 'drawingA' ).id = 'drawingA' + testNr
        doc.getElementById( 'drawingB' ).id = 'drawingB' + testNr
        doc.getElementById( 'drawingC' ).id = 'drawingC' + testNr
        doc.getElementById( 'resultText' ).id = 'resultText' + testNr

        return doc.getElementById( 'testRow' )
    }

    var addTableRow = function( testNr )
    {
        var tableElement = document.getElementById( 'testFixture' )
        var tableBodyElement = tableElement.getElementsByTagName( 'tbody' )[ 0 ]
        tableBodyElement.appendChild( createTableRow( testNr ) )
    }

    var doTest = function( testNr, image, resultCallback, isSvgImage = false )
    {
        addTableRow( testNr )

        //
        // Set output elements/contexts
        //
        let ctxA = SVGRXTools.getCanvasContext( 'drawingA' + testNr, 200, 300 )
        let ctxB = SVGRXTools.getCanvasContext( 'drawingB' + testNr, 200, 300 )
        let ctxC = SVGRXTools.getCanvasContext( 'drawingC' + testNr, 200, 300 )
        let resultText = 'resultText' + testNr


        //
        // First column - result
        //
        resultCallback( ctxA )


        //
        // Second column - expected
        //
        ctxB.drawImage( image, 0, 0 )


        //
        // Third column - difference
        //
        let diffImageData = ctxC.createImageData( ctxC.canvas.width, ctxC.canvas.height )
        // compare result with expected
        let resultImageData = ctxA.getImageData( 0, 0, ctxA.canvas.width, ctxA.canvas.height )
        let expectedImageData = ctxB.getImageData( 0, 0, ctxB.canvas.width, ctxB.canvas.height )
        if( isSvgImage )
        {
            SVGRXTools.convertSvgCanvasImageData( expectedImageData )
        }
        SVGRXTools.diffImageData(
            resultImageData,
            expectedImageData,
            diffImageData
        )
        SVGRXTools.applyThresholdToImageData(
            diffImageData,
            diffImageData
        )
        let result = SVGRXTools.scoreImageData( diffImageData )
        // write diff data to canvas
        ctxC.putImageData( diffImageData, 0, 0 )


        //
        // Foruth column - score
        //
        let score = (result * 100).toFixed(4)

        let resultDiv = document.getElementById( resultText )
        resultDiv.innerHTML = 'score is: <br>' + score
        if( Number(score) < 1 )
        {
            resultDiv.innerHTML += '<br> Passed!'
            resultDiv.style.backgroundColor = '#00FF00'
        }
        else
        {
            resultDiv.innerHTML += '<br> Failed!'
            resultDiv.style.backgroundColor = '#FF0000'
        }
    }

    Object.assign( SVGRXTestTools, {
        doTest: doTest,
    })

    return SVGRXTestTools
}( SVGRXTestTools || {} )
