var SVGRXTools = function( SVGRXTools )
{
    // Default drawing color and lineWidth parameters
    // are set for use with the compareImageData() function
    var drawEllipseOnCanvas = function( ctx, Pc, rx, ry, angleStart, angleEnd, ccw = false, color = 'rgba(0,0,0,255)', lineWidth = 2 )
    {
        ctx.strokeStyle = color
        ctx.lineCap = 'square'
        ctx.lineJoin = 'miter'
        ctx.lineWidth = lineWidth

        ctx.beginPath()
        // ctxA.arc(x, y, radius, startAngle, endAngle, ccw)
        ctx.ellipse( Pc.x, Pc.y, rx, ry, 0, angleStart.angleRadians, angleEnd.angleRadians, ccw )
        ctx.stroke()
    }

    // Default drawing color and lineWidth parameters
    // are set for use with the compareImageData() function
    var drawArcOnCanvas = function( ctx, Pc, r, angleStart, angleEnd, ccw = false, color = 'rgba(0,0,0,255)', lineWidth = 2 )
    {
        ctx.strokeStyle = color
        ctx.lineCap = 'square'
        ctx.lineJoin = 'miter'
        ctx.lineWidth = lineWidth

        ctx.beginPath()
        // ctxA.arc(x, y, radius, startAngle, endAngle, ccw)
        ctx.arc( Pc.x, Pc.y, r, angleStart.angleRadians, angleEnd.angleRadians, ccw )
        ctx.stroke()
    }

    var drawHandles = function( ctx, cpoints )
    {
        var handle = function( idx )
        {
            var handleSize = 5
            ctx.fillRect( cpoints[idx].x-handleSize, cpoints[idx].y-handleSize, handleSize*2, handleSize*2 )
        }

        ctx.fillStyle = 'blue'
        // start point
        handle( 0 )
        // end point
        handle( cpoints.length - 1 )

        ctx.fillStyle = 'red'

        for( let idx = 1; idx<cpoints.length-1; idx++ )
        {
            handle( idx )
        }
    }

    // fillStyle is set for use with the compareImageData() function
    var clearCanvas = function( ctx )
    {
        ctx.fillStyle = 'rgba(255,255,255,255)'
        ctx.fillRect(0,0,ctx.canvas.width,ctx.canvas.height)
    }

    // Default drawing color and lineWidth parameters
    // are set for use with the compareImageData() function
    var drawPath = function( ctx, points, color = 'rgba(0,0,0,255)', lineWidth = 2 )
    {
        ctx.lineWidth = lineWidth
        ctx.lineCap = 'square'
        ctx.lineJoin = 'miter'
        ctx.strokeStyle = color

        ctx.beginPath()
        ctx.moveTo( points[0].x, points[0].y )
        for( let idx=1; idx<points.length; idx++)
        {
            ctx.lineTo( points[idx].x, points[idx].y )
        }
        ctx.stroke()
    }

    // Default drawing color and lineWidth parameters
    // are set for use with the compareImageData() function
    var drawBeziers = function( ctx, arrayOfBeziers, color = 'rgba(0,0,0,255)', lineWidth = 2 )
    {
        ctx.lineWidth = lineWidth
        ctx.lineCap = 'square'
        ctx.lineJoin = 'miter'
        ctx.strokeStyle = color

        ctx.beginPath()

        let lastPoint = SVGRX.Point()

        for( let idx=0; idx<arrayOfBeziers.length; idx++)
        {
            let bezier = arrayOfBeziers[ idx ]

            // Tracking the point where previous Bezier ended
            // enables us to issue moveTo command only when it is needed.
            // This in turn provides for lineJoin setting
            // to influence how the point where two curves join
            // looks like when rendered.
            // If we were to issue moveTo for the start point of
            // every curve in arrayOfBeziers, then the points where
            // two curves join wouldn't be considered common point
            // for those two curves and lineJoin wouldn't have any
            // influence on how that point is rendered.
            // This technique makes the same path look exactly the same
            // to the pixel when rendered by either drawBezier() or
            // drawPath().
            if( ! bezier.P0.equals( lastPoint ) )
            {
                ctx.moveTo( bezier.P0.x, bezier.P0.y )
            }

            if( bezier instanceof SVGRX.Bezier2 )
            {
                ctx.lineTo( bezier.P1.x, bezier.P1.y )
                lastPoint = bezier.P1
            }
            else if( bezier instanceof SVGRX.Bezier3 )
            {
                ctx.quadraticCurveTo(
                    bezier.P1.x, bezier.P1.y,
                    bezier.P2.x, bezier.P2.y
                )
                lastPoint = bezier.P2
            }
            else if( bezier instanceof SVGRX.Bezier4 )
            {
                ctx.bezierCurveTo(
                    bezier.P1.x, bezier.P1.y,
                    bezier.P2.x, bezier.P2.y,
                    bezier.P3.x, bezier.P3.y
                )
                lastPoint = bezier.P3
            }
        }

        ctx.stroke()
    }

    var drawPoints = function( ctx, points, color )
    {
        ctx.fillStyle = color
        var size = 2
        for( let idx=0; idx<points.length; idx++)
        {
            ctx.fillRect( points[idx].x-size, points[idx].y-size, size*2, size*2 )
        }
    }

    var getCanvasContext = function( canvasName, canvasWidth = 1200, canvasHeight = 500 )
    {
        var canvas = document.getElementById(canvasName)
        canvas.width = canvasWidth
        canvas.height = canvasHeight
        canvas.style.width = canvasWidth
        canvas.style.height = canvasHeight
        return canvas.getContext('2d')
    }

    var diffImageData = function ( resultData, expectedData, diffData )
    {
        if (resultData.width !== expectedData.width || resultData.height != expectedData.height)
        {
            return
        }
        if( diffData.width !== resultData.width || diffData.height !== resultData.height )
        {
            return
        }

        var n = resultData.data.length

        for (let i = 0; i < n / 4; i++)
        {
            diffData.data[4 * i + 0] = Math.abs( resultData.data[4 * i + 0] - expectedData.data[4 * i + 0] )
            diffData.data[4 * i + 1] = Math.abs( resultData.data[4 * i + 1] - expectedData.data[4 * i + 1] )
            diffData.data[4 * i + 2] = Math.abs( resultData.data[4 * i + 2] - expectedData.data[4 * i + 2] )
            diffData.data[4 * i + 3] = 255
        }
    }

    var applyThresholdToImageData = function ( imageData, resultData, lowThreshold = 127, highThreshold = 255 )
    {
        if (imageData.width !== resultData.width || imageData.height != resultData.height)
        {
            return
        }

        var threshold = function( value )
        {
            if( lowThreshold <= value && value <= highThreshold )
            {
                // white
                return 255
            }

            // black
            return 0
        }

        var n = imageData.data.length

        for (let i = 0; i < n / 4; i++)
        {
            resultData.data[4 * i + 0] = threshold( imageData.data[4 * i + 0] )
            resultData.data[4 * i + 1] = threshold( imageData.data[4 * i + 1] )
            resultData.data[4 * i + 2] = threshold( imageData.data[4 * i + 2] )
            resultData.data[4 * i + 3] = 255
        }
    }

    var scoreImageData = function ( imageData )
    {
        // https://en.wikipedia.org/wiki/Root-mean-square_deviation
        var diff = 0
        var number = 0

        var n = imageData.data.length

        for (let i = 0; i < n / 4; i++)
        {
            let sq
            let sumsq = 0
            sq = imageData.data[4 * i + 0] / 255
            sumsq += sq * sq
            sq = ( imageData.data[4 * i + 1] ) / 255
            sumsq += sq * sq
            sq = ( imageData.data[4 * i + 2] ) / 255
            sumsq += sq * sq
            // diff += Math.sqrt( sumsq )
            diff += sumsq

        }
        diff /= (n / 4 * 3)
        diff = Math.sqrt(diff)

        return diff // 255
    }

    /**
     * When image is loaded from SVG and drawn on canvas using drawImage(),
     * different levels of gray are represented as RGBA value ( 0, 0, 0, lvl )
     * where 'lvl' is gray level 0-255.
     * Our reference JPEG images are using RGBA values in format ( lvl, lvl, lvl, 255 ).
     * and all other functions here assume that is the format of image data:
     * pixel value is encoded in RGB and alpha is set to 1.0 (=255)
     * 
     * This function converts image data of canvas loaded from SVG file into
     * the (lvl,lvl,lvl,255) format
     */
    var convertSvgCanvasImageData = function ( imageData )
    {
        var n = imageData.data.length / 4

        for (let i = 0; i < n; i++)
        {
            let rgb = 255 - imageData.data[4 * i + 3]
            imageData.data[4 * i + 0] = rgb
            imageData.data[4 * i + 1] = rgb
            imageData.data[4 * i + 2] = rgb
            imageData.data[4 * i + 3] = 255
        }
    }

    var compareImageData = function ( resultData, expectedData, diffData )
    {
        if (resultData.width !== expectedData.width || resultData.height != expectedData.height)
        {
            return NaN
        }
        if( diffData.width !== resultData.width || diffData.height !== resultData.height )
        {
            return NaN
        }

        // https://en.wikipedia.org/wiki/Root-mean-square_deviation
        var diff = 0
        var n = resultData.data.length
        var number = 0
        var sumA = 0
        var sumB = 0

        for (let i = 0; i < n / 4; i++)
        {
            var sq
            var sumsq = 0
            sq = ( resultData.data[4 * i + 0] - expectedData.data[4 * i + 0] ) / 255
            sumsq += sq * sq
            sq = ( resultData.data[4 * i + 1] - expectedData.data[4 * i + 1] ) / 255
            sumsq += sq * sq
            sq = ( resultData.data[4 * i + 2] - expectedData.data[4 * i + 2] ) / 255
            sumsq += sq * sq
            // diff += Math.sqrt( sumsq )
            diff += sumsq

            // sq = ( resultData.data[4 * i + 0] )
            // diff += sq * sq
            // sq = ( resultData.data[4 * i + 1] )
            // diff += sq * sq
            // sq = ( resultData.data[4 * i + 2] )
            // diff += sq * sq

            sumA +=   resultData.data[4 * i + 0]
            sumA +=   resultData.data[4 * i + 1]
            sumA +=   resultData.data[4 * i + 2]
            sumB +=   expectedData.data[4 * i + 0]  
            sumB +=   expectedData.data[4 * i + 1]  
            sumB +=   expectedData.data[4 * i + 2]  

            diffData.data[4 * i + 0] = Math.abs( resultData.data[4 * i + 0] - expectedData.data[4 * i + 0] )
            diffData.data[4 * i + 1] = Math.abs( resultData.data[4 * i + 1] - expectedData.data[4 * i + 1] )
            diffData.data[4 * i + 2] = Math.abs( resultData.data[4 * i + 2] - expectedData.data[4 * i + 2] )
            diffData.data[4 * i + 3] = 255
        }
        diff /= (n / 4 * 3)
        diff = Math.sqrt(diff)

        return diff // 255
    }

    var convertToBlackAndWhite = function ( imageData, resultData, threshold = 0.5 )
    {
        threshold *= 255
        threshold = threshold.toFixed( 0 )
        // https://en.wikipedia.org/wiki/Relative_luminance
        // relative luminance for linear RGB space
        // for transformations lRGB <-> sRGB see: https://en.wikipedia.org/wiki/SRGB#Specification_of_the_transformation
        var luminance = function( R, G, B )
        {
            return ( ( 0.2126 * R ) + ( 0.7152 * G ) + ( 0.0722 * B ) ) / 255.0
        }
        var blackAndWhiteFn = function( luminance )
        {
            return luminance < threshold ? 0 : 255
        }

        // https://en.wikipedia.org/wiki/Root-mean-square_deviation
        var n = imageData.data.length

        for (let i = 0; i < n / 4; i++)
        {
            let red = imageData.data[4 * i + 0]
            let green = imageData.data[4 * i + 1]
            let blue = imageData.data[4 * i + 2]

            let bwcol = blackAndWhiteFn( luminance( red, green, blue ) * 255 )

            resultData.data[4 * i + 0] = bwcol
            resultData.data[4 * i + 1] = bwcol
            resultData.data[4 * i + 2] = bwcol
            resultData.data[4 * i + 3] = 255
        }
    }

    Object.assign( SVGRXTools, {
        drawEllipseOnCanvas: drawEllipseOnCanvas,
        drawArcOnCanvas: drawArcOnCanvas,
        drawHandles: drawHandles,
        clearCanvas: clearCanvas,
        drawPath: drawPath,
        drawBeziers: drawBeziers,
        drawPoints: drawPoints,
        getCanvasContext: getCanvasContext,
        compareImageData: compareImageData,
        scoreImageData: scoreImageData,
        convertSvgCanvasImageData: convertSvgCanvasImageData,
        diffImageData: diffImageData,
        applyThresholdToImageData: applyThresholdToImageData,
        convertToBlackAndWhite: convertToBlackAndWhite,
    })

    return SVGRXTools
}( SVGRXTools || {} )
