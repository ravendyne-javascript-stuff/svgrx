#!/usr/bin/env python

import SimpleHTTPServer
import BaseHTTPServer

server_address = ('127.0.0.1', 8888)
httpd = BaseHTTPServer.HTTPServer(server_address, SimpleHTTPServer.SimpleHTTPRequestHandler)
print "Starting server on http://%s:%s" % server_address
httpd.serve_forever()
