#!/usr/bin/python

import svgwrite
from svgwrite import cm, mm


# Inkscape hardwired resolution
dpi = 96

# Use metric:
chord = 160.0 #mm
dpl = dpi/25.4 #dots/mm
# or:
# chord = 20.0 #inch
# dpl = dpi #dots/inch

scale = chord * dpl

def plot(data_file):
    dwg = svgwrite.Drawing(filename=data_file+".svg", debug=True)
    profile = dwg.add(dwg.g(id='profile', stroke='red'))

    path = svgwrite.path.Path(fill='none')
    path.push("M%d,%d" % (scale, 0))

    with open(data_file) as file:
        lines = file.readlines()
        for line in lines:
            line = line.strip()
            if line[0].isdigit():
                coords = line.split()
                x = int(float(coords[0]) * scale)
                y = -int(float(coords[1]) * scale)
                # print x, y
                path.push("L%d,%d" % (x, y))


    path.push("Z")
    profile.add(path)

    dwg.add(dwg.text(data_file, x=[0], y=[0]))

    dwg.save()


data_files = ['s1210.dat', 'e423.dat']

for data_file in data_files:
    print data_file
    plot(data_file)

