https://www.w3.org/TR/SVG/Overview.html


============================================================================================================

- [ ] organize
    - class tree with attributes and methods
    - phases
    - which ones are used in what phase

- [ ] update tests by phases
    - [ ] geometry classes
        - [ ] constructors
        - [ ] methods
    - [ ] svg classes
        - [x] constructors
        - [x] properties/attributes
        - [ ] convert methods
        - [ ] svg.flatten()

- [ ] image drawing tests
    - [ ] refactor tools to be used
    - [ ] create test template
    - [ ] set up pages/runners for tests

- [ ] update class/function jsdocs as you go

- [ ] move regex to types/Matchers and create parsers for svg attributes

- [ ] examples ???





- [ ] Group append() ??

- [ ] Svg.js: remove SVGRX references, replace with imports, or move the methods/functions to separate module

- [ ] handle SVG error cases and process it (https://www.w3.org/TR/SVG/implnote.html#ErrorProcessing)


- [ ] set viewport on each svg child element (for Transformable.lenght() method to use)


============================================================================================================
Drawing an elliptical arc using polylines, quadratic or cubic Bezier curves 

https://www.spaceroots.org/documents/ellipse/
https://www.spaceroots.org/documents/ellipse/elliptical-arc.pdf
https://www.spaceroots.org/documents/ellipse/EllipticalArc.java


- convert list of elements to list of basic operations with transformations applied to their numerical parameters
- list of basic operations (essentially a https://docs.oracle.com/javase/7/docs/api/java/awt/geom/PathIterator.html):
    - move to
    - linear bezier (line to)
    - quadratic bezier
    - cubic bezier

- converts series of basic operations to segments with predefined precision
    - this can be directly converted to gcode with only using move command

- convert series of basic operations to gcode with predefined precision
    - this should provide for possibility to use other gcode operations like arc etc.


affine transformations (svg.Matrix)
https://docs.oracle.com/javase/7/docs/api/java/awt/geom/AffineTransform.html

============================================================================================================
https://svg-edit.github.io/svgedit/releases/svg-edit-2.8.1/svg-editor.html


"babel-preset-es2015-rollup" includes (http://babeljs.io/docs/plugins/preset-es2015/):

check-es2015-constants
transform-es2015-arrow-functions
transform-es2015-block-scoped-functions
transform-es2015-block-scoping
transform-es2015-classes
transform-es2015-computed-properties
transform-es2015-destructuring
transform-es2015-duplicate-keys
transform-es2015-for-of
transform-es2015-function-name
transform-es2015-literals
transform-es2015-modules-commonjs
transform-es2015-object-super
transform-es2015-parameters
transform-es2015-shorthand-properties
transform-es2015-spread
transform-es2015-sticky-regex
transform-es2015-template-literals
transform-es2015-typeof-symbol
transform-es2015-unicode-regex
transform-regenerator

