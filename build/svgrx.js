(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
    typeof define === 'function' && define.amd ? define(['exports'], factory) :
    (factory((global.SVGRX = global.SVGRX || {})));
}(this, (function (exports) { 'use strict';

    // Polyfills

    if (Math.radians === undefined) {

        Math.radians = function (degrees) {

            return degrees * Math.PI / 180;
        };
    }

    if (Math.degrees === undefined) {

        Math.degrees = function (radians) {

            return radians * 180 / Math.PI;
        };
    }

    if (Number.EPSILON === undefined) {

        Number.EPSILON = Math.pow(2, -52);
    }

    if (Number.isInteger === undefined) {

        // Missing in IE
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/isInteger

        Number.isInteger = function (value) {

            return typeof value === 'number' && isFinite(value) && Math.floor(value) === value;
        };
    }

    if (Number.isFinite === undefined) {

        Number.isFinite = function (value) {
            return typeof value === 'number' && isFinite(value);
        };
    }

    if (Function.prototype.name === undefined) {

        // Missing in IE
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/name

        Object.defineProperty(Function.prototype, 'name', {

            get: function get() {

                return this.toString().match(/^\s*function\s*([^\(\s]*)/)[1];
            }

        });
    }

    if (Object.assign === undefined) {

        // Missing in IE
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign

        (function () {

            Object.assign = function (target) {

                'use strict';

                if (target === undefined || target === null) {

                    throw new TypeError('Cannot convert undefined or null to object');
                }

                var output = Object(target);

                for (var index = 1; index < arguments.length; index++) {

                    var source = arguments[index];

                    if (source !== undefined && source !== null) {

                        for (var nextKey in source) {

                            if (Object.prototype.hasOwnProperty.call(source, nextKey)) {

                                output[nextKey] = source[nextKey];
                            }
                        }
                    }
                }

                return output;
            };
        })();
    }

    // https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach#Polyfill
    // Production steps of ECMA-262, Edition 5, 15.4.4.18
    // Reference: http://es5.github.io/#x15.4.4.18
    if (!Array.prototype.forEach) {

        Array.prototype.forEach = function (callback /*, thisArg*/) {

            var T, k;

            if (this == null) {
                throw new TypeError('this is null or not defined');
            }

            // 1. Let O be the result of calling toObject() passing the
            // |this| value as the argument.
            var O = Object(this);

            // 2. Let lenValue be the result of calling the Get() internal
            // method of O with the argument "length".
            // 3. Let len be toUint32(lenValue).
            var len = O.length >>> 0;

            // 4. If isCallable(callback) is false, throw a TypeError exception. 
            // See: http://es5.github.com/#x9.11
            if (typeof callback !== 'function') {
                throw new TypeError(callback + ' is not a function');
            }

            // 5. If thisArg was supplied, let T be thisArg; else let
            // T be undefined.
            if (arguments.length > 1) {
                T = arguments[1];
            }

            // 6. Let k be 0
            k = 0;

            // 7. Repeat, while k < len
            while (k < len) {

                var kValue;

                // a. Let Pk be ToString(k).
                //    This is implicit for LHS operands of the in operator
                // b. Let kPresent be the result of calling the HasProperty
                //    internal method of O with argument Pk.
                //    This step can be combined with c
                // c. If kPresent is true, then
                if (k in O) {

                    // i. Let kValue be the result of calling the Get internal
                    // method of O with argument Pk.
                    kValue = O[k];

                    // ii. Call the Call internal method of callback with T as
                    // the this value and argument list containing kValue, k, and O.
                    callback.call(T, kValue, k, O);
                }
                // d. Increase k by 1.
                k++;
            }
            // 8. return undefined
        };
    }

    // https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/String/includes#Polyfill
    if (!String.prototype.includes) {
        String.prototype.includes = function (search, start) {
            'use strict';

            if (typeof start !== 'number') {
                start = 0;
            }

            if (start + search.length > this.length) {
                return false;
            } else {
                return this.indexOf(search, start) !== -1;
            }
        };
    }

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/sign#Polyfill
    if (!Math.sign) {
        Math.sign = function (x) {
            // If x is NaN, the result is NaN.
            // If x is -0, the result is -0.
            // If x is +0, the result is +0.
            // If x is negative and not -0, the result is -1.
            // If x is positive and not +0, the result is +1.
            x = +x; // convert to a number
            if (x === 0 || isNaN(x)) {
                return Number(x);
            }
            return x > 0 ? 1 : -1;
        };
    }

    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
      return typeof obj;
    } : function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };









































    var slicedToArray = function () {
      function sliceIterator(arr, i) {
        var _arr = [];
        var _n = true;
        var _d = false;
        var _e = undefined;

        try {
          for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
            _arr.push(_s.value);

            if (i && _arr.length === i) break;
          }
        } catch (err) {
          _d = true;
          _e = err;
        } finally {
          try {
            if (!_n && _i["return"]) _i["return"]();
          } finally {
            if (_d) throw _e;
          }
        }

        return _arr;
      }

      return function (arr, i) {
        if (Array.isArray(arr)) {
          return arr;
        } else if (Symbol.iterator in Object(arr)) {
          return sliceIterator(arr, i);
        } else {
          throw new TypeError("Invalid attempt to destructure non-iterable instance");
        }
      };
    }();

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * param - {} or BaseElement
     */
    function BaseElement(param) {
        //
        // Case when BaseElement is called as a function
        //
        if (!(this instanceof BaseElement)) {
            return new BaseElement(param);
        }

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _id = GUID();
        var _class = '';
        var _style = '';
        var _attributes = {};

        var _children = [];

        //
        // PRIVATE FUNCTIONS
        //
        var setPropertiesFromAttributes = function setPropertiesFromAttributes(attributes) {
            _id = attributes['id'] || _id;
            _class = attributes['class'] || _class;
            _style = attributes['style'] || _style;
        };

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            id: {
                get: function get$$1() {
                    return _id;
                },
                set: function set$$1(value) {
                    if (typeof value === 'string') _id = value;
                }
            },
            class: {
                get: function get$$1() {
                    return _class;
                }
            },
            style: {
                get: function get$$1() {
                    return _style;
                }
            },
            attrs: {
                get: function get$$1() {
                    return _attributes;
                }
            },

            children: {
                get: function get$$1() {
                    return _children;
                },
                set: function set$$1(value) {
                    if (isBaseElementArray(value)) _children = value;
                }
            }
        });

        //
        // constructor code
        //
        if (param instanceof BaseElement) {
            _attributes = {};
            Object.assign(_attributes, param.attrs);

            setPropertiesFromAttributes(_attributes);

            _children = param.children;
        } else if ((typeof param === 'undefined' ? 'undefined' : _typeof(param)) === 'object') {
            Object.assign(_attributes, param);

            setPropertiesFromAttributes(_attributes);
        }
    }

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(BaseElement.prototype, {
        toString: function toString() {
            return '<BaseElement ' + this.id + '>';
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
        Affine transformation matrix and its operations.
        (https://en.wikipedia.org/wiki/Affine_transformation)
        (https://docs.oracle.com/javase/7/docs/api/java/awt/geom/AffineTransform.html)
        (https://upload.wikimedia.org/wikipedia/commons/2/2c/2D_affine_transformation_matrix.svg)

        SVG represents affine transform matrix as a list of 6 values [a, b, c, d, e, f]
        in an element's 'transform' attribute. The 6 values represent matrix elements as follows:

        | a  c  e |
        | b  d  f |
        | 0  0  1 |

        See: https://www.w3.org/TR/SVG/coords.html#TransformMatrixDefined
    */
    function Matrix(param) {
        if (!(this instanceof Matrix)) {
            return new Matrix(param);
        }

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _unitVector = [1, 0, 0, 1, 0, 0];
        // default transformation is unit vector
        var _vect = _unitVector;

        //
        // PRIVATE FUNCTIONS
        //
        function getVectorForParameter(param) {
            if (param instanceof Array && param.length == 6) {
                if (isNumericArray(param)) {
                    return param;
                }
            }

            return _unitVector;
        }

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            // private properties getters/setters
            // so we don't have to, i.e. check if assigned values are numbers
            // or to try/catch when working with private props in every other method
            vect: {
                enumerable: true,
                get: function get() {
                    return _vect;
                },
                set: function set(value) {
                    _vect = getVectorForParameter(value);
                }
            }
        });

        //
        // constructor code
        //
        _vect = getVectorForParameter(param);
    }

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Matrix.prototype, {
        toString: function toString() {
            return "[" + this.vect + "]";
        },

        /** Matrix multiplication with:
            - matrix
            - point
        */
        mul: function mul(other) {
            if (other instanceof Matrix) {
                var a = this.vect[0] * other.vect[0] + this.vect[2] * other.vect[1];
                var b = this.vect[1] * other.vect[0] + this.vect[3] * other.vect[1];
                var c = this.vect[0] * other.vect[2] + this.vect[2] * other.vect[3];
                var d = this.vect[1] * other.vect[2] + this.vect[3] * other.vect[3];
                var e = this.vect[0] * other.vect[4] + this.vect[2] * other.vect[5] + this.vect[4];
                var f = this.vect[1] * other.vect[4] + this.vect[3] * other.vect[5] + this.vect[5];

                return new Matrix([a, b, c, d, e, f]);
            } else if (other instanceof Point) {
                var x = other.x * this.vect[0] + other.y * this.vect[2] + this.vect[4];
                var y = other.x * this.vect[1] + other.y * this.vect[3] + this.vect[5];

                return new Point(x, y);
            }

            // pass-through
            return other;
        },

        scale: function scale(sx, sy) {
            sx = sx || 0;
            sy = sy || sx;

            if (!Number.isFinite(sx) && !Number.isFinite(sy)) {
                return this;
            }

            return this.mul(Matrix([sx, 0, 0, sy, 0, 0]));
        },

        skew: function skew(ax, ay) {
            ax = ax || 0;
            ay = ay || 0;

            if (!Number.isFinite(ax) && !Number.isFinite(ay)) {
                return this;
            }

            var tanax = Math.tan(Math.radians(ax));
            var tanay = Math.tan(Math.radians(ay));

            return this.mul(Matrix([1, tanay, tanax, 1, 0, 0]));
        },

        translate: function translate(tx, ty) {
            tx = tx;
            ty = ty || 0;

            return this.mul(Matrix([1, 0, 0, 1, tx, ty]));
        },

        rotate: function rotate(angle) {
            var cosa = Math.cos(Math.radians(angle));
            var sina = Math.sin(Math.radians(angle));

            return this.mul(Matrix([cosa, sina, -sina, cosa, 0, 0]));
        }

    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    var MatchRegex = {
        // 10, 10.00, .10, +10, -10
        decimalNumber: '[-+]?(?:\\d+(?:\\.\\d*)?|\\.\\d+)',
        // one of the literal values
        unit: '(?:em|ex|px|in|cm|mm|pt|pc|%)?'
    };

    var collectMatches = function collectMatches(regex, str) {
        var result = [];

        var m = regex.exec(str);
        while (m !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            if (m.index === regex.lastIndex) {
                regex.lastIndex++;
            }

            // The result can be accessed through the `m`-variable.
            m.forEach(function (match, groupIndex) {
                result.push(match);
            });

            m = regex.exec(str);
        }

        return result;
    };

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * param - {} or TransformableElement
     */
    function TransformableElement(param) {
        //
        // Case when TransformableElement is called as a function
        //
        if (!(this instanceof TransformableElement)) {
            return new TransformableElement(param);
        }

        BaseElement.call(this, param);

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _transform = '';

        var _transformData = [];
        var _transformMatrix = [];

        //
        // PRIVATE FUNCTIONS
        //
        var parseTransform = function parseTransform(transform) {
            var result = [];

            var _numberRegex = /[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?/g;
            var _unitRegex = /em|ex|px|in|cm|mm|pt|pc|%/g;

            var svg_transforms = ['matrix', 'translate', 'scale', 'rotate', 'skewX', 'skewY'];

            // match any SVG transformation with its parameter (until final parenthese)
            // [^)]*    == anything but a closing parenthese
            // '|'.join == OR-list of SVG transformations
            svg_transforms = svg_transforms.map(function (val) {
                // match transform name and anything after it up to, and including, ')'
                return val + '[^)]*\\)';
            });
            // join matchers for all transforms with regex OR: '|''
            var regex = svg_transforms.join('|');
            var transformsRegex = new RegExp(regex, 'g');

            var matches = collectMatches(transformsRegex, transform);
            // 'matches' is an array of strings in a form "operation_name ( arg1, arg2 ... )"
            // i.e. ["translate(-10,-20)", "scale(2)", "rotate(45)", "translate(5,10)"]
            for (var i = 0; i < matches.length; i++) {
                // opargs is an array of two strings: operation name and arguments between ()
                // i.e. ["translate", "-10,-20)"]
                var opargs = matches[i].split('(');

                // opargs[1] is matched to produce args array of strings where each element is one operation argument
                // i.e. ["-10", "-20"]
                var args = collectMatches(_numberRegex, opargs[1]);
                args = args.map(function (val) {
                    return parseFloat(val) || 0;
                });

                result.push({
                    // operation (string)
                    op: opargs[0],
                    // arguments array (numbers)
                    args: args
                });
            }

            return result;
        };

        var createTransformMatrix = function createTransformMatrix(data) {
            var matrix = new Matrix();

            for (var idx = 0; idx < data.length; idx++) {
                var op = data[idx].op;
                var args = data[idx].args;

                switch (op) {
                    case 'matrix':
                        {
                            matrix = matrix.mul(new Matrix(args));
                        }
                        break;

                    case 'translate':
                        {
                            var tx = args[0];
                            var ty = args[1] || 0;
                            matrix = matrix.mul(Matrix([1, 0, 0, 1, tx, ty]));
                        }
                        break;

                    case 'scale':
                        {
                            var sx = args[0];
                            var sy = args[1] || sx;
                            matrix = matrix.mul(Matrix([sx, 0, 0, sy, 0, 0]));
                        }
                        break;

                    case 'rotate':
                        {
                            var cosa = Math.cos(Math.radians(args[0]));
                            var sina = Math.sin(Math.radians(args[0]));
                            var cx = args[1] || 0;
                            var cy = args[2] || 0;

                            // translate to rotation center
                            matrix = matrix.mul(Matrix([1, 0, 0, 1, cx, cy]));
                            // rotate
                            matrix = matrix.mul(Matrix([cosa, sina, -sina, cosa, 0, 0]));
                            // translate back to where we were
                            matrix = matrix.mul(Matrix([1, 0, 0, 1, -cx, -cy]));
                        }
                        break;

                    case 'skewX':
                        {
                            var tana = Math.tan(Math.radians(args[0]));
                            matrix = matrix.mul(Matrix([1, 0, tana, 1, 0, 0]));
                        }
                        break;

                    case 'skewY':
                        {
                            var _tana = Math.tan(Math.radians(args[0]));
                            matrix = matrix.mul(Matrix([1, _tana, 0, 1, 0, 0]));
                        }
                        break;
                }
            }

            return matrix;
        };

        var setPropertiesFromAttributes = function setPropertiesFromAttributes(attributes) {
            _transform = attributes['transform'] || _transform;

            _transformData = parseTransform(_transform);
            _transformMatrix = createTransformMatrix(_transformData);
        };

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            transform: {
                get: function get$$1() {
                    return _transform;
                }
            },

            transformData: {
                get: function get$$1() {
                    return _transformData;
                }
            },
            matrix: {
                get: function get$$1() {
                    return _transformMatrix;
                }
            }
        });

        //
        // constructor code
        //
        if (param instanceof TransformableElement) {
            setPropertiesFromAttributes({ 'transform': param.transform });
        } else if ((typeof param === 'undefined' ? 'undefined' : _typeof(param)) === 'object') {
            setPropertiesFromAttributes(this.attrs);
        }
    }

    //
    // CLASS INHERITANCE
    //
    TransformableElement.prototype = Object.assign(Object.create(BaseElement.prototype), {
        constructor: TransformableElement,
        isTransformableElement: true
    });

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(TransformableElement.prototype, {
        toString: function toString() {
            return '<TransformableElement ' + this.id + '>';
        },

        // Override in subclass
        convert: function convert(matrix) {
            // array of Beziers
            return [];
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    var isBaseElementArray = function isBaseElementArray(array) {
        if (array instanceof Array) {
            if (array.length == 0) {
                return true;
            }

            for (var idx = 0; idx < array.length; idx++) {
                if (_typeof(array[idx]) !== 'object' && !(array[idx] instanceof BaseElement)) {
                    return false;
                }
            }

            return true;
        }

        return false;
    };

    var isNumericArray = function isNumericArray(array) {
        if (array instanceof Array) {
            if (array.length == 0) {
                return true;
            }

            for (var idx = 0; idx < array.length; idx++) {
                if (typeof array[idx] !== 'number' && !(array[idx] instanceof Number)) {
                    return false;
                }
            }

            return true;
        }

        return false;
    };

    var isPointArray = function isPointArray(array) {
        if (array instanceof Array) {
            if (array.length == 0) {
                return true;
            }

            for (var idx = 0; idx < array.length; idx++) {
                if (_typeof(array[idx]) !== 'object' && !(array[idx] instanceof Point)) {
                    return false;
                }
            }

            return true;
        }

        return false;
    };

    var isTransformableElementArray = function isTransformableElementArray(array) {
        if (array instanceof Array) {
            if (array.length == 0) {
                return true;
            }

            for (var idx = 0; idx < array.length; idx++) {
                if (_typeof(array[idx]) !== 'object' && !(array[idx] instanceof TransformableElement)) {
                    return false;
                }
            }

            return true;
        }

        return false;
    };

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce?v=example
    var flattenArray = function flattenArray(array) {
        return array.reduce(function (acc, val) {
            return acc.concat(Array.isArray(val) ? flattenArray(val) : val);
        }, []);
    };

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    var lengthInBaseUnits = function lengthInBaseUnits(v) {
        var mode = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'xy';
        var viewport = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : Point();

        var numberRegex = /[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?/g;
        var unitRegex = /em|ex|px|in|cm|mm|pt|pc|%/g;
        var unitConversions = {
            '': 1, // Default unit (same as pixel)
            'px': 1, // px: pixel. Default SVG unit
            'em': 10, // 1 em = 10 px FIXME
            'ex': 5, // 1 ex =  5 px FIXME
            'in': 96, // 1 in = 96 px
            'cm': 96 / 2.54, // 1 cm = 1/2.54 in
            'mm': 96 / 25.4, // 1 mm = 1/25.4 in
            'pt': 96 / 72.0, // 1 pt = 1/72 in
            'pc': 96 / 6.0, // 1 pc = 1/6 in
            '%': 1 / 100.0 // 1 percent
        };

        if (typeof v !== 'string') {
            return v;
        }

        var value = 0;

        var match = numberRegex.exec(v);
        if (match === null) {
            return null;
        }
        value = match[0];

        var unit = '';

        var match = unitRegex.exec(v);
        if (match !== null) {
            unit = match[0];
        }

        if (unit == '%') {
            if (mode == 'x') {
                return parseFloat(value) * unitConversions[unit] * viewport.x;
            }
            if (mode == 'y') {
                return parseFloat(value) * unitConversions[unit] * viewport.y;
            }
            if (mode == 'xy') {
                //???
            }
        }

        var result = parseFloat(value) || 0;
        result *= unitConversions[unit];

        return result;
    };

    var pointArrayToNumeric = function pointArrayToNumeric(array) {
        if (!isPointArray(array)) {
            return [];
        }

        var coordPoints = array.map(function (pt) {
            return [pt.x, pt.y];
        });

        return flattenArray(coordPoints);
    };

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * Rounds numbers to twice the number of decimals specified
     * in Point.precision and rounds to 0 numbers that are
     * smaller than +/- 10^(-Point.precision).
     */
    var roundToPrecision = function roundToPrecision(value) {
        // we only handle near zero values
        var epsilon = Math.pow(10, -Point.precision);
        if (-epsilon < value && value < epsilon) return 0;

        // return value
        return Number(value.toFixed(Point.precision * 2));
    };

    var minX = function minX(array) {
        if (!isPointArray(array)) {
            return 0;
        }

        return array.reduce(function (acc, val, idx) {
            if (val.x < acc) return val.x;
            return acc;
        }, 0);
    };

    var minY = function minY(array) {
        if (!isPointArray(array)) {
            return 0;
        }

        return array.reduce(function (acc, val, idx) {
            if (val.y < acc) return val.y;
            return acc;
        }, 0);
    };

    var maxX = function maxX(array) {
        if (!isPointArray(array)) {
            return 0;
        }

        return array.reduce(function (acc, val, idx) {
            if (val.x > acc) return val.x;
            return acc;
        }, 0);
    };

    var maxY = function maxY(array) {
        if (!isPointArray(array)) {
            return 0;
        }

        return array.reduce(function (acc, val, idx) {
            if (val.y > acc) return val.y;
            return acc;
        }, 0);
    };

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    // https://tools.ietf.org/html/rfc4122
    // http://www.boost.org/doc/libs/1_47_0/boost/uuid/uuid.hpp
    // https://en.wikipedia.org/wiki/Universally_unique_identifier
    var GUID = function GUID() {
        var quad = function quad() {
            var zeroMask = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0xFFFF;
            var oneMask = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0x0000;

            // Get random value between 0x10000 and 0x20000.
            // This will ensure that we have leading zeroes later on
            // when we do conversion to hex string
            var randomValue = Math.floor((1 + Math.random()) * 0x10000);
            randomValue = randomValue & (0xF0000 | zeroMask) | oneMask;

            return '' + randomValue
            // convert to hex string ( base == 16 ),
            // this string will be 5 characters long
            // since generated value is between 0x10000 and 0x20000.
            .toString(16)
            // strip leading character so we are left
            // with 4-character string
            .substring(1);
        };

        return '' +
        // time_low
        quad() + quad() + '-' +
        // time_mid
        quad() + '-' +
        // time_hi
        // bits 12-15 to 4-bit verion (v4)
        quad(0x0FFF, 0x4000) + '-' +
        // clock_seq
        // bits 6-7 to 01
        quad(0x3FFF, 0x8000) + '-' +
        // node
        quad() + quad() + quad();
    };

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * Point is a class that represents specific point in 2D space
     * as opposed to a point that moves around in 2D space (i.e. by changing it's x,y values)
     *
     * This means that all arithmetic methods of the class (i.e. add(), mul() etc.)
     * return as result a new Point object.
     *
     * Ways to create an object that represents a specific point in 2D space:
     * var pt = new Point( x, y )
     * var pt = new Point( [x, y] )
     * var pt = new Point( otherPointObject )
     */
    function Point(x, y) {
        //
        // Case when Point is called as a function
        //
        if (!(this instanceof Point)) {
            return new Point(x, y);
        }

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _x = 0;
        var _y = 0;

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            // X, Y getter/setter
            // so we don't have to check if assigned values are numbers
            // or to try/catch when working with X, Y in every other method
            x: {
                enumerable: true,
                get: function get() {
                    return _x;
                },
                set: function set(value) {
                    if (Number.isFinite(value)) _x = roundToPrecision(value);
                }
            },
            y: {
                enumerable: true,
                get: function get() {
                    return _y;
                },
                set: function set(value) {
                    if (Number.isFinite(value)) _y = roundToPrecision(value);
                }
            }
        });

        //
        // constructor code
        //
        if (Number.isFinite(x) && Number.isFinite(y)) {
            this.x = x;
            this.y = y;
        }
        if (isNumericArray(x) && x.length == 2) {
            this.x = x[0];
            this.y = x[1];
        }
        if (x instanceof Point) {
            this.x = x.x;
            this.y = x.y;
        }
    }

    //
    // 'STATIC' CLASS PROPERTIES
    //
    Object.defineProperties(Point, {
        precision: {
            get: function get() {
                if (!Point._precision) {
                    Point._precision = 6;
                }

                if (!Number.isFinite(Point._precision)) {
                    Point._precision = 6;
                }

                return Number(Point._precision.toFixed(0));
            },
            set: function set(value) {
                if (Number.isFinite(value)) {
                    Point._precision = Number(value.toFixed(0));
                }
            }
        }
    });

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Point.prototype, {
        toString: function toString() {
            return '(' + this.x.toFixed(Point.precision) + ', ' + this.y.toFixed(Point.precision) + ')';
        },

        add: function add(other) {
            if (other instanceof Point) {
                return new Point(this.x + other.x, this.y + other.y);
            }

            return this;
        },

        sub: function sub(other) {
            if (other instanceof Point) {
                return new Point(this.x - other.x, this.y - other.y);
            }

            return this;
        },

        mul: function mul(other) {
            if (Number.isFinite(other)) {
                return new Point(this.x * other, this.y * other);
            }

            return this;
        },

        equals: function equals(other) {
            if (!(other instanceof Point)) {
                return false;
            }

            return this.x == other.x && this.y == other.y;
        },

        coord: function coord() {
            return [this.x, this.y];
        },

        /** The point distance from 2D space origin (0, 0) */
        length: function length() {
            return roundToPrecision(Math.sqrt(this.x * this.x + this.y * this.y));
        },

        /** Return a point that is at a given angular distance along the circle
            constructed when this point rotates around 2D space origin (0, 0)
            at radius equal this.length() */
        rotate: function rotate(angle) {
            if (!(angle instanceof Angle)) {
                angle = new Angle(angle);
            }

            var x = this.x * angle.cos - this.y * angle.sin;
            var y = this.x * angle.sin + this.y * angle.cos;
            x = roundToPrecision(x);
            y = roundToPrecision(y);

            return new Point(x, y);
        },

        /** Returns normalized point (length() == 1) */
        norm: function norm() {
            var len = this.length();
            return new Point(this.x / len, this.y / len);
        },

        /** Returns dot product of vectors (origin, this) and (origin, other) */
        dot: function dot(other) {
            other = new Point(other);

            return this.x * other.x + this.y * other.y;
        },

        /** Returns angle in degrees between vectors (origin, this) and (origin, other) */
        angle: function angle(other) {
            if (!(other instanceof Point)) {
                return Math.degrees(Math.atan2(this.y, this.x));
            }

            // let angle = other.angle() - this.angle()
            // return angle

            var angle = Math.atan2(other.y, other.x) - Math.atan2(this.y, this.x);
            return Math.degrees(angle);
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * Helper class when working with specific angle values
     * so we don't have to do calculations like sin, cos, etc.
     * every time we reference the angle.
     *
     * 'param' is angle in degrees
     */
    function Angle(param) {
        //
        // Case when Angle is called as a function
        //
        if (!(this instanceof Angle)) {
            return new Angle(param);
        }

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _angleRad = 0;
        var _angleDeg = 0;
        var _sin = 0;
        var _cos = 1;

        //
        // PRIVATE FUNCTIONS
        //
        function setAngle(angle) {
            if (Number.isFinite(angle)) {
                // Constrain to values between -360 and +360
                // because we need to be able to express angle difference with this also
                // and the difference can be > +180 or < -180, depending on values of
                // lhs and rhs of subtraction operation
                if (Math.abs(angle) == 360) {
                    _angleDeg = angle;
                } else {
                    _angleDeg = angle % 360;
                }
                _angleRad = Math.radians(_angleDeg);
            }

            // Angle can be defined via a 2D space point as an
            // andgle between vector [origin, Point] and X-axis
            if (angle instanceof Point) {
                var pt = angle;

                // We round to precision so we can compare with 0
                var length = roundToPrecision(pt.length());
                if (length != 0) {
                    _angleRad = Math.atan2(pt.y, pt.x);
                } else {
                    _angleRad = 0;
                }
                _angleDeg = Math.degrees(_angleRad);
            }

            _angleRad = roundToPrecision(_angleRad);
            _angleDeg = roundToPrecision(_angleDeg);
            _sin = Math.sin(_angleRad);
            _cos = Math.cos(_angleRad);
        }

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            angle: {
                enumerable: true,
                get: function get() {
                    return _angleDeg;
                },
                set: function set(value) {
                    setAngle(value);
                }
            },
            sin: {
                enumerable: true,
                get: function get() {
                    return _sin;
                }
            },
            cos: {
                enumerable: true,
                get: function get() {
                    return _cos;
                }
            },

            angleRadians: {
                enumerable: true,
                get: function get() {
                    return _angleRad;
                }
            }
        });

        //
        // constructor code
        //
        setAngle(param);
    }

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Angle.prototype, {
        neg: function neg() {
            return new Angle(new Point(this.cos, -this.sin));
        },

        add: function add(other) {
            if (!other instanceof Angle) {
                return new Angle(this);
            }

            return new Angle(this.angle + other.angle);
        },

        sub: function sub(other) {
            if (!other instanceof Angle) {
                return new Angle(this);
            }

            return new Angle(this.angle - other.angle);
        },

        /** Returns a point which is at 'length' distance from (0,0) and at an angle to X-axis equal to this angle */
        toPoint: function toPoint(length) {
            if (!Number.isFinite(length)) {
                // This assumes length == 1.0
                return new Point(this.cos, this.sin);
            }

            return new Point(this.cos * length, this.sin * length);
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * Base class for 2nd, 3rd and 4th order Bezier curve classes.
     * Contains general properties and methods.
     * This class is intended to be subclassed and not used directly.
     */
    function Bezier(points) {
        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _order = 0;
        var _points = [];

        var _start = new Point();
        var _end = new Point();
        var _cpoints = [];

        //
        // PRIVATE FUNCTIONS
        //
        var setOrderAndPoints = function setOrderAndPoints(elements) {
            if (isPointArray(elements) && elements.length >= 2) {
                // unpack Point array to array of coordinates
                elements = pointArrayToNumeric(elements);
            }

            if (!isNumericArray(elements)) {
                return;
            }

            if (elements.length < 4 || elements.length > 8 || elements.length % 2 != 0) {
                return;
            }

            _order = elements.length / 2;
            _points = elements.slice();
        };

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            order: {
                get: function get() {
                    return _order;
                }
            },
            points: {
                get: function get() {
                    return _points;
                }
            },

            start: {
                get: function get() {
                    return _start;
                },
                set: function set(value) {
                    _start = new Point(value);
                }
            },
            end: {
                get: function get() {
                    return _end;
                },
                set: function set(value) {
                    _end = new Point(value);
                }
            },
            cpoints: {
                get: function get() {
                    return _cpoints;
                },
                set: function set(value) {
                    if (isPointArray(value)) _cpoints = value;
                }
            }
        });

        //
        // constructor code
        //
        setOrderAndPoints(points);
        this.createControlPoints();
    }

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Bezier.prototype, {
        toString: function toString() {
            var str = 'Bezier' + String(this.order) + ' : ';
            if (this.order >= 2) {
                str += this.P0 + ', ' + this.P1;
            }
            if (this.order >= 3) {
                str += ', ' + this.P2;
            }
            if (this.order >= 4) {
                str += ', ' + this.P3;
            }
            return str;
        },

        /**
         * P0, P1, P2 and P3 should be declared in subclasses.
         * If they're not, this method will create them,
         * otherwise it will overwrite/update the existing ones.
         */
        createControlPoints: function createControlPoints() {
            if (this.order >= 2) {
                // Linear curve
                this.P0 = new Point(this.points[0], this.points[1]);
                this.P1 = new Point(this.points[2], this.points[3]);

                this.start = this.P0;
                this.end = this.P1;
                this.cpoints = [];
            }
            if (this.order >= 3) {
                // Quadratic curve
                this.P2 = new Point(this.points[4], this.points[5]);

                this.end = this.P2;
                this.cpoints = [this.P1];
            }
            if (this.order >= 4) {
                // Cubic curve
                this.P3 = new Point(this.points[6], this.points[7]);

                this.end = this.P3;
                this.cpoints = [this.P1, this.P2];
            }
        },

        // Override in subclass
        _interpolate: function _interpolate(t) {
            return new Point();
        },

        evaluate: function evaluate(t) {
            if (t < 0) {
                return this.start;
            }
            if (t > 1) {
                return this.end;
            }

            return this._interpolate(t);
        },

        // Override in subclass
        transform: function transform(matrix) {
            return new Bezier(this.points);
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * Implements linear Bezier curve
     */
    function Bezier4(points) {
        //
        // Case when Bezier4 is called as a function
        //
        if (!(this instanceof Bezier4)) {
            return new Bezier4(points);
        }

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _P0 = new Point();
        var _P1 = new Point();
        var _P2 = new Point();
        var _P3 = new Point();

        //
        // PRIVATE FUNCTIONS
        //

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            P0: {
                enumerable: true,
                get: function get() {
                    return _P0;
                },
                set: function set(value) {
                    _P0 = new Point(value);
                }
            },
            P1: {
                enumerable: true,
                get: function get() {
                    return _P1;
                },
                set: function set(value) {
                    _P1 = new Point(value);
                }
            },
            P2: {
                enumerable: true,
                get: function get() {
                    return _P2;
                },
                set: function set(value) {
                    _P2 = new Point(value);
                }
            },
            P3: {
                enumerable: true,
                get: function get() {
                    return _P3;
                },
                set: function set(value) {
                    _P3 = new Point(value);
                }
            }
        });

        //
        // constructor code
        //
        Bezier.call(this, points);
    }

    //
    // CLASS INHERITANCE
    //
    Bezier4.prototype = Object.assign(Object.create(Bezier.prototype), {
        constructor: Bezier4
    });

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Bezier4.prototype, {
        toString: function toString() {
            return 'Bezier Q : ' + this.P0 + ', ' + this.P1 + ', ' + this.P2 + ', ' + this.P3;
        },

        // Called by Bezier.evaluate(t)s
        // Bernstein polinomial evaluation
        _interpolate: function _interpolate(t) {
            // B( t ) =
            //      ( 1 - t ) ^ 3 * P0 +
            //      3 * ( 1 - t ) ^ 2 * t * P1 +
            //      3 * ( 1 - t ) * t ^ 2 * P2 + 
            //      t ^ 3 * P3

            // ( 1 - t ) ^ 3 * P0
            var A = this.P0.mul(Math.pow(1 - t, 3));
            // 3 * ( 1 - t ) ^ 2 * t * P1
            var B = this.P1.mul(3 * (1 - t) * (1 - t) * t);
            // 3 * ( 1 - t ) * t ^ 2 * P2
            var C = this.P2.mul(3 * (1 - t) * t * t);
            // t ^ 3 * P3
            var D = this.P3.mul(Math.pow(t, 3));

            // B( t ) = A + B + C + D
            var pt = A.add(B).add(C).add(D);

            return pt;
        },

        transform: function transform(matrix) {
            return new Bezier4([matrix.mul(this.P0), matrix.mul(this.P1), matrix.mul(this.P2), matrix.mul(this.P3)]);
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    // https://www.w3.org/TR/SVG/paths.html#PathDataEllipticalArcCommands
    // https://www.w3.org/TR/SVG/implnote.html#ArcImplementationNotes
    /**
    * params -> [ cx, cy, rx, ry, start-angle, delta-angle, x-axis-rotation ]
    */
    function Arc(params) {
        //
        // Case when Arc is called as a function
        //
        if (!(this instanceof Arc)) {
            return new Arc(params);
        }

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _params = [0, 0, 1, 1, 0, 90, 0];
        var _Pc = new Point();
        var _rx = 1;
        var _ry = 1;
        var _startAngle = new Angle(0);
        var _deltaAngle = new Angle(90);
        var _xAxisRotation = new Angle(0);

        var _transformationMatrix = new Matrix();

        //
        // PRIVATE FUNCTIONS
        //

        var parseParams = function parseParams() {
            var cx = void 0,
                cy = void 0;
            var _params2 = _params;

            var _params3 = slicedToArray(_params2, 7);

            cx = _params3[0];
            cy = _params3[1];
            _rx = _params3[2];
            _ry = _params3[3];
            _startAngle = _params3[4];
            _deltaAngle = _params3[5];
            _xAxisRotation = _params3[6];


            if (Math.abs(_deltaAngle) < 0.0001) {
                // TODO: degenerate to a Point() instead?
                throw new Error("arc is too small!");
            }

            _Pc = new Point(cx, cy);
            _startAngle = new Angle(_startAngle);
            _deltaAngle = new Angle(_deltaAngle);
            _xAxisRotation = new Angle(_xAxisRotation);

            _transformationMatrix = _transformationMatrix.translate(cx, cy);
            _transformationMatrix = _transformationMatrix.rotate(_xAxisRotation.angle);
            _transformationMatrix = _transformationMatrix.scale(_rx, _ry);
        };

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            params: {
                get: function get$$1() {
                    return _params;
                }
            },
            center: {
                enumerable: true,
                get: function get$$1() {
                    return _Pc;
                }
            },
            rx: {
                enumerable: true,
                get: function get$$1() {
                    return _rx;
                }
            },
            ry: {
                enumerable: true,
                get: function get$$1() {
                    return _ry;
                }
            },
            start: {
                enumerable: true,
                get: function get$$1() {
                    return _startAngle;
                }
            },
            delta: {
                enumerable: true,
                get: function get$$1() {
                    return _deltaAngle;
                }
            },
            xAxisRotation: {
                enumerable: true,
                get: function get$$1() {
                    return _xAxisRotation;
                }
            },
            matrix: {
                get: function get$$1() {
                    return _transformationMatrix;
                }
            }
        });

        //
        // constructor code
        //
        if (!isNumericArray(params)) {
            return;
        }
        if (params.length < 7) {
            return;
        }

        _params = params;
        parseParams();
    }

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Arc.prototype, {
        toString: function toString() {
            return 'Arc: ' + this.propBar;
        },

        /*
            Outline of SVG Path arc conversion:
        
            given: x1, y1, rx, ry, x-axis-rotation, fA, fS, x2, y2
                - use createFromSVGParameters() to find cx, cy, startAngle and endAngle
                let arc = Arc.createFromSVGParameters( x1, y1, rx, ry, phi, fA, fS, x2, y2 )
        
            arc.convert():
            - arc has:
                - center point
                - both axes: rx, ry
                - start angle
                - end angle
                - x-axis angle
        
            - simplify calculation down to the circle:
                - center (0,0)
                - radius (1.0)
                - start angle
                - end angle
        
            - all other values are accumulated in tranformation matrix (in this order):
                - rotate by x-axis-angle
                - center point -> translate to Pc
                - rx,ry -> scaleX rx, scaleY ry
        
            - use approximateArc() on the simplified circle to get array of Bezier points
            - apply transformation matrix to all the points and return as result
        
        */
        convert: function convert(matrix) {
            if (!(matrix instanceof Matrix)) {
                matrix = new Matrix();
            }

            var Pc = new Point(0, 0);
            var R = 1.0;
            var Pstart = this.start.toPoint(R);

            var arrayOfBeziers = Arc.approximateArc(Pstart, this.delta.angle, Pc);

            var ctm = this.matrix.mul(matrix);

            return arrayOfBeziers.map(function (val) {
                return val.transform(ctm);
            });
        }
    });

    //
    // PUBLIC "STATIC" CLASS METHODS
    //
    Object.assign(Arc, {
        /**
        * Used when parsing SVG Path 'd' attribute. Implementation based on notes from
        * https://www.w3.org/TR/SVG/implnote.html#ArcConversionEndpointToCenter
        * parameters: last point x1, y1, Path Arc parameters: rx ry x-axis-rotation large-arc-flag sweep-flag x y
        */
        createFromSVGParameters: function createFromSVGParameters(x1, y1, rx, ry, phi, fA, fS, x2, y2) {
            var P1 = new Point(x1, y1);
            var P2 = new Point(x2, y2);

            // START - parameter checking
            // https://www.w3.org/TR/SVG/implnote.html#ArcOutOfRangeParameters
            if (P1.sub(P2).length() < 0.0001) {
                throw new Error("arc points too close");
                // return undefined
            }
            if (rx == 0 || ry == 0) {
                throw new Error("radius == 0");
                // return Bezier2()
            }
            rx = Math.abs(rx);
            ry = Math.abs(ry);
            phi %= 360;
            // END - parameter checking

            // Step 1
            // Un-rotate the mid-point between P1 and P2
            var P1p = P1.sub(P2).mul(0.5).rotate(-phi);

            // https://www.w3.org/TR/SVG/implnote.html#ArcCorrectionOutOfRangeRadii
            // Make sure we have at least one solution
            var lambda = P1p.x * P1p.x / (rx * rx) + P1p.y * P1p.y / (ry * ry);
            if (lambda > 1.0) {
                lambda = Math.sqrt(lambda);
                rx *= lambda;
                ry *= lambda;
            }

            // Step 2
            // Find ellipse center point
            var cxp = rx * P1p.y / ry;
            var cyp = -ry * P1p.x / rx;
            var coef = Math.sqrt((rx * rx * ry * ry - rx * rx * P1p.y * P1p.y - ry * ry * P1p.x * P1p.x) / (rx * rx * P1p.y * P1p.y + ry * ry * P1p.x * P1p.x));
            if (fA == fS) {
                coef = -coef;
            }
            cxp *= coef;
            cyp *= coef;

            // Step 3
            // rotate back the center to it's correct position (to undo what we did in Step 1)
            var Pcp = new Point(cxp, cyp).rotate(phi);
            var Pc = Pcp.rotate(phi).add(P1.add(P2).mul(0.5));

            // Step 4
            // Find starting angle (for P1) and arc angle (the angle between P1 and P2)
            // by using points in 'un-rotated' space (P1p and cxp, cyp)
            var unitX = new Point(1, 0);
            var first = new Point((P1p.x - cxp) / rx, (P1p.y - cyp) / ry);
            var theta1 = unitX.angle(first);

            var second = new Point((-P1p.x - cxp) / rx, (-P1p.y - cyp) / ry);
            var thetaD = first.angle(second);

            if (fS == 0 && thetaD > 0) {
                thetaD -= 360;
            }
            if (fS == 1 && thetaD < 0) {
                thetaD += 360;
            }

            return new Arc([Pc.x, Pc.y, rx, ry, theta1, thetaD, phi]);
        },

        /**
         * Approximates circular arc with center in Pc and start point Pstart.
         * Arc spans deltaAngle degrees: clockwise if deltaAngle > 0,
         * and counterclockwise if deltaAngle < 0.
         *
         */
        approximateArc: function approximateArc(Pstart, deltaAngle, Pc) {
            var absAngle = Math.abs(deltaAngle);
            if (absAngle < 0.0001) {
                // TODO: degenerate to a Point() instead ?
                throw new Error("Arc is too small!");
            }
            var angleSign = Math.sign(deltaAngle);

            var translatePoints = function translatePoints(points, targetPoint) {
                var newPoints = [];
                for (var idx = 0; idx < points.length; idx++) {
                    newPoints.push(points[idx].add(targetPoint));
                }

                return newPoints;
            };

            var normalizeAngle = function normalizeAngle(angle) {
                if (angle < 0) {
                    angle += 360;
                }

                return angle;
            };

            // translate to (0,0)
            Pstart = Pstart.sub(Pc);

            // We need to have angles in range 0 < angle < 360 in order
            // to properly calculate number of 90-deg arc segments
            var startAngle = normalizeAngle(Pstart.angle());

            var numberOfQuarters = Math.floor(absAngle / 90);
            var restOfTheArc = absAngle - numberOfQuarters * 90;

            var result = [];
            var currentPoint = Pstart;
            var angle90 = angleSign * 90;
            restOfTheArc = angleSign * restOfTheArc;
            for (var n = 0; n < numberOfQuarters; n++) {
                var _points = Arc.approximateArcBasic(currentPoint, angle90);

                _points = translatePoints(_points, Pc);
                result.push(new Bezier4(_points));

                currentPoint = currentPoint.rotate(angle90);
            }

            var points = Arc.approximateArcBasic(currentPoint, restOfTheArc);
            points = translatePoints(points, Pc);
            result.push(new Bezier4(points));

            return result;
        },

        // http://stackoverflow.com/a/40327968
        approximateArcBasic: function approximateArcBasic(Pstart, angle) {
            /*
                parameters: (Pstart, angle)
                    - assume (xc,yc) = (0,0)
                    - then R = Pstart.length()
                    - -90 < angle < 90
                    - arc starts at Pstart
                 we'll calculate bezier points for case when P0 is at (R,0)
                and then just rotate them by angle between Pstart and X-axis
                 - assume P0 = (R,0)
                - find P3 for (angle): (Rsin(angle),Rcos(angle))
                - calculate Bezier4 points
                - rotate Bezier4 points by Pstart.angle(1,0)
             */
            angle %= 360;
            if (Math.abs(angle) > 90) {
                // divide and conquer?
                // reject the case?
                throw new Error('approximateArcBasic: angle > +/-90 degrees');
            }

            var R = Pstart.length();
            var P0 = Point(R, 0);
            var P3 = P0.rotate(angle);

            // 0 < phi < PI/4
            // which means:
            // 0 < angle < 90
            var phi = Math.radians(angle / 2);

            // text on SO has error here
            // missing '/' between '3' and '('
            var k = 4 / 3 / (1 / Math.cos(phi) + 1);
            // let k = 0.55191502449

            // PM = (P0 + P3) / 2
            var PM = P0.add(P3).mul(0.5);

            // PH = PM / cos(x) = PM sec(x) = (P0 + P3) sec(x) / 2
            var PH = PM.mul(1 / Math.cos(phi));

            // PE = PH / cos(x) = PM sec(x)^2 = (P0 + P3) sec(x)^2 / 2
            var PE = PH.mul(1 / Math.cos(phi));

            // P1 = (1 - k)P0 + k PE
            // let P1 = P0.mul( 1 - k ).add( PE.mul( k ) )
            // or
            // P1 = P0 + k (PE - P0)
            var P1 = P0.add(PE.sub(P0).mul(k));

            // P2 = (1 - k)P3 + k PE
            // let P2 = P3.mul( 1 - k ).add( PE.mul( k ) )
            // or
            // P2 = P3 + k (PE - P3)
            var P2 = P3.add(PE.sub(P3).mul(k));

            var backAngle = Pstart.angle();
            P0 = P0.rotate(backAngle);
            P1 = P1.rotate(backAngle);
            P2 = P2.rotate(backAngle);
            P3 = P3.rotate(backAngle);

            return [P0, P1, P2, P3];
        }

    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * Implements linear Bezier curve
     */
    function Bezier2(points) {
        //
        // Case when Bezier2 is called as a function
        //
        if (!(this instanceof Bezier2)) {
            return new Bezier2(points);
        }

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _P0 = new Point();
        var _P1 = new Point();

        //
        // PRIVATE FUNCTIONS
        //

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            P0: {
                enumerable: true,
                get: function get() {
                    return _P0;
                },
                set: function set(value) {
                    _P0 = new Point(value);
                }
            },
            P1: {
                enumerable: true,
                get: function get() {
                    return _P1;
                },
                set: function set(value) {
                    _P1 = new Point(value);
                }
            }
        });

        //
        // constructor code
        //
        Bezier.call(this, points);
    }

    //
    // CLASS INHERITANCE
    //
    Bezier2.prototype = Object.assign(Object.create(Bezier.prototype), {
        constructor: Bezier2
    });

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Bezier2.prototype, {
        toString: function toString() {
            return 'Bezier L : ' + this.P0 + ', ' + this.P1;
        },

        // Called by Bezier.evaluate(t)s
        // Bernstein polinomial evaluation
        _interpolate: function _interpolate(t) {
            // B( t ) =
            //      ( 1 - t ) * P0 +
            //      t * P1

            // ( 1 - t ) * P0
            var A = this.P0.mul(1 - t);
            // t * P1
            var B = this.P1.mul(t);

            // B( t ) = A + B
            var pt = A.add(B);

            return pt;
        },

        transform: function transform(matrix) {
            return new Bezier2([matrix.mul(this.P0), matrix.mul(this.P1)]);
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * Implements linear Bezier curve
     */
    function Bezier3(points) {
        //
        // Case when Bezier3 is called as a function
        //
        if (!(this instanceof Bezier3)) {
            return new Bezier3(points);
        }

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _P0 = new Point();
        var _P1 = new Point();
        var _P2 = new Point();

        //
        // PRIVATE FUNCTIONS
        //

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            P0: {
                enumerable: true,
                get: function get() {
                    return _P0;
                },
                set: function set(value) {
                    _P0 = new Point(value);
                }
            },
            P1: {
                enumerable: true,
                get: function get() {
                    return _P1;
                },
                set: function set(value) {
                    _P1 = new Point(value);
                }
            },
            P2: {
                enumerable: true,
                get: function get() {
                    return _P2;
                },
                set: function set(value) {
                    _P2 = new Point(value);
                }
            }
        });

        //
        // constructor code
        //
        Bezier.call(this, points);
    }

    //
    // CLASS INHERITANCE
    //
    Bezier3.prototype = Object.assign(Object.create(Bezier.prototype), {
        constructor: Bezier3
    });

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Bezier3.prototype, {
        toString: function toString() {
            return 'Bezier Q : ' + this.P0 + ', ' + this.P1 + ', ' + this.P2;
        },

        // Called by Bezier.evaluate(t)s
        // Bernstein polinomial evaluation
        _interpolate: function _interpolate(t) {
            // B( t ) =
            //      ( 1 - t ) ^ 2 * P0 +
            //      2 * ( 1 - t ) * t * P1 +
            //      t ^ 2 * P2

            // ( 1 - t ) ^ 2 * P0
            var A = this.P0.mul((1 - t) * (1 - t));
            // 2 * ( 1 - t ) * t * P1
            var B = this.P1.mul(2 * (1 - t) * t);
            // t ^ 2 * P2
            var C = this.P2.mul(t * t);

            // B( t ) = A + B + C
            var pt = A.add(B).add(C);

            return pt;
        },

        transform: function transform(matrix) {
            return new Bezier3([matrix.mul(this.P0), matrix.mul(this.P1), matrix.mul(this.P2)]);
        }
    });

    /**
        Direct conversion of code from http://antigrain.com/research/adaptive_bezier/#toc0009
    */

    var flattenBezier3 = function flattenBezier3(x1, y1, x2, y2, x3, y3) {

        //------------------------------------------------------------------------
        var curve_collinearity_epsilon = 1e-30;
        var curve_angle_tolerance_epsilon = 0.01;
        var curve_recursion_limit = 32;

        var m_points = [];
        var m_count = 0;
        var m_approximation_scale = 1.0;
        var m_angle_tolerance = 0.0;

        var m_distance_tolerance;

        var init = function init(x1, y1, x2, y2, x3, y3) {
            m_points = [];
            m_distance_tolerance = 0.5 / m_approximation_scale;
            m_distance_tolerance *= m_distance_tolerance;
            bezier(x1, y1, x2, y2, x3, y3);
            m_count = 0;
        };

        //------------------------------------------------------------------------
        var recursive_bezier = function recursive_bezier(x1, y1, x2, y2, x3, y3, level) {
            if (level > curve_recursion_limit) {
                return;
            }

            // Calculate all the mid-points of the line segments
            //----------------------
            var x12 = (x1 + x2) / 2;
            var y12 = (y1 + y2) / 2;
            var x23 = (x2 + x3) / 2;
            var y23 = (y2 + y3) / 2;
            var x123 = (x12 + x23) / 2;
            var y123 = (y12 + y23) / 2;

            var dx = x3 - x1;
            var dy = y3 - y1;
            var d = Math.abs((x2 - x3) * dy - (y2 - y3) * dx);

            if (d > curve_collinearity_epsilon) {
                // Regular care
                //-----------------
                if (d * d <= m_distance_tolerance * (dx * dx + dy * dy)) {
                    // If the curvature doesn't exceed the distance_tolerance value
                    // we tend to finish subdivisions.
                    //----------------------
                    if (m_angle_tolerance < curve_angle_tolerance_epsilon) {
                        m_points.push(SVGRX.Point(x123, y123));
                        return;
                    }

                    // Angle & Cusp Condition
                    //----------------------
                    var da = Math.abs(Math.atan2(y3 - y2, x3 - x2) - Math.atan2(y2 - y1, x2 - x1));
                    if (da >= pi) da = 2 * pi - da;

                    if (da < m_angle_tolerance) {
                        // Finally we can stop the recursion
                        //----------------------
                        m_points.push(SVGRX.Point(x123, y123));
                        return;
                    }
                }
            } else {
                // Collinear case
                //-----------------
                dx = x123 - (x1 + x3) / 2;
                dy = y123 - (y1 + y3) / 2;
                if (dx * dx + dy * dy <= m_distance_tolerance) {
                    m_points.push(SVGRX.Point(x123, y123));
                    return;
                }
            }

            // Continue subdivision
            //----------------------
            recursive_bezier(x1, y1, x12, y12, x123, y123, level + 1);
            recursive_bezier(x123, y123, x23, y23, x3, y3, level + 1);
        };

        //------------------------------------------------------------------------
        var bezier = function bezier(x1, y1, x2, y2, x3, y3) {
            m_points.push(SVGRX.Point(x1, y1));
            recursive_bezier(x1, y1, x2, y2, x3, y3, 0);
            m_points.push(SVGRX.Point(x3, y3));
        };

        init(x1, y1, x2, y2, x3, y3);
        return m_points;
    };

    /**
        Direct conversion of code from http://antigrain.com/research/adaptive_bezier/#toc0009
    */

    var flattenBezier4 = function flattenBezier4(x1, y1, x2, y2, x3, y3, x4, y4) {

        //------------------------------------------------------------------------
        var curve_collinearity_epsilon = 1e-30;
        var curve_angle_tolerance_epsilon = 0.01;
        var curve_recursion_limit = 32;

        var m_points = [];
        var m_count = 0;
        var m_approximation_scale = 1.0;
        var m_angle_tolerance = 0.0;
        var m_cusp_limit = 0.0;

        var m_distance_tolerance;

        var init = function init(x1, y1, x2, y2, x3, y3, x4, y4) {
            m_points = [];
            m_distance_tolerance = 0.5 / m_approximation_scale;
            m_distance_tolerance *= m_distance_tolerance;
            bezier(x1, y1, x2, y2, x3, y3, x4, y4);
            m_count = 0;
        };

        //------------------------------------------------------------------------
        var recursive_bezier = function recursive_bezier(x1, y1, x2, y2, x3, y3, x4, y4, level) {
            if (level > curve_recursion_limit) {
                return;
            }

            // Calculate all the mid-points of the line segments
            //----------------------
            var x12 = (x1 + x2) / 2;
            var y12 = (y1 + y2) / 2;
            var x23 = (x2 + x3) / 2;
            var y23 = (y2 + y3) / 2;
            var x34 = (x3 + x4) / 2;
            var y34 = (y3 + y4) / 2;
            var x123 = (x12 + x23) / 2;
            var y123 = (y12 + y23) / 2;
            var x234 = (x23 + x34) / 2;
            var y234 = (y23 + y34) / 2;
            var x1234 = (x123 + x234) / 2;
            var y1234 = (y123 + y234) / 2;

            if (level > 0) // Enforce subdivision first time
                {
                    // Try to approximate the full cubic curve by a single straight line
                    //------------------
                    var dx = x4 - x1;
                    var dy = y4 - y1;

                    var d2 = Math.abs((x2 - x4) * dy - (y2 - y4) * dx);
                    var d3 = Math.abs((x3 - x4) * dy - (y3 - y4) * dx);

                    var da1, da2;

                    if (d2 > curve_collinearity_epsilon && d3 > curve_collinearity_epsilon) {
                        // Regular care
                        //-----------------
                        if ((d2 + d3) * (d2 + d3) <= m_distance_tolerance * (dx * dx + dy * dy)) {
                            // If the curvature doesn't exceed the distance_tolerance value
                            // we tend to finish subdivisions.
                            //----------------------
                            if (m_angle_tolerance < curve_angle_tolerance_epsilon) {
                                m_points.push(SVGRX.Point(x1234, y1234));
                                return;
                            }

                            // Angle & Cusp Condition
                            //----------------------
                            var a23 = Math.atan2(y3 - y2, x3 - x2);
                            da1 = Math.abs(a23 - Math.atan2(y2 - y1, x2 - x1));
                            da2 = Math.abs(Math.atan2(y4 - y3, x4 - x3) - a23);
                            if (da1 >= pi) da1 = 2 * pi - da1;
                            if (da2 >= pi) da2 = 2 * pi - da2;

                            if (da1 + da2 < m_angle_tolerance) {
                                // Finally we can stop the recursion
                                //----------------------
                                m_points.push(SVGRX.Point(x1234, y1234));
                                return;
                            }

                            if (m_cusp_limit != 0.0) {
                                if (da1 > m_cusp_limit) {
                                    m_points.push(SVGRX.Point(x2, y2));
                                    return;
                                }

                                if (da2 > m_cusp_limit) {
                                    m_points.push(SVGRX.Point(x3, y3));
                                    return;
                                }
                            }
                        }
                    } else {
                        if (d2 > curve_collinearity_epsilon) {
                            // p1,p3,p4 are collinear, p2 is considerable
                            //----------------------
                            if (d2 * d2 <= m_distance_tolerance * (dx * dx + dy * dy)) {
                                if (m_angle_tolerance < curve_angle_tolerance_epsilon) {
                                    m_points.push(SVGRX.Point(x1234, y1234));
                                    return;
                                }

                                // Angle Condition
                                //----------------------
                                da1 = Math.abs(Math.atan2(y3 - y2, x3 - x2) - Math.atan2(y2 - y1, x2 - x1));
                                if (da1 >= pi) da1 = 2 * pi - da1;

                                if (da1 < m_angle_tolerance) {
                                    m_points.push(SVGRX.Point(x2, y2));
                                    m_points.push(SVGRX.Point(x3, y3));
                                    return;
                                }

                                if (m_cusp_limit != 0.0) {
                                    if (da1 > m_cusp_limit) {
                                        m_points.push(SVGRX.Point(x2, y2));
                                        return;
                                    }
                                }
                            }
                        } else if (d3 > curve_collinearity_epsilon) {
                            // p1,p2,p4 are collinear, p3 is considerable
                            //----------------------
                            if (d3 * d3 <= m_distance_tolerance * (dx * dx + dy * dy)) {
                                if (m_angle_tolerance < curve_angle_tolerance_epsilon) {
                                    m_points.push(SVGRX.Point(x1234, y1234));
                                    return;
                                }

                                // Angle Condition
                                //----------------------
                                da1 = Math.abs(Math.atan2(y4 - y3, x4 - x3) - Math.atan2(y3 - y2, x3 - x2));
                                if (da1 >= pi) da1 = 2 * pi - da1;

                                if (da1 < m_angle_tolerance) {
                                    m_points.push(SVGRX.Point(x2, y2));
                                    m_points.push(SVGRX.Point(x3, y3));
                                    return;
                                }

                                if (m_cusp_limit != 0.0) {
                                    if (da1 > m_cusp_limit) {
                                        m_points.push(SVGRX.Point(x3, y3));
                                        return;
                                    }
                                }
                            }
                        } else {
                            // Collinear case
                            //-----------------
                            dx = x1234 - (x1 + x4) / 2;
                            dy = y1234 - (y1 + y4) / 2;
                            if (dx * dx + dy * dy <= m_distance_tolerance) {
                                m_points.push(SVGRX.Point(x1234, y1234));
                                return;
                            }
                        }
                    }
                }

            // Continue subdivision
            //----------------------
            recursive_bezier(x1, y1, x12, y12, x123, y123, x1234, y1234, level + 1);
            recursive_bezier(x1234, y1234, x234, y234, x34, y34, x4, y4, level + 1);
        };

        //------------------------------------------------------------------------
        var bezier = function bezier(x1, y1, x2, y2, x3, y3, x4, y4) {
            m_points.push(SVGRX.Point(x1, y1));
            recursive_bezier(x1, y1, x2, y2, x3, y3, x4, y4, 0);
            m_points.push(SVGRX.Point(x4, y4));
        };

        init(x1, y1, x2, y2, x3, y3, x4, y4);
        return m_points;
    };

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * https://www.w3.org/TR/SVG/struct.html#SVGElement
     */
    function Svg(param, docText) {
        //
        // Case when Svg is called as a function
        //
        if (!(this instanceof Svg)) {
            return new Svg(param, docText);
        }

        BaseElement.call(this, param);

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _x = 0;
        var _y = 0;
        var _width = 1280;
        var _height = 1024;
        var _version = '1.1';

        var _docText = '';

        //
        // PRIVATE FUNCTIONS
        //
        var setPropertiesFromAttributes = function setPropertiesFromAttributes(attributes) {
            _x = attributes['x'] || _x;
            _y = attributes['y'] || _y;
            _width = attributes['width'] || _width;
            _height = attributes['height'] || _height;
            _version = attributes['version'] || _version;
        };

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            x: {
                get: function get$$1() {
                    return _x;
                }
            },
            y: {
                get: function get$$1() {
                    return _y;
                }
            },
            width: {
                get: function get$$1() {
                    return _width;
                }
            },
            height: {
                get: function get$$1() {
                    return _height;
                }
            },
            version: {
                get: function get$$1() {
                    return _version;
                }
            },

            source: {
                get: function get$$1() {
                    return _docText;
                }
            }
        });

        //
        // constructor code
        //
        if (typeof docText === 'string') {
            _docText = docText;
        } else if ((typeof param === 'undefined' ? 'undefined' : _typeof(param)) === 'object') {
            setPropertiesFromAttributes(this.attrs);
        }

        //
        // initialize private property accessors here
        //
    }

    //
    // INHERIT FROM CLASS WHICH INHERITS FROM OTHER CLASSES
    //
    // https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Object/create#Classical_inheritance_with_Object.create()
    Svg.prototype = Object.assign(Object.create(BaseElement.prototype), {
        constructor: Svg,
        isSvg: true
    });

    //
    // PUBLIC CLASS METHODS
    //
    // https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Object/create#Using_propertiesObject_argument_with_Object.create()
    Object.assign(Svg.prototype, {
        toString: function toString() {
            return '<Svg ' + this.id + '>';
        },

        convert: function convert(matrix) {
            var result = [];

            if (this.children.length > 0) {
                var ctm = new Matrix();
                ctm = ctm.translate(this.x, this.y);
                ctm = ctm.mul(matrix);

                for (var idx = 0; idx < this.children.length; idx++) {
                    var c = this.children[idx];
                    var r = c.convert(ctm);
                    result = result.concat(r);
                }
            }

            return result;
        },

        flatten: function flatten(matrix) {
            var result = [];
            var converted = this.convert(matrix);

            for (var idx = 0; idx < converted.length; idx++) {
                var points = [];
                var el = converted[idx];
                if (el instanceof SVGRX.Bezier2) {
                    // line
                    points = [el.P0, el.P1];
                } else if (el instanceof SVGRX.Bezier3) {
                    // quadratic
                    points = SVGRX.flattenBezier3(el.P0.x, el.P0.y, el.P1.x, el.P1.y, el.P2.x, el.P2.y);
                } else if (el instanceof SVGRX.Bezier4) {
                    // cubic
                    points = SVGRX.flattenBezier4(el.P0.x, el.P0.y, el.P1.x, el.P1.y, el.P2.x, el.P2.y, el.P3.x, el.P3.y);
                }

                result.push(points);
            }

            return result;
        }
    });

    var CreateFromString = function CreateFromString(content) {
        var createObject = function createObject(tagName, attrs) {

            switch (tagName) {
                // containers
                case 'g':
                    return new SVGRX.Group(attrs);
                    break;

                case 'svg':
                    return new SVGRX.Svg(attrs);
                    break;

                // graphics
                case 'circle':
                    return new SVGRX.Circle(attrs);
                    break;

                case 'ellipse':
                    return new SVGRX.Ellipse(attrs);
                    break;

                case 'line':
                    return new SVGRX.Line(attrs);
                    break;

                case 'path':
                    return new SVGRX.Path(attrs);
                    break;

                case 'polygon':
                    return new SVGRX.Polygon(attrs);
                    break;

                case 'polyline':
                    return new SVGRX.Polyline(attrs);
                    break;

                case 'rect':
                    return new SVGRX.Rect(attrs);
                    break;

                default:
                    return null;
                    break;
            }
        };

        var getAttributes = function getAttributes(child) {
            var attrs = {};

            if (child.hasAttributes()) {
                var attributes = child.attributes;

                for (var k = 0; k < attributes.length; k++) {
                    attrs[attributes[k].name] = attributes[k].value;
                }
            }

            return attrs;
        };

        var parseChildren = function parseChildren(children) {
            if (children.length == 0) return [];

            var result = [];

            for (var i = 0; i < children.length; i++) {
                var child = children[i];
                var attrs = getAttributes(child);

                var object = createObject(child.tagName, attrs);

                if (object) {
                    object.tag = child.tagName;
                    object.children = parseChildren(child.children);

                    result.push(object);
                } else {
                    console.log('unsupported tagName: ', child.tagName);
                }
            }

            return result;
        };

        var createNodes = function createNodes(doc) {
            var parsed = parseChildren(doc.children);
            // valid SVG XMLDocument should have only one child (the root node)
            // and that child should be the <svg> element
            if (parsed.length > 0) {
                return parsed[0];
            }

            return {};
        };

        var parser = new DOMParser();
        // returns a SVGDocument, which also is a Document.
        // https://developer.mozilla.org/en-US/docs/Web/API/Document
        var doc = parser.parseFromString(content, "image/svg+xml");
        // console.log(doc.children)

        if (doc instanceof XMLDocument) {
            var rootNode = createNodes(doc);
            return rootNode;
        }

        return null;
    };

    var CreateFromUrl = function CreateFromUrl(url) {
        var result = new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            // true - async
            // false - sync
            xhr.open('GET', url, true);

            // If specified, responseType must be empty string or "document"
            // only can set this for async requests
            // xhr.responseType = 'document'

            // overrideMimeType() can be used to force the response to be parsed as XML
            // xhr.overrideMimeType( 'text/xml' )
            xhr.overrideMimeType('text/plain');

            xhr.onload = function () {
                if (xhr.readyState === xhr.DONE) {
                    if (xhr.status === 200) {
                        var svgNode = CreateFromString(xhr.response);

                        if (svgNode instanceof SVGRX.Svg) {
                            resolve(svgNode);
                        }
                    }
                }

                reject(xhr);
            };

            xhr.send(null);
        });

        return result;
    };

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * https://www.w3.org/TR/SVG/struct.html#GElement
     * param - {} or Group
     */
    function Group(param) {
        //
        // Case when Group is called as a function
        //
        if (!(this instanceof Group)) {
            return new Group(param);
        }

        TransformableElement.call(this, param);

        //
        // PRIVATE OBJECT PROPERTIES
        //

        //
        // PRIVATE FUNCTIONS
        //
        var setPropertiesFromAttributes = function setPropertiesFromAttributes(attributes) {};

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {});

        //
        // constructor code
        //
        if (param instanceof Group) {
            setPropertiesFromAttributes(this.attrs);
        } else if ((typeof param === 'undefined' ? 'undefined' : _typeof(param)) === 'object') {
            setPropertiesFromAttributes(this.attrs);
        }
    }

    //
    // CLASS INHERITANCE
    //
    Group.prototype = Object.assign(Object.create(TransformableElement.prototype), {
        constructor: Group,
        isGroup: true
    });

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Group.prototype, {
        toString: function toString() {
            return '<Group ' + this.id + '>';
        },

        convert: function convert(matrix) {
            var result = [];

            if (this.children.length > 0) {
                var affineTransform = this.matrix.mul(matrix);

                for (var idx = 0; idx < this.children.length; idx++) {
                    var c = this.children[idx];
                    var r = c.convert(affineTransform);
                    result = result.concat(r);
                }
            }

            return result;
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * param - {...} or GraphicsElement
     */
    function GraphicsElement(param) {
        //
        // Case when GraphicsElement is called as a function
        //
        if (!(this instanceof GraphicsElement)) {
            return new GraphicsElement(param);
        }

        TransformableElement.call(this, param);

        //
        // PRIVATE OBJECT PROPERTIES
        //

        //
        // PRIVATE FUNCTIONS
        //
        var setPropertiesFromAttributes = function setPropertiesFromAttributes(attributes) {};

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {});

        //
        // constructor code
        //
        if (param instanceof GraphicsElement) {
            setPropertiesFromAttributes(this.attrs);
        } else if ((typeof param === 'undefined' ? 'undefined' : _typeof(param)) === 'object') {
            setPropertiesFromAttributes(this.attrs);
        }
    }

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(GraphicsElement.prototype, {
        toString: function toString() {
            return '<GraphicsElement ' + this.id + '>';
        }
    });

    //
    // CLASS INHERITANCE
    //
    GraphicsElement.prototype = Object.assign(Object.create(TransformableElement.prototype), {
        constructor: GraphicsElement,
        isGraphicsElement: true
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * https://www.w3.org/TR/SVG/shapes.html#CircleElement
     * param - {...} or Circle
     */
    function Circle(param) {
        //
        // Case when Circle is called as a function
        //
        if (!(this instanceof Circle)) {
            return new Circle(param);
        }

        GraphicsElement.call(this, param);

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _center = new Point(0, 0);
        var _r = 0;

        //
        // PRIVATE FUNCTIONS
        //
        var setPropertiesFromAttributes = function setPropertiesFromAttributes(attributes) {
            var cx = lengthInBaseUnits(attributes['cx']);
            var cy = lengthInBaseUnits(attributes['cy']);
            _r = lengthInBaseUnits(attributes['r']);
            _center = new Point(cx, cy);
        };

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            cx: {
                enumerable: true,
                get: function get$$1() {
                    return _center.x;
                }
            },
            cy: {
                enumerable: true,
                get: function get$$1() {
                    return _center.y;
                }
            },
            r: {
                enumerable: true,
                get: function get$$1() {
                    return _r;
                }
            },

            center: {
                get: function get$$1() {
                    return _center;
                }
            }
        });

        //
        // constructor code
        //
        if (param instanceof Circle) {
            _center = param.center;
            _r = param.r;
        } else if ((typeof param === 'undefined' ? 'undefined' : _typeof(param)) === 'object') {
            setPropertiesFromAttributes(this.attrs);
        }
    }

    //
    // CLASS INHERITANCE
    //
    Circle.prototype = Object.assign(Object.create(GraphicsElement.prototype), {
        constructor: Circle,
        isCircle: true
    });

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Circle.prototype, {
        toString: function toString() {
            return '<Circle ' + this.id + '>';
        },

        // http://spencermortensen.com/articles/bezier-circle/
        convert: function convert(matrix) {
            var _this = this;

            if (!(matrix instanceof Matrix)) {
                matrix = new Matrix();
            }

            var ctm = this.matrix.mul(matrix);
            var c = 0.55191502449;

            // circle centerd on (0, 0) with radius 1
            var segmentA = [Point(0, 1), Point(c, 1), Point(1, c), Point(1, 0)];
            var segmentB = [Point(1, 0), Point(1, -c), Point(c, -1), Point(0, -1)];
            var segmentC = [Point(0, -1), Point(-c, -1), Point(-1, -c), Point(-1, 0)];
            var segmentD = [Point(-1, 0), Point(-1, c), Point(-c, 1), Point(0, 1)];

            // scale segments to the circle radius size
            segmentA = segmentA.map(function (val) {
                return val.mul(_this.r);
            });
            segmentB = segmentB.map(function (val) {
                return val.mul(_this.r);
            });
            segmentC = segmentC.map(function (val) {
                return val.mul(_this.r);
            });
            segmentD = segmentD.map(function (val) {
                return val.mul(_this.r);
            });

            // translate segments to match the circle center (cx, cy)
            segmentA = segmentA.map(function (val) {
                return val.add(_this.center);
            });
            segmentB = segmentB.map(function (val) {
                return val.add(_this.center);
            });
            segmentC = segmentC.map(function (val) {
                return val.add(_this.center);
            });
            segmentD = segmentD.map(function (val) {
                return val.add(_this.center);
            });

            // convert to cubic bezier curves
            segmentA = new Bezier4(pointArrayToNumeric(segmentA));
            segmentB = new Bezier4(pointArrayToNumeric(segmentB));
            segmentC = new Bezier4(pointArrayToNumeric(segmentC));
            segmentD = new Bezier4(pointArrayToNumeric(segmentD));

            return [segmentA.transform(ctm), segmentB.transform(ctm), segmentC.transform(ctm), segmentD.transform(ctm)];
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * https://www.w3.org/TR/SVG/shapes.html#EllipseElement
     * param - {...} or Ellipse
     */
    function Ellipse(param) {
        //
        // Case when Ellipse is called as a function
        //
        if (!(this instanceof Ellipse)) {
            return new Ellipse(param);
        }

        GraphicsElement.call(this, param);

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _center = new Point(0, 0);
        var _rx = 0;
        var _ry = 0;

        //
        // PRIVATE FUNCTIONS
        //
        var setPropertiesFromAttributes = function setPropertiesFromAttributes(attributes) {
            var cx = lengthInBaseUnits(attributes['cx']);
            var cy = lengthInBaseUnits(attributes['cy']);
            _rx = lengthInBaseUnits(attributes['rx']);
            _ry = lengthInBaseUnits(attributes['ry']);
            _center = new Point(cx, cy);
        };

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            cx: {
                enumerable: true,
                get: function get$$1() {
                    return _center.x;
                }
            },
            cy: {
                enumerable: true,
                get: function get$$1() {
                    return _center.y;
                }
            },
            rx: {
                enumerable: true,
                get: function get$$1() {
                    return _rx;
                }
            },
            ry: {
                enumerable: true,
                get: function get$$1() {
                    return _ry;
                }
            },

            center: {
                get: function get$$1() {
                    return _center;
                }
            }
        });

        //
        // constructor code
        //
        if (param instanceof Ellipse) {
            _center = param.center;
            _rx = param.rx;
            _ry = param.ry;
        } else if ((typeof param === 'undefined' ? 'undefined' : _typeof(param)) === 'object') {
            setPropertiesFromAttributes(this.attrs);
        }
    }

    //
    // CLASS INHERITANCE
    //
    Ellipse.prototype = Object.assign(Object.create(GraphicsElement.prototype), {
        constructor: Ellipse,
        isEllipse: true
    });

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Ellipse.prototype, {
        toString: function toString() {
            return '<Ellipse ' + this.id + '>';
        },

        // http://spencermortensen.com/articles/bezier-circle/
        // applied to ellipse by using scale transform on circle
        convert: function convert(matrix) {
            var _this = this;

            if (!(matrix instanceof Matrix)) {
                matrix = new Matrix();
            }

            var ctm = this.matrix.mul(matrix);
            var c = 0.55191502449;

            // circle centerd on (0, 0) with radius 1
            var segmentA = [Point(0, 1), Point(c, 1), Point(1, c), Point(1, 0)];
            var segmentB = [Point(1, 0), Point(1, -c), Point(c, -1), Point(0, -1)];
            var segmentC = [Point(0, -1), Point(-c, -1), Point(-1, -c), Point(-1, 0)];
            var segmentD = [Point(-1, 0), Point(-1, c), Point(-c, 1), Point(0, 1)];

            // scale segments to the ellipse axes sizes
            var scaleMatrix = new Matrix().scale(this.rx, this.ry);
            segmentA = segmentA.map(function (val) {
                return scaleMatrix.mul(val);
            });
            segmentB = segmentB.map(function (val) {
                return scaleMatrix.mul(val);
            });
            segmentC = segmentC.map(function (val) {
                return scaleMatrix.mul(val);
            });
            segmentD = segmentD.map(function (val) {
                return scaleMatrix.mul(val);
            });

            // translate segments to match the ellipse center (cx, cy)
            segmentA = segmentA.map(function (val) {
                return val.add(_this.center);
            });
            segmentB = segmentB.map(function (val) {
                return val.add(_this.center);
            });
            segmentC = segmentC.map(function (val) {
                return val.add(_this.center);
            });
            segmentD = segmentD.map(function (val) {
                return val.add(_this.center);
            });

            // convert to cubic bezier curves
            segmentA = new Bezier4(pointArrayToNumeric(segmentA));
            segmentB = new Bezier4(pointArrayToNumeric(segmentB));
            segmentC = new Bezier4(pointArrayToNumeric(segmentC));
            segmentD = new Bezier4(pointArrayToNumeric(segmentD));

            return [segmentA.transform(ctm), segmentB.transform(ctm), segmentC.transform(ctm), segmentD.transform(ctm)];
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * https://www.w3.org/TR/SVG/shapes.html#LineElement
     * param - {...} or Line
     */
    function Line(param) {
        //
        // Case when Line is called as a function
        //
        if (!(this instanceof Line)) {
            return new Line(param);
        }

        GraphicsElement.call(this, param);

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _P1 = new Point(0, 0);
        var _P2 = new Point(0, 0);

        //
        // PRIVATE FUNCTIONS
        //
        var setPropertiesFromAttributes = function setPropertiesFromAttributes(attributes) {
            var x1 = lengthInBaseUnits(attributes['x1']);
            var y1 = lengthInBaseUnits(attributes['y1']);
            var x2 = lengthInBaseUnits(attributes['x2']);
            var y2 = lengthInBaseUnits(attributes['y2']);
            _P1 = new Point(x1, y1);
            _P2 = new Point(x2, y2);
        };

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            x1: {
                enumerable: true,
                get: function get$$1() {
                    return _P1.x;
                }
            },
            y1: {
                enumerable: true,
                get: function get$$1() {
                    return _P1.y;
                }
            },
            x2: {
                enumerable: true,
                get: function get$$1() {
                    return _P2.x;
                }
            },
            y2: {
                enumerable: true,
                get: function get$$1() {
                    return _P2.y;
                }
            },

            P1: {
                get: function get$$1() {
                    return _P1;
                }
            },
            P2: {
                get: function get$$1() {
                    return _P2;
                }
            }
        });

        //
        // constructor code
        //
        if (param instanceof Line) {
            _P1 = param.P1;
            _P2 = param.P2;
        } else if ((typeof param === 'undefined' ? 'undefined' : _typeof(param)) === 'object') {
            setPropertiesFromAttributes(this.attrs);
        }
    }

    //
    // CLASS INHERITANCE
    //
    Line.prototype = Object.assign(Object.create(GraphicsElement.prototype), {
        constructor: Line,
        isLine: true
    });

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Line.prototype, {
        toString: function toString() {
            return '<Line ' + this.id + '>';
        },

        convert: function convert(matrix) {
            if (!(matrix instanceof Matrix)) {
                matrix = new Matrix();
            }

            var ctm = this.matrix.mul(matrix);
            var element = new Bezier2(pointArrayToNumeric([this.P1, this.P2]));

            return [element.transform(ctm)];
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * https://www.w3.org/TR/SVG/paths.html#PathElement
     * param - {...} or Path
     */
    function Path(param) {
        //
        // Case when Path is called as a function
        //
        if (!(this instanceof Path)) {
            return new Path(param);
        }

        GraphicsElement.call(this, param);

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _pathData = [];
        var _d = '';

        var _pathDataCommands = 'MmZzLlHhVvCcSsQqTtAa';
        // https://www.w3.org/TR/SVG/paths.html#PathDataBNF
        var _pathDataRegexStr = '[-+]?(?:\\d+(?:\\.\\d*)?|\\.\\d+)(?:[eE][-+]?\\d+)?|\\ *[' + _pathDataCommands + ']\\ *';
        var _pathDataRegex = new RegExp(_pathDataRegexStr, 'g');

        //
        // PRIVATE FUNCTIONS
        //

        var parseData = function parseData(data) {
            // moveto (Mm), closepath (Zz), lineto (Ll), lineto H (Hh), lineto V (Vv)
            // curve cubic (CcSs), curve quadratic (QqTt), arc (Aa)

            _pathData = collectMatches(_pathDataRegex, data);
            var convertPathDataValue = function convertPathDataValue(val) {
                val = val.trim();
                if (!_pathDataCommands.includes(val)) {
                    // if it's not command then it should be a coordinate
                    // whatever is there that is not a number will be converted to zero
                    val = parseFloat(val) || 0;
                }
                return val;
            };
            _pathData = _pathData.map(convertPathDataValue);
        };
        // https://www.w3.org/TR/SVG/paths.html#PathData

        var setPropertiesFromAttributes = function setPropertiesFromAttributes(attributes) {
            _d = attributes['d'] || _d;
            if (typeof _d !== 'string') {
                _d = '';
            }

            parseData(_d);
        };

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            d: {
                enumerable: true,
                get: function get$$1() {
                    return _d;
                }
            },

            pathData: {
                get: function get$$1() {
                    return _pathData;
                }
            }
        });

        //
        // constructor code
        //
        if (param instanceof Path) {
            _d = param.d;
            parseData(_d);
        } else if ((typeof param === 'undefined' ? 'undefined' : _typeof(param)) === 'object') {
            setPropertiesFromAttributes(this.attrs);
        }
    }

    //
    // CLASS INHERITANCE
    //
    Path.prototype = Object.assign(Object.create(GraphicsElement.prototype), {
        constructor: Path,
        isPath: true
    });

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Path.prototype, {
        toString: function toString() {
            return '<Path ' + this.id + '>';
        },

        convert: function convert(matrix) {
            if (this.pathData.length == 0) {
                return [];
            }

            if (!(matrix instanceof Matrix)) {
                matrix = new Matrix();
            }

            var ctm = this.matrix.mul(matrix);
            var result = [];

            var pathData = this.pathData;

            var startPoint = null;
            var currentPoint = [0, 0];
            var prevControlPoint = currentPoint;

            var command = null;
            var coordinatesAreAbsolute = true;

            var idx = 0;
            while (idx < pathData.length) {
                var data = pathData[idx];

                // string data is a command
                if (typeof data === 'string') {
                    command = data;
                    idx++;

                    coordinatesAreAbsolute = command == command.toUpperCase();
                    command = command.toUpperCase();
                }
                // otherwise it is a number, which is ensured by parseData() in constructor

                // https://www.w3.org/TR/SVG/paths.html#PathDataMovetoCommands
                if (command == 'M') {
                    var x = pathData[idx];
                    idx++;
                    var y = pathData[idx];
                    idx++;
                    // let pt = new Point( x, y )

                    if (!coordinatesAreAbsolute) {
                        x += currentPoint[0];
                        y += currentPoint[1];
                    }

                    currentPoint = [x, y];

                    // Reset the subpath start point
                    startPoint = currentPoint;

                    // If there happens to be more coordinates after initial 'M'
                    // they are treated as LineTo commands. We set 'command' here
                    // and the while loop will fallthrough to the LineTo handling code
                    // if current idx points to a number. If not, 'command' will be
                    // set to whatever the current command might be.
                    command = 'L';
                }

                // https://www.w3.org/TR/SVG/paths.html#PathDataLinetoCommands
                else if ('LHV'.includes(command)) {
                        var _x = 0;
                        var _y = 0;

                        // The way to fill in
                        // y for 'H' and
                        // x for 'V'
                        if (coordinatesAreAbsolute) {
                            _x = currentPoint[0];
                            _y = currentPoint[1];
                        }

                        switch (command) {
                            case 'L':
                                _x = pathData[idx];
                                idx++;
                                _y = pathData[idx];
                                idx++;
                                break;
                            case 'H':
                                _x = pathData[idx];
                                idx++;
                                break;
                            case 'V':
                                _y = pathData[idx];
                                idx++;
                                break;
                        }

                        if (!coordinatesAreAbsolute) {
                            _x += currentPoint[0];
                            _y += currentPoint[1];
                        }

                        var points = flattenArray([currentPoint, _x, _y]);

                        result.push(new Bezier2(points));

                        currentPoint = [_x, _y];
                    }

                    // https://www.w3.org/TR/SVG/paths.html#PathDataQuadraticBezierCommands
                    else if (command == 'Q') {
                            // currentPoint = Start point - P0
                            // Control point - P1
                            var x1 = pathData[idx];
                            idx++;
                            var y1 = pathData[idx];
                            idx++;
                            // End point - P2
                            var _x2 = pathData[idx];
                            idx++;
                            var _y2 = pathData[idx];
                            idx++;

                            if (!coordinatesAreAbsolute) {
                                x1 += currentPoint[0];
                                y1 += currentPoint[1];
                                _x2 += currentPoint[0];
                                _y2 += currentPoint[1];
                            }

                            var _points = flattenArray([currentPoint, x1, y1, _x2, _y2]);

                            result.push(new Bezier3(_points));

                            // Save control point for 'T' command
                            prevControlPoint = [x1, y1];

                            currentPoint = [_x2, _y2];
                        } else if (command == 'T') {
                            // currentPoint = Start point - P0

                            // prevControlPoint = Control point - P1

                            // https://www.w3.org/TR/SVG/implnote.html#PathElementImplementationNotes
                            // We need to rotate prevControlPoint 180 degrees around currentPoint
                            // to get the control point for this curve.
                            // Rotating point (x,y) 180 degrees around point (x0, y0):
                            // x' = 2 * x0 - x
                            // y' = 2 * y0 - y
                            var _x3 = 2 * currentPoint[0] - prevControlPoint[0];
                            var _y3 = 2 * currentPoint[1] - prevControlPoint[1];

                            // End point - P2
                            var _x4 = pathData[idx];
                            idx++;
                            var _y4 = pathData[idx];
                            idx++;

                            if (!coordinatesAreAbsolute) {
                                _x4 += currentPoint[0];
                                _y4 += currentPoint[1];
                            }

                            var _points2 = flattenArray([currentPoint, _x3, _y3, _x4, _y4]);

                            result.push(new Bezier3(_points2));

                            // Save control point for 'T' command
                            prevControlPoint = [_x3, _y3];

                            currentPoint = [_x4, _y4];
                        }

                        // https://www.w3.org/TR/SVG/paths.html#PathDataCubicBezierCommands
                        else if (command == 'C') {
                                // currentPoint = Start point - P0
                                // Control point - P1
                                var _x5 = pathData[idx];
                                idx++;
                                var _y5 = pathData[idx];
                                idx++;
                                // Control point - P2
                                var x2 = pathData[idx];
                                idx++;
                                var y2 = pathData[idx];
                                idx++;
                                // End point - P3
                                var _x6 = pathData[idx];
                                idx++;
                                var _y6 = pathData[idx];
                                idx++;

                                if (!coordinatesAreAbsolute) {
                                    _x5 += currentPoint[0];
                                    _y5 += currentPoint[1];
                                    x2 += currentPoint[0];
                                    y2 += currentPoint[1];
                                    _x6 += currentPoint[0];
                                    _y6 += currentPoint[1];
                                }

                                var _points3 = flattenArray([currentPoint, _x5, _y5, x2, y2, _x6, _y6]);

                                result.push(new Bezier4(_points3));

                                // Save control point for 'S' command
                                prevControlPoint = [x2, y2];

                                currentPoint = [_x6, _y6];
                            } else if (command == 'S') {
                                // currentPoint = Start point - P0

                                // prevControlPoint = Control point - P1

                                // https://www.w3.org/TR/SVG/implnote.html#PathElementImplementationNotes
                                // We need to rotate prevControlPoint 180 degrees around currentPoint
                                // to get the control point for this curve.
                                // Rotating point (x,y) 180 degrees around point (x0, y0):
                                // x' = 2 * x0 - x
                                // y' = 2 * y0 - y
                                var _x7 = 2 * currentPoint[0] - prevControlPoint[0];
                                var _y7 = 2 * currentPoint[1] - prevControlPoint[1];

                                // Control point - P2
                                var _x8 = pathData[idx];
                                idx++;
                                var _y8 = pathData[idx];
                                idx++;
                                // End point - P3
                                var _x9 = pathData[idx];
                                idx++;
                                var _y9 = pathData[idx];
                                idx++;

                                if (!coordinatesAreAbsolute) {
                                    _x8 += currentPoint[0];
                                    _y8 += currentPoint[1];
                                    _x9 += currentPoint[0];
                                    _y9 += currentPoint[1];
                                }

                                var _points4 = flattenArray([currentPoint, _x7, _y7, _x8, _y8, _x9, _y9]);

                                result.push(new Bezier4(_points4));

                                // Save control point for 'S' command
                                prevControlPoint = [_x8, _y8];

                                currentPoint = [_x9, _y9];
                            }

                            // https://www.w3.org/TR/SVG/paths.html#PathDataEllipticalArcCommands
                            // https://www.w3.org/TR/SVG/implnote.html#ArcImplementationNotes
                            else if (command == 'A') {
                                    // Parameters:
                                    // (rx ry x-axis-rotation large-arc-flag sweep-flag x y)+
                                    var rx = pathData[idx];
                                    idx++;
                                    var ry = pathData[idx];
                                    idx++;

                                    var xAxisRotation = pathData[idx];
                                    idx++;
                                    var fA = pathData[idx];
                                    idx++;
                                    var fS = pathData[idx];
                                    idx++;

                                    var _x10 = pathData[idx];
                                    idx++;
                                    var _y10 = pathData[idx];
                                    idx++;

                                    if (!coordinatesAreAbsolute) {
                                        _x10 += currentPoint[0];
                                        _y10 += currentPoint[1];
                                    }

                                    // Arc = ( x1, y1, rx, ry, phi, fA, fS, x2, y2 )
                                    var arc = Arc.createFromSVGParameters(currentPoint[0], currentPoint[1], rx, ry, xAxisRotation, fA, fS, _x10, _y10);
                                    // A neat trick to 'unpack' the array returned by arc.convert()
                                    // and not have to use code like
                                    // result = result.concat( arc.convert() )
                                    Array.prototype.push.apply(result, arc.convert());

                                    currentPoint = [_x10, _y10];
                                }

                                // https://www.w3.org/TR/SVG/paths.html#PathDataClosePathCommand
                                else if (command == 'Z') {
                                        var _points5 = flattenArray([currentPoint, startPoint]);

                                        result.push(new Bezier2(_points5));

                                        currentPoint = startPoint;
                                    }

                                    // Not a command
                                    else {}
                                        // idx++


                                        // Covers the case when initial command
                                        // in path data string is not 'M'
                if (startPoint === null) {
                    startPoint = currentPoint;
                }
            }

            return result.map(function (el) {
                return el.transform(ctm);
            });
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * https://www.w3.org/TR/SVG/shapes.html#PolygonElement
     * param - {...} or Polygon
     */
    function Polygon(param) {
        //
        // Case when Polygon is called as a function
        //
        if (!(this instanceof Polygon)) {
            return new Polygon(param);
        }

        GraphicsElement.call(this, param);

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _points = '';
        var _pointsdData = [];

        var _decimalNumberRegexStr = MatchRegex.decimalNumber;
        var _pointsRegex = new RegExp(_decimalNumberRegexStr + '\\s?,\\s?' + _decimalNumberRegexStr, 'g');

        //
        // PRIVATE FUNCTIONS
        //

        // https://www.w3.org/TR/SVG/shapes.html#PolygonElementPointsAttribute
        var parsePoints = function parsePoints(data) {
            _pointsdData = collectMatches(_pointsRegex, data);
            _pointsdData = _pointsdData.map(function (val) {
                var parts = val.split(',');
                return [parseFloat(parts[0].trim()) || 0, parseFloat(parts[1].trim()) || 0];
            });
        };

        var setPropertiesFromAttributes = function setPropertiesFromAttributes(attributes) {
            _points = attributes['points'] || _points;
            if (typeof _points !== 'string') {
                _points = '';
            }

            parsePoints(_points);
        };

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            points: {
                enumerable: true,
                get: function get$$1() {
                    return _points;
                }
            },

            pointsData: {
                get: function get$$1() {
                    return _pointsdData;
                }
            }
        });

        //
        // constructor code
        //
        if (param instanceof Polygon) {
            _points = param.points;
            parsePoints(_points);
        } else if ((typeof param === 'undefined' ? 'undefined' : _typeof(param)) === 'object') {
            setPropertiesFromAttributes(this.attrs);
        }
    }

    //
    // CLASS INHERITANCE
    //
    Polygon.prototype = Object.assign(Object.create(GraphicsElement.prototype), {
        constructor: Polygon,
        isPolygon: true
    });

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Polygon.prototype, {
        toString: function toString() {
            return '<Polygon ' + this.id + '>';
        },

        /*
            - perform an absolute moveto operation to the first coordinate pair in the list of points
            - for each subsequent coordinate pair, perform an absolute lineto operation to that coordinate pair
            - perform a closepath command
        */
        convert: function convert(matrix) {
            if (this.pointsData.length == 0) {
                return [];
            }

            if (!(matrix instanceof Matrix)) {
                matrix = new Matrix();
            }

            var ctm = this.matrix.mul(matrix);
            var result = [];

            // perform an absolute moveto operation to the first coordinate pair in the list of points
            var current = this.pointsData[0];
            var start = current;
            var line = void 0;

            for (var idx = 0; idx < this.pointsData.length; idx++) {
                // for each subsequent coordinate pair, perform an absolute lineto operation to that coordinate pair
                var here = this.pointsData[idx];
                line = Bezier2(flattenArray([current, here]));

                result.push(line);

                current = here;
            }

            // perform a closepath command
            line = Bezier2(flattenArray([current, start]));
            result.push(line);

            return result.map(function (el) {
                return el.transform(ctm);
            });
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * https://www.w3.org/TR/SVG/shapes.html#PolylineElement
     * param - {...} or Polyline
     */
    function Polyline(param) {
        //
        // Case when Polyline is called as a function
        //
        if (!(this instanceof Polyline)) {
            return new Polyline(param);
        }

        GraphicsElement.call(this, param);

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _points = '';
        var _pointsdData = [];

        var _decimalNumberRegexStr = MatchRegex.decimalNumber;
        var _pointsRegex = new RegExp(_decimalNumberRegexStr + '\\s?,\\s?' + _decimalNumberRegexStr, 'g');

        //
        // PRIVATE FUNCTIONS
        //

        // https://www.w3.org/TR/SVG/shapes.html#PolylineElementPointsAttribute
        var parsePoints = function parsePoints(data) {
            _pointsdData = collectMatches(_pointsRegex, data);
            _pointsdData = _pointsdData.map(function (val) {
                var parts = val.split(',');
                return [parseFloat(parts[0].trim()) || 0, parseFloat(parts[1].trim()) || 0];
            });
        };

        var setPropertiesFromAttributes = function setPropertiesFromAttributes(attributes) {
            _points = attributes['points'] || _points;
            if (typeof _points !== 'string') {
                _points = '';
            }

            parsePoints(_points);
        };

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            points: {
                enumerable: true,
                get: function get$$1() {
                    return _points;
                }
            },

            pointsData: {
                get: function get$$1() {
                    return _pointsdData;
                }
            }
        });

        //
        // constructor code
        //
        if (param instanceof Polyline) {
            _points = param.points;
            parsePoints(_points);
        } else if ((typeof param === 'undefined' ? 'undefined' : _typeof(param)) === 'object') {
            setPropertiesFromAttributes(this.attrs);
        }
    }

    //
    // CLASS INHERITANCE
    //
    Polyline.prototype = Object.assign(Object.create(GraphicsElement.prototype), {
        constructor: Polyline,
        isPolyline: true
    });

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Polyline.prototype, {
        toString: function toString() {
            return '<Polyline ' + this.id + '>';
        },

        /*
            - perform an absolute moveto operation to the first coordinate pair in the list of points
            - for each subsequent coordinate pair, perform an absolute lineto operation to that coordinate pair
        */
        convert: function convert(matrix) {
            if (this.pointsData.length == 0) {
                return [];
            }

            if (!(matrix instanceof Matrix)) {
                matrix = new Matrix();
            }

            var ctm = this.matrix.mul(matrix);
            var result = [];

            // perform an absolute moveto operation to the first coordinate pair in the list of points
            var current = this.pointsData[0];
            var start = current;
            var line = void 0;

            for (var idx = 0; idx < this.pointsData.length; idx++) {
                // for each subsequent coordinate pair, perform an absolute lineto operation to that coordinate pair
                var here = this.pointsData[idx];
                line = Bezier2(flattenArray([current, here]));

                result.push(line);

                current = here;
            }

            return result.map(function (el) {
                return el.transform(ctm);
            });
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * https://www.w3.org/TR/SVG/shapes.html#RectElement
     * param - {...} or Rect
     */
    function Rect(param) {
        //
        // Case when Rect is called as a function
        //
        if (!(this instanceof Rect)) {
            return new Rect(param);
        }

        GraphicsElement.call(this, param);

        //
        // PRIVATE OBJECT PROPERTIES
        //
        var _bottomLeft = new Point(0, 0);
        var _topRight = new Point(0, 0);
        var _rx = 0;
        var _ry = 0;

        //
        // PRIVATE FUNCTIONS
        //
        var setPropertiesFromAttributes = function setPropertiesFromAttributes(attributes) {
            var x = lengthInBaseUnits(attributes['x']);
            var y = lengthInBaseUnits(attributes['y']);
            var width = lengthInBaseUnits(attributes['width']);
            var height = lengthInBaseUnits(attributes['height']);
            _rx = lengthInBaseUnits(attributes['rx'] || _rx);
            _ry = lengthInBaseUnits(attributes['ry'] || _ry);

            _bottomLeft = new Point(x, y);
            _topRight = new Point(x + width, y + height);
        };

        //
        // PRIVATE OBJECT PROPERTIES ACCESSORS
        //
        Object.defineProperties(this, {
            x: {
                enumerable: true,
                get: function get$$1() {
                    return _bottomLeft.x;
                }
            },
            y: {
                enumerable: true,
                get: function get$$1() {
                    return _bottomLeft.y;
                }
            },
            width: {
                enumerable: true,
                get: function get$$1() {
                    return _topRight.x - _bottomLeft.x;
                }
            },
            height: {
                enumerable: true,
                get: function get$$1() {
                    return _topRight.y - _bottomLeft.y;
                }
            },
            rx: {
                enumerable: true,
                get: function get$$1() {
                    return _rx;
                }
            },
            ry: {
                enumerable: true,
                get: function get$$1() {
                    return _ry;
                }
            },

            bottomLeft: {
                get: function get$$1() {
                    return _bottomLeft;
                }
            },
            topRight: {
                get: function get$$1() {
                    return _topRight;
                }
            }
        });

        //
        // constructor code
        //
        if (param instanceof Rect) {
            _bottomLeft = param.bottomLeft;
            _topRight = param.topRight;
            _rx = param.rx;
            _ry = param.ry;
        } else if ((typeof param === 'undefined' ? 'undefined' : _typeof(param)) === 'object') {
            setPropertiesFromAttributes(this.attrs);
        }
    }

    //
    // CLASS INHERITANCE
    //
    Rect.prototype = Object.assign(Object.create(GraphicsElement.prototype), {
        constructor: Rect,
        isRect: true
    });

    //
    // PUBLIC CLASS METHODS
    //
    Object.assign(Rect.prototype, {
        toString: function toString() {
            return '<Rect ' + this.id + '>';
        },

        convert: function convert(matrix) {
            if (!(matrix instanceof Matrix)) {
                matrix = new Matrix();
            }

            var ctm = this.matrix.mul(matrix);
            var result = [];
            var heightAsPt = new Point(0, this.height);

            var A = this.bottomLeft;
            var B = this.bottomLeft.add(heightAsPt);
            var C = this.topRight;
            var D = this.topRight.sub(heightAsPt);

            A = matrix.mul(A);
            B = matrix.mul(B);
            C = matrix.mul(C);
            D = matrix.mul(D);

            var left = new Bezier2(pointArrayToNumeric([A, B]));
            var top = new Bezier2(pointArrayToNumeric([B, C]));
            var right = new Bezier2(pointArrayToNumeric([C, D]));
            var bottom = new Bezier2(pointArrayToNumeric([D, A]));

            return [left.transform(ctm), top.transform(ctm), right.transform(ctm), bottom.transform(ctm)];
        }
    });

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    /**
     * @author zagortenej / https://gitlab.com/zagortenej
     */

    var REVISION = '1dev';

    exports.Angle = Angle;
    exports.Arc = Arc;
    exports.Point = Point;
    exports.Matrix = Matrix;
    exports.Bezier = Bezier;
    exports.Bezier2 = Bezier2;
    exports.Bezier3 = Bezier3;
    exports.Bezier4 = Bezier4;
    exports.flattenBezier3 = flattenBezier3;
    exports.flattenBezier4 = flattenBezier4;
    exports.BaseElement = BaseElement;
    exports.TransformableElement = TransformableElement;
    exports.Svg = Svg;
    exports.CreateFromString = CreateFromString;
    exports.CreateFromUrl = CreateFromUrl;
    exports.Group = Group;
    exports.GraphicsElement = GraphicsElement;
    exports.Circle = Circle;
    exports.Ellipse = Ellipse;
    exports.Line = Line;
    exports.Path = Path;
    exports.Polygon = Polygon;
    exports.Polyline = Polyline;
    exports.Rect = Rect;
    exports.collectMatches = collectMatches;
    exports.MatchRegex = MatchRegex;
    exports.lengthInBaseUnits = lengthInBaseUnits;
    exports.pointArrayToNumeric = pointArrayToNumeric;
    exports.isNumericArray = isNumericArray;
    exports.isPointArray = isPointArray;
    exports.isTransformableElementArray = isTransformableElementArray;
    exports.isBaseElementArray = isBaseElementArray;
    exports.flattenArray = flattenArray;
    exports.roundToPrecision = roundToPrecision;
    exports.minX = minX;
    exports.minY = minY;
    exports.maxX = maxX;
    exports.maxY = maxY;
    exports.GUID = GUID;
    exports.REVISION = REVISION;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
