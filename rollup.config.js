import babel from 'rollup-plugin-babel'

export default {
    entry: 'src/main.js',
    indent: '    ',
    // sourceMap: true,

    plugins: [
        babel({
            exclude: 'node_modules/**',
        }),
    ],

    targets: [
        {
            format: 'umd',
            moduleName: 'SVGRX',
            dest: 'build/svgrx.js'
        },
    ]
}
