/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { GUID, isBaseElementArray } from '../util/lib.js'

/**
 * param - {} or BaseElement
 */
function BaseElement( param )
{
    //
    // Case when BaseElement is called as a function
    //
    if( ! ( this instanceof BaseElement ) )
    {
        return new BaseElement( param )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _id = GUID()
    var _class = ''
    var _style = ''
    var _attributes = {}

    var _children = []

    //
    // PRIVATE FUNCTIONS
    //
    var setPropertiesFromAttributes = function( attributes )
    {
        _id     = attributes['id']      || _id
        _class  = attributes['class']   || _class
        _style  = attributes['style']   || _style
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        id:
        {
            get: function() { return _id },
            set: function( value ) { if( typeof value === 'string' ) _id = value },
        },
        class:
        {
            get: function() { return _class },
        },
        style:
        {
            get: function() { return _style },
        },
        attrs:
        {
            get: function() { return _attributes },
        },

        children:
        {
            get: function() { return _children },
            set: function( value ) { if( isBaseElementArray( value ) ) _children = value },
        },
    })

    //
    // constructor code
    //
    if( param instanceof BaseElement )
    {
        _attributes = {}
        Object.assign( _attributes, param.attrs )

        setPropertiesFromAttributes( _attributes )

        _children = param.children
    }
    else if( typeof param === 'object' )
    {
        Object.assign( _attributes, param )

        setPropertiesFromAttributes( _attributes )
    }
}

//
// PUBLIC CLASS METHODS
//
Object.assign( BaseElement.prototype,
{
    toString: function()
    {
        return `<BaseElement ${this.id}>`
    },
} )



export { BaseElement }
