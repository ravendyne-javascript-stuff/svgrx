/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { BaseElement } from './BaseElement.js'

import { Matrix } from '../geometry/Matrix.js'

import { collectMatches } from '../types/Matchers.js'

/**
 * param - {} or TransformableElement
 */
function TransformableElement( param )
{
    //
    // Case when TransformableElement is called as a function
    //
    if( ! ( this instanceof TransformableElement ) )
    {
        return new TransformableElement( param )
    }

    BaseElement.call( this, param )

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _transform = ''

    var _transformData = []
    var _transformMatrix = []

    //
    // PRIVATE FUNCTIONS
    //
    var parseTransform = function( transform )
    {
        var result = []

        var _numberRegex = /[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?/g
        var _unitRegex =  /em|ex|px|in|cm|mm|pt|pc|%/g


        var svg_transforms = [ 'matrix', 'translate', 'scale', 'rotate', 'skewX', 'skewY' ]

        // match any SVG transformation with its parameter (until final parenthese)
        // [^)]*    == anything but a closing parenthese
        // '|'.join == OR-list of SVG transformations
        svg_transforms = svg_transforms.map(
        ( val ) =>
        {
            // match transform name and anything after it up to, and including, ')'
            return val + '[^)]*\\)'
        })
        // join matchers for all transforms with regex OR: '|''
        var regex = svg_transforms.join('|')
        var transformsRegex = new RegExp( regex, 'g')

        var matches = collectMatches( transformsRegex, transform )
        // 'matches' is an array of strings in a form "operation_name ( arg1, arg2 ... )"
        // i.e. ["translate(-10,-20)", "scale(2)", "rotate(45)", "translate(5,10)"]
        for( let i = 0; i < matches.length; i++ )
        {
            // opargs is an array of two strings: operation name and arguments between ()
            // i.e. ["translate", "-10,-20)"]
            var opargs = matches[ i ].split('(')

            // opargs[1] is matched to produce args array of strings where each element is one operation argument
            // i.e. ["-10", "-20"]
            var args = collectMatches( _numberRegex, opargs[ 1 ] )
            args = args.map( ( val ) => {
                return parseFloat( val ) || 0
            })

            result.push(
            {
                // operation (string)
                op: opargs[ 0 ],
                // arguments array (numbers)
                args: args,
            } )
        }

        return result
    }

    var createTransformMatrix = function( data )
    {
        var matrix = new Matrix()

        for( let idx = 0; idx < data.length; idx++ )
        {
            let op = data[ idx ].op
            let args = data[ idx ].args

            switch( op )
            {
                case 'matrix':
                {
                    matrix = matrix.mul( new Matrix( args ) )
                }
                break

                case 'translate':
                {
                    let tx = args[ 0 ]
                    let ty = args[ 1 ] || 0
                    matrix = matrix.mul( Matrix( [ 1, 0, 0, 1, tx, ty ] ) )
                }
                break

                case 'scale':
                {
                    let sx = args[ 0 ]
                    let sy = args[ 1 ] || sx
                    matrix = matrix.mul( Matrix( [ sx, 0, 0, sy, 0, 0 ] ) )
                }
                break

                case 'rotate':
                {
                    let cosa = Math.cos( Math.radians( args[ 0 ] ) )
                    let sina = Math.sin( Math.radians( args[ 0 ] ) )
                    let cx = args[ 1 ] || 0
                    let cy = args[ 2 ] || 0

                    // translate to rotation center
                    matrix = matrix.mul( Matrix( [ 1, 0, 0, 1, cx, cy ] ) )
                    // rotate
                    matrix = matrix.mul( Matrix( [ cosa, sina, -sina, cosa, 0, 0 ] ) )
                    // translate back to where we were
                    matrix = matrix.mul( Matrix( [ 1, 0, 0, 1, -cx, -cy ] ) )
                }
                break

                case 'skewX':
                {
                    let tana = Math.tan( Math.radians( args[ 0 ] ) )
                    matrix = matrix.mul( Matrix( [ 1, 0, tana, 1, 0, 0 ] ) )
                }
                break

                case 'skewY':
                {
                    let tana = Math.tan( Math.radians( args[ 0 ] ) )
                    matrix = matrix.mul( Matrix( [ 1, tana, 0, 1, 0, 0 ] ) )
                }
                break
            }
        }

        return matrix
    }

    var setPropertiesFromAttributes = function( attributes )
    {
        _transform  = attributes['transform']   || _transform

        _transformData = parseTransform( _transform )
        _transformMatrix = createTransformMatrix( _transformData )
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        transform:
        {
            get: function() { return _transform },
        },

        transformData:
        {
            get: function() { return _transformData },
        },
        matrix:
        {
            get: function() { return _transformMatrix },
        },
    })

    //
    // constructor code
    //
    if( param instanceof TransformableElement )
    {
        setPropertiesFromAttributes( { 'transform': param.transform } )
    }
    else if( typeof param === 'object' )
    {
        setPropertiesFromAttributes( this.attrs )
    }
}


//
// CLASS INHERITANCE
//
TransformableElement.prototype = Object.assign( Object.create( BaseElement.prototype ),
{
    constructor: TransformableElement,
    isTransformableElement: true,
})


//
// PUBLIC CLASS METHODS
//
Object.assign( TransformableElement.prototype,
{
    toString: function()
    {
        return `<TransformableElement ${this.id}>`
    },

    // Override in subclass
    convert: function( matrix )
    {
        // array of Beziers
        return []
    },
} )


export { TransformableElement }
