/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { Point } from './Point.js'
import { Bezier } from './Bezier.js'

/**
 * Implements linear Bezier curve
 */
function Bezier4( points )
{
    //
    // Case when Bezier4 is called as a function
    //
    if( ! ( this instanceof Bezier4 ) )
    {
        return new Bezier4( points )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _P0 = new Point()
    var _P1 = new Point()
    var _P2 = new Point()
    var _P3 = new Point()

    //
    // PRIVATE FUNCTIONS
    //

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties( this,
    {
        P0:
        {
            enumerable: true,
            get: function() { return _P0 },
            set: function( value ) { _P0 = new Point( value ) },
        },
        P1:
        {
            enumerable: true,
            get: function() { return _P1 },
            set: function( value ) { _P1 = new Point( value ) },
        },
        P2:
        {
            enumerable: true,
            get: function() { return _P2 },
            set: function( value ) { _P2 = new Point( value ) },
        },
        P3:
        {
            enumerable: true,
            get: function() { return _P3 },
            set: function( value ) { _P3 = new Point( value ) },
        },
    })

    //
    // constructor code
    //
    Bezier.call( this, points )

}


//
// CLASS INHERITANCE
//
Bezier4.prototype = Object.assign( Object.create( Bezier.prototype ),
{
    constructor: Bezier4,
})


//
// PUBLIC CLASS METHODS
//
Object.assign( Bezier4.prototype,
{
    toString: function()
    {
        return 'Bezier Q : ' + this.P0 + ', ' + this.P1 + ', ' + this.P2 + ', ' + this.P3
    },

    // Called by Bezier.evaluate(t)s
    // Bernstein polinomial evaluation
    _interpolate: function( t )
    {
        // B( t ) =
        //      ( 1 - t ) ^ 3 * P0 +
        //      3 * ( 1 - t ) ^ 2 * t * P1 +
        //      3 * ( 1 - t ) * t ^ 2 * P2 + 
        //      t ^ 3 * P3

        // ( 1 - t ) ^ 3 * P0
        var A = this.P0.mul( Math.pow( 1 - t, 3 ) )
        // 3 * ( 1 - t ) ^ 2 * t * P1
        var B = this.P1.mul( 3 * ( 1 - t ) * ( 1 - t ) * t )
        // 3 * ( 1 - t ) * t ^ 2 * P2
        var C = this.P2.mul( 3 * ( 1 - t ) * t * t )
        // t ^ 3 * P3
        var D = this.P3.mul( Math.pow( t, 3 ) )

        // B( t ) = A + B + C + D
        var pt = A.add( B ).add( C ).add( D )

        return pt
    },

    transform: function( matrix )
    {
        return new Bezier4( [
            matrix.mul( this.P0 ),
            matrix.mul( this.P1 ),
            matrix.mul( this.P2 ),
            matrix.mul( this.P3 ),
        ] )
    },
} )


export { Bezier4 }
