/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { Point } from './Point.js'
import { Bezier } from './Bezier.js'

/**
 * Implements linear Bezier curve
 */
function Bezier3( points )
{
    //
    // Case when Bezier3 is called as a function
    //
    if( ! ( this instanceof Bezier3 ) )
    {
        return new Bezier3( points )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _P0 = new Point()
    var _P1 = new Point()
    var _P2 = new Point()

    //
    // PRIVATE FUNCTIONS
    //

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties( this,
    {
        P0:
        {
            enumerable: true,
            get: function() { return _P0 },
            set: function( value ) { _P0 = new Point( value ) },
        },
        P1:
        {
            enumerable: true,
            get: function() { return _P1 },
            set: function( value ) { _P1 = new Point( value ) },
        },
        P2:
        {
            enumerable: true,
            get: function() { return _P2 },
            set: function( value ) { _P2 = new Point( value ) },
        },
    })

    //
    // constructor code
    //
    Bezier.call( this, points )

}


//
// CLASS INHERITANCE
//
Bezier3.prototype = Object.assign( Object.create( Bezier.prototype ),
{
    constructor: Bezier3,
})


//
// PUBLIC CLASS METHODS
//
Object.assign( Bezier3.prototype,
{
    toString: function()
    {
        return 'Bezier Q : ' + this.P0 + ', ' + this.P1 + ', ' + this.P2
    },

    // Called by Bezier.evaluate(t)s
    // Bernstein polinomial evaluation
    _interpolate: function( t )
    {
        // B( t ) =
        //      ( 1 - t ) ^ 2 * P0 +
        //      2 * ( 1 - t ) * t * P1 +
        //      t ^ 2 * P2

        // ( 1 - t ) ^ 2 * P0
        var A = this.P0.mul( ( 1 - t ) * ( 1 - t ) )
        // 2 * ( 1 - t ) * t * P1
        var B = this.P1.mul( 2 * ( 1 - t ) * t )
        // t ^ 2 * P2
        var C = this.P2.mul( t * t )

        // B( t ) = A + B + C
        var pt = A.add( B ).add( C )

        return pt
    },

    transform: function( matrix )
    {
        return new Bezier3( [
            matrix.mul( this.P0 ),
            matrix.mul( this.P1 ),
            matrix.mul( this.P2 ),
        ] )
    },
} )


export { Bezier3 }
