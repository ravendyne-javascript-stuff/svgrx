/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { Angle } from './Angle.js'
import { Matrix } from './Matrix.js'
import { Point } from './Point.js'
import { Bezier4 } from './Bezier4.js'

import { isNumericArray, isPointArray } from '../util/lib.js'

// https://www.w3.org/TR/SVG/paths.html#PathDataEllipticalArcCommands
// https://www.w3.org/TR/SVG/implnote.html#ArcImplementationNotes
/**
* params -> [ cx, cy, rx, ry, start-angle, delta-angle, x-axis-rotation ]
*/
function Arc( params )
{
    //
    // Case when Arc is called as a function
    //
    if( ! ( this instanceof Arc ) )
    {
        return new Arc( params )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _params = [ 0, 0, 1, 1, 0, 90, 0 ]
    var _Pc = new Point()
    var _rx = 1
    var _ry = 1
    var _startAngle = new Angle( 0 )
    var _deltaAngle = new Angle( 90 )
    var _xAxisRotation = new Angle( 0 )

    var _transformationMatrix = new Matrix()

    //
    // PRIVATE FUNCTIONS
    //

    var parseParams = function()
    {
        let cx, cy
        [ cx, cy, _rx, _ry, _startAngle, _deltaAngle, _xAxisRotation ] = _params

        if( Math.abs( _deltaAngle ) < 0.0001 )
        {
            // TODO: degenerate to a Point() instead?
            throw new Error("arc is too small!")
        }

        _Pc = new Point( cx, cy )
        _startAngle = new Angle( _startAngle )
        _deltaAngle = new Angle( _deltaAngle )
        _xAxisRotation = new Angle( _xAxisRotation )

        _transformationMatrix = _transformationMatrix.translate( cx, cy )
        _transformationMatrix = _transformationMatrix.rotate( _xAxisRotation.angle )
        _transformationMatrix = _transformationMatrix.scale( _rx, _ry )
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties( this,
    {
        params:
        {
            get: function() { return _params },
        },
        center:
        {
            enumerable: true,
            get: function() { return _Pc },
        },
        rx:
        {
            enumerable: true,
            get: function() { return _rx },
        },
        ry:
        {
            enumerable: true,
            get: function() { return _ry },
        },
        start:
        {
            enumerable: true,
            get: function() { return _startAngle },
        },
        delta:
        {
            enumerable: true,
            get: function() { return _deltaAngle },
        },
        xAxisRotation:
        {
            enumerable: true,
            get: function() { return _xAxisRotation },
        },
        matrix:
        {
            get: function() { return _transformationMatrix },
        },
    })

    //
    // constructor code
    //
    if( ! isNumericArray( params ) )
    {
        return
    }
    if( params.length < 7 )
    {
        return
    }

    _params = params
    parseParams()
}

//
// PUBLIC CLASS METHODS
//
Object.assign( Arc.prototype,
{
    toString: function()
    {
        return 'Arc: ' + this.propBar
    },

/*
    Outline of SVG Path arc conversion:

    given: x1, y1, rx, ry, x-axis-rotation, fA, fS, x2, y2
        - use createFromSVGParameters() to find cx, cy, startAngle and endAngle
        let arc = Arc.createFromSVGParameters( x1, y1, rx, ry, phi, fA, fS, x2, y2 )

    arc.convert():
    - arc has:
        - center point
        - both axes: rx, ry
        - start angle
        - end angle
        - x-axis angle

    - simplify calculation down to the circle:
        - center (0,0)
        - radius (1.0)
        - start angle
        - end angle

    - all other values are accumulated in tranformation matrix (in this order):
        - rotate by x-axis-angle
        - center point -> translate to Pc
        - rx,ry -> scaleX rx, scaleY ry

    - use approximateArc() on the simplified circle to get array of Bezier points
    - apply transformation matrix to all the points and return as result

*/
    convert: function( matrix )
    {
        if( ! ( matrix instanceof Matrix ) )
        {
            matrix = new Matrix()
        }

        let Pc = new Point( 0, 0 )
        let R = 1.0
        let Pstart = this.start.toPoint( R )

        let arrayOfBeziers = Arc.approximateArc( Pstart, this.delta.angle, Pc )

        let ctm = this.matrix.mul( matrix )

        return arrayOfBeziers.map( ( val ) => val.transform( ctm ) )
    }
} )

//
// PUBLIC "STATIC" CLASS METHODS
//
Object.assign( Arc,
{
    /**
    * Used when parsing SVG Path 'd' attribute. Implementation based on notes from
    * https://www.w3.org/TR/SVG/implnote.html#ArcConversionEndpointToCenter
    * parameters: last point x1, y1, Path Arc parameters: rx ry x-axis-rotation large-arc-flag sweep-flag x y
    */
    createFromSVGParameters: function( x1, y1, rx, ry, phi, fA, fS, x2, y2 )
    {
        let P1 = new Point( x1, y1 )
        let P2 = new Point( x2, y2 )

        // START - parameter checking
        // https://www.w3.org/TR/SVG/implnote.html#ArcOutOfRangeParameters
        if( P1.sub( P2 ).length() < 0.0001 )
        {
            throw new Error("arc points too close")
            // return undefined
        }
        if( rx == 0 || ry == 0 )
        {
            throw new Error("radius == 0")
            // return Bezier2()
        }
        rx = Math.abs( rx )
        ry = Math.abs( ry )
        phi %= 360
        // END - parameter checking

        // Step 1
        // Un-rotate the mid-point between P1 and P2
        let P1p = P1.sub( P2 ).mul( 0.5 ).rotate( -phi )

        // https://www.w3.org/TR/SVG/implnote.html#ArcCorrectionOutOfRangeRadii
        // Make sure we have at least one solution
        let lambda = ( P1p.x * P1p.x ) / ( rx * rx ) + ( P1p.y * P1p.y ) / ( ry * ry )
        if( lambda > 1.0 )
        {
            lambda = Math.sqrt( lambda )
            rx *= lambda
            ry *= lambda
        }

        // Step 2
        // Find ellipse center point
        let cxp = rx * P1p.y / ry
        let cyp = - ry * P1p.x / rx
        let coef = Math.sqrt( ( rx*rx*ry*ry - rx*rx*P1p.y*P1p.y - ry*ry*P1p.x*P1p.x ) / ( rx*rx * P1p.y*P1p.y + ry*ry * P1p.x*P1p.x ) )
        if( fA == fS )
        {
            coef = - coef
        }
        cxp *= coef
        cyp *= coef

        // Step 3
        // rotate back the center to it's correct position (to undo what we did in Step 1)
        let Pcp = new Point( cxp, cyp ).rotate( phi )
        let Pc = Pcp.rotate( phi ).add( P1.add( P2 ).mul( 0.5 ) )

        // Step 4
        // Find starting angle (for P1) and arc angle (the angle between P1 and P2)
        // by using points in 'un-rotated' space (P1p and cxp, cyp)
        let unitX = new Point( 1, 0 )
        let first = new Point( ( P1p.x - cxp ) / rx, ( P1p.y - cyp ) / ry )
        let theta1 = unitX.angle( first )

        let second = new Point( ( - P1p.x - cxp ) / rx, ( - P1p.y - cyp ) / ry )
        let thetaD = first.angle( second )

        if( fS == 0 && thetaD > 0 )
        {
            thetaD -= 360
        }
        if( fS == 1 && thetaD < 0 )
        {
            thetaD += 360
        }

        return new Arc( [ Pc.x, Pc.y, rx, ry, theta1, thetaD, phi ] )
    },

    /**
     * Approximates circular arc with center in Pc and start point Pstart.
     * Arc spans deltaAngle degrees: clockwise if deltaAngle > 0,
     * and counterclockwise if deltaAngle < 0.
     *
     */
    approximateArc: function( Pstart, deltaAngle, Pc )
    {
        let absAngle = Math.abs( deltaAngle )
        if( absAngle < 0.0001 )
        {
            // TODO: degenerate to a Point() instead ?
            throw new Error("Arc is too small!")
        }
        let angleSign = Math.sign( deltaAngle )


        var translatePoints = function( points, targetPoint )
        {
            let newPoints = []
            for( let idx = 0; idx < points.length; idx++ )
            {
                newPoints.push( points[ idx ].add( targetPoint ) )
            }

            return newPoints
        }

        var normalizeAngle = function( angle )
        {
            if( angle < 0 )
            {
                angle += 360
            }

            return angle
        }

        // translate to (0,0)
        Pstart = Pstart.sub( Pc )

        // We need to have angles in range 0 < angle < 360 in order
        // to properly calculate number of 90-deg arc segments
        let startAngle = normalizeAngle( Pstart.angle() )

        let numberOfQuarters = Math.floor( absAngle / 90 )
        let restOfTheArc = absAngle - numberOfQuarters * 90

        let result = []
        let currentPoint = Pstart
        let angle90 = angleSign * 90
        restOfTheArc = angleSign * restOfTheArc
        for( let n = 0; n < numberOfQuarters; n++ )
        {
            let points = Arc.approximateArcBasic( currentPoint, angle90 )

            points = translatePoints( points, Pc )
            result.push( new Bezier4( points ) )

            currentPoint = currentPoint.rotate( angle90 )
        }

        let points = Arc.approximateArcBasic( currentPoint, restOfTheArc )
        points = translatePoints( points, Pc )
        result.push( new Bezier4( points ) )

        return result
    },

    // http://stackoverflow.com/a/40327968
    approximateArcBasic: function( Pstart, angle )
    {
    /*
        parameters: (Pstart, angle)
            - assume (xc,yc) = (0,0)
            - then R = Pstart.length()
            - -90 < angle < 90
            - arc starts at Pstart

        we'll calculate bezier points for case when P0 is at (R,0)
        and then just rotate them by angle between Pstart and X-axis

        - assume P0 = (R,0)
        - find P3 for (angle): (Rsin(angle),Rcos(angle))
        - calculate Bezier4 points
        - rotate Bezier4 points by Pstart.angle(1,0)

    */
        angle %= 360
        if( Math.abs( angle ) > 90 )
        {
            // divide and conquer?
            // reject the case?
            throw new Error('approximateArcBasic: angle > +/-90 degrees')
        }

        let R = Pstart.length()
        let P0 = Point( R, 0 )
        let P3 = P0.rotate( angle )

        // 0 < phi < PI/4
        // which means:
        // 0 < angle < 90
        let phi = Math.radians( angle / 2 )

        // text on SO has error here
        // missing '/' between '3' and '('
        let k = ( 4 / 3 ) / ( 1 / Math.cos( phi ) + 1 )
        // let k = 0.55191502449

        // PM = (P0 + P3) / 2
        let PM = P0.add( P3 ).mul( 0.5 )

        // PH = PM / cos(x) = PM sec(x) = (P0 + P3) sec(x) / 2
        let PH = PM.mul( 1 / Math.cos( phi ) )

        // PE = PH / cos(x) = PM sec(x)^2 = (P0 + P3) sec(x)^2 / 2
        let PE = PH.mul( 1 / Math.cos( phi ) )

        // P1 = (1 - k)P0 + k PE
        // let P1 = P0.mul( 1 - k ).add( PE.mul( k ) )
        // or
        // P1 = P0 + k (PE - P0)
        let P1 = P0.add( PE.sub( P0 ).mul( k ) )

        // P2 = (1 - k)P3 + k PE
        // let P2 = P3.mul( 1 - k ).add( PE.mul( k ) )
        // or
        // P2 = P3 + k (PE - P3)
        let P2 = P3.add( PE.sub( P3 ).mul( k ) )

        let backAngle = Pstart.angle()
        P0 = P0.rotate( backAngle )
        P1 = P1.rotate( backAngle )
        P2 = P2.rotate( backAngle )
        P3 = P3.rotate( backAngle )

        return [ P0, P1, P2, P3 ]
    },

} )

export { Arc }
