/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { Point } from './Point.js'

import { isNumericArray } from '../util/lib.js'

/**
    Affine transformation matrix and its operations.
    (https://en.wikipedia.org/wiki/Affine_transformation)
    (https://docs.oracle.com/javase/7/docs/api/java/awt/geom/AffineTransform.html)
    (https://upload.wikimedia.org/wikipedia/commons/2/2c/2D_affine_transformation_matrix.svg)

    SVG represents affine transform matrix as a list of 6 values [a, b, c, d, e, f]
    in an element's 'transform' attribute. The 6 values represent matrix elements as follows:

    | a  c  e |
    | b  d  f |
    | 0  0  1 |

    See: https://www.w3.org/TR/SVG/coords.html#TransformMatrixDefined
*/
function Matrix( param )
{
    if( ! ( this instanceof Matrix ) )
    {
        return new Matrix( param )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    const _unitVector = [ 1, 0, 0, 1, 0, 0 ]
    // default transformation is unit vector
    var _vect = _unitVector

    //
    // PRIVATE FUNCTIONS
    //
    function getVectorForParameter( param )
    {
        if( param instanceof Array && param.length == 6 )
        {
            if( isNumericArray( param ) )
            {
                return param
            }
        }

        return _unitVector
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        // private properties getters/setters
        // so we don't have to, i.e. check if assigned values are numbers
        // or to try/catch when working with private props in every other method
        vect:
        {
            enumerable: true,
            get: function() { return _vect },
            set: function( value ) { _vect = getVectorForParameter( value ) },
        },
    })

    //
    // constructor code
    //
    _vect = getVectorForParameter( param )
}

//
// PUBLIC CLASS METHODS
//
Object.assign( Matrix.prototype,
{
    toString: function()
    {
        return "[" + this.vect + "]"
    },

    /** Matrix multiplication with:
        - matrix
        - point
    */
    mul: function( other )
    {
        if( other instanceof Matrix )
        {
            var a = this.vect[0] * other.vect[0] + this.vect[2] * other.vect[1]
            var b = this.vect[1] * other.vect[0] + this.vect[3] * other.vect[1]
            var c = this.vect[0] * other.vect[2] + this.vect[2] * other.vect[3]
            var d = this.vect[1] * other.vect[2] + this.vect[3] * other.vect[3]
            var e = this.vect[0] * other.vect[4] + this.vect[2] * other.vect[5]  + this.vect[4]
            var f = this.vect[1] * other.vect[4] + this.vect[3] * other.vect[5]  + this.vect[5]

            return new Matrix( [ a, b, c, d, e, f ] )
        }
        else if( other instanceof Point )
        {
            var x = other.x * this.vect[0] + other.y * this.vect[2] + this.vect[4]
            var y = other.x * this.vect[1] + other.y * this.vect[3] + this.vect[5]
            
            return new Point( x, y )
        }

        // pass-through
        return other
    },

    scale: function( sx, sy )
    {
        sx = sx || 0
        sy = sy || sx

        if( ! Number.isFinite( sx ) && ! Number.isFinite( sy ) )
        {
            return this
        }

        return this.mul( Matrix( [ sx, 0, 0, sy, 0, 0 ] ) )
    },

    skew: function( ax, ay )
    {
        ax = ax || 0
        ay = ay || 0

        if( ! Number.isFinite( ax ) && ! Number.isFinite( ay ) )
        {
            return this
        }

        let tanax = Math.tan( Math.radians( ax ) )
        let tanay = Math.tan( Math.radians( ay ) )

        return this.mul( Matrix( [ 1, tanay, tanax, 1, 0, 0 ] ) )
    },

    translate: function( tx, ty )
    {
        tx = tx
        ty = ty || 0

        return this.mul( Matrix( [ 1, 0, 0, 1, tx, ty ] ) )
    },

    rotate: function( angle )
    {
        let cosa = Math.cos( Math.radians( angle ) )
        let sina = Math.sin( Math.radians( angle ) )

        return this.mul( Matrix( [ cosa, sina, -sina, cosa, 0, 0 ] ) )
    },

} )

export { Matrix }
