/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { Angle } from './Angle.js'
import * as utils from '../util/lib.js'

/**
 * Point is a class that represents specific point in 2D space
 * as opposed to a point that moves around in 2D space (i.e. by changing it's x,y values)
 *
 * This means that all arithmetic methods of the class (i.e. add(), mul() etc.)
 * return as result a new Point object.
 *
 * Ways to create an object that represents a specific point in 2D space:
 * var pt = new Point( x, y )
 * var pt = new Point( [x, y] )
 * var pt = new Point( otherPointObject )
 */
function Point( x, y )
{
    //
    // Case when Point is called as a function
    //
    if( ! ( this instanceof Point ) )
    {
        return new Point( x, y )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _x = 0
    var _y = 0

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        // X, Y getter/setter
        // so we don't have to check if assigned values are numbers
        // or to try/catch when working with X, Y in every other method
        x:
        {
            enumerable: true,
            get: function() { return _x },
            set: function( value ) { if( Number.isFinite( value ) ) _x = utils.roundToPrecision( value ) },
        },
        y:
        {
            enumerable: true,
            get: function() { return _y },
            set: function( value ) { if( Number.isFinite( value ) ) _y = utils.roundToPrecision( value ) },
        },
    })

    //
    // constructor code
    //
    if( Number.isFinite( x ) && Number.isFinite( y ) )
    {
        this.x = x
        this.y = y
    }
    if( utils.isNumericArray( x ) && x.length == 2 )
    {
        this.x = x[0]
        this.y = x[1]
    }
    if( x instanceof Point )
    {
        this.x = x.x
        this.y = x.y
    }
}

//
// 'STATIC' CLASS PROPERTIES
//
Object.defineProperties( Point,
{
    precision:
    {
        get: function()
        {
            if( ! Point._precision )
            {
                Point._precision = 6
            }

            if( ! Number.isFinite( Point._precision ) )
            {
                Point._precision = 6
            }

            return Number( Point._precision.toFixed( 0 ) )
        },
        set: function( value )
        {
            if( Number.isFinite( value ) )
            {
                Point._precision = Number( value.toFixed( 0 ) )
            }
        },
    },
})

//
// PUBLIC CLASS METHODS
//
Object.assign( Point.prototype,
{
    toString: function()
    {
        return '(' + this.x.toFixed( Point.precision ) + ', ' + this.y.toFixed( Point.precision ) + ')'
    },

    add: function( other )
    {
        if( other instanceof Point )
        {
            return new Point( this.x + other.x, this.y + other.y )
        }

        return this
    },

    sub: function( other )
    {
        if( other instanceof Point )
        {
            return new Point( this.x - other.x, this.y - other.y )
        }

        return this
    },

    mul: function( other )
    {
        if( Number.isFinite( other ) )
        {
            return new Point( this.x * other, this.y * other )
        }

        return this
    },

    equals: function( other )
    {
        if( ! ( other instanceof Point ) )
        {
            return false
        }

        return ( this.x == other.x && this.y == other.y )
    },

    coord: function()
    {
        return [ this.x, this.y ]
    },

    /** The point distance from 2D space origin (0, 0) */
    length: function()
    {
        return utils.roundToPrecision( Math.sqrt( this.x * this.x + this.y * this.y ) )
    },

    /** Return a point that is at a given angular distance along the circle
        constructed when this point rotates around 2D space origin (0, 0)
        at radius equal this.length() */
    rotate: function( angle )
    {
        if ( ! ( angle instanceof Angle ) )
        {
            angle = new Angle( angle )
        }

        var x = this.x * angle.cos - this.y * angle.sin
        var y = this.x * angle.sin + this.y * angle.cos
        x = utils.roundToPrecision( x )
        y = utils.roundToPrecision( y )

        return new Point( x, y )
    },

    /** Returns normalized point (length() == 1) */
    norm: function()
    {
        let len = this.length()
        return new Point( this.x / len, this.y / len )
    },

    /** Returns dot product of vectors (origin, this) and (origin, other) */
    dot: function( other )
    {
        other = new Point( other )

        return this.x * other.x + this.y * other.y
    },

    /** Returns angle in degrees between vectors (origin, this) and (origin, other) */
    angle: function( other )
    {
        if( ! ( other instanceof  Point ) )
        {
            return Math.degrees( Math.atan2( this.y, this.x ) )
        }

        // let angle = other.angle() - this.angle()
        // return angle

        let angle = Math.atan2( other.y, other.x ) - Math.atan2( this.y, this.x )
        return Math.degrees( angle )
    },
} )

export { Point }
