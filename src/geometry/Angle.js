/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { Point } from './Point.js'
import * as utils from '../util/lib.js'

/**
 * Helper class when working with specific angle values
 * so we don't have to do calculations like sin, cos, etc.
 * every time we reference the angle.
 *
 * 'param' is angle in degrees
 */
function Angle( param )
{
    //
    // Case when Angle is called as a function
    //
    if( ! ( this instanceof Angle ) )
    {
        return new Angle( param )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _angleRad = 0
    var _angleDeg = 0
    var _sin = 0
    var _cos = 1

    //
    // PRIVATE FUNCTIONS
    //
    function setAngle( angle )
    {
        if( Number.isFinite( angle ) )
        {
            // Constrain to values between -360 and +360
            // because we need to be able to express angle difference with this also
            // and the difference can be > +180 or < -180, depending on values of
            // lhs and rhs of subtraction operation
            if( Math.abs( angle ) == 360 )
            {
                _angleDeg = angle
            }
            else
            {
                _angleDeg = angle % 360
            }
            _angleRad = Math.radians( _angleDeg )
        }

        // Angle can be defined via a 2D space point as an
        // andgle between vector [origin, Point] and X-axis
        if( angle instanceof Point )
        {
            var pt = angle

            // We round to precision so we can compare with 0
            var length = utils.roundToPrecision( pt.length() )
            if( length != 0 )
            {
                _angleRad = Math.atan2( pt.y, pt.x )
            }
            else
            {
                _angleRad = 0
            }
            _angleDeg = Math.degrees( _angleRad )
        }

        _angleRad = utils.roundToPrecision( _angleRad )
        _angleDeg = utils.roundToPrecision( _angleDeg )
        _sin = Math.sin( _angleRad )
        _cos = Math.cos( _angleRad )
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties( this,
    {
        angle:
        {
            enumerable: true,
            get: function() { return _angleDeg },
            set: function( value ) { setAngle( value ) },
        },
        sin:
        {
            enumerable: true,
            get: function() { return _sin },
        },
        cos:
        {
            enumerable: true,
            get: function() { return _cos },
        },

        angleRadians:
        {
            enumerable: true,
            get: function() { return _angleRad },
        },
    })

    //
    // constructor code
    //
    setAngle( param )
}

//
// PUBLIC CLASS METHODS
//
Object.assign( Angle.prototype,
{
    neg: function()
    {
        return new Angle( new Point( this.cos, -this.sin ) )
    },

    add: function( other )
    {
        if( ! other instanceof Angle )
        {
            return new Angle( this )
        }

        return new Angle( this.angle + other.angle )
    },

    sub: function( other )
    {
        if( ! other instanceof Angle )
        {
            return new Angle( this )
        }

        return new Angle( this.angle - other.angle )
    },

    /** Returns a point which is at 'length' distance from (0,0) and at an angle to X-axis equal to this angle */
    toPoint: function( length )
    {
        if( ! Number.isFinite( length ) )
        {
            // This assumes length == 1.0
            return new Point( this.cos, this.sin )
        }

        return new Point( this.cos * length, this.sin * length )
    },
} )

export { Angle }
