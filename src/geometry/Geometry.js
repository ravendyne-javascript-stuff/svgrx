/**
    Direct conversion of code from http://antigrain.com/research/adaptive_bezier/#toc0009
*/

var flattenBezier3 = function(
    x1, y1,
    x2, y2,
    x3, y3)
{

    //------------------------------------------------------------------------
    const curve_collinearity_epsilon              = 1e-30
    const curve_angle_tolerance_epsilon           = 0.01
    const curve_recursion_limit = 32

    var m_points = []
    var m_count = 0
    var m_approximation_scale = 1.0
    var m_angle_tolerance = 0.0

    var m_distance_tolerance

    var init = function(
        x1, y1,
        x2, y2,
        x3, y3)
    {
        m_points = []
        m_distance_tolerance = 0.5 / m_approximation_scale
        m_distance_tolerance *= m_distance_tolerance
        bezier(x1, y1, x2, y2, x3, y3)
        m_count = 0
    }


    //------------------------------------------------------------------------
    var recursive_bezier = function(
        x1, y1,
        x2, y2,
        x3, y3,
        level)
    {
        if(level > curve_recursion_limit) 
        {
            return
        }

        // Calculate all the mid-points of the line segments
        //----------------------
        var x12   = (x1 + x2) / 2                
        var y12   = (y1 + y2) / 2
        var x23   = (x2 + x3) / 2
        var y23   = (y2 + y3) / 2
        var x123  = (x12 + x23) / 2
        var y123  = (y12 + y23) / 2

        var dx = x3-x1
        var dy = y3-y1
        var d = Math.abs(((x2 - x3) * dy - (y2 - y3) * dx))

        if(d > curve_collinearity_epsilon)
        { 
            // Regular care
            //-----------------
            if(d * d <= m_distance_tolerance * (dx*dx + dy*dy))
            {
                // If the curvature doesn't exceed the distance_tolerance value
                // we tend to finish subdivisions.
                //----------------------
                if(m_angle_tolerance < curve_angle_tolerance_epsilon)
                {
                    m_points.push(SVGRX.Point(x123, y123))
                    return
                }

                // Angle & Cusp Condition
                //----------------------
                var da = Math.abs(Math.atan2(y3 - y2, x3 - x2) - Math.atan2(y2 - y1, x2 - x1))
                if(da >= pi) da = 2*pi - da

                if(da < m_angle_tolerance)
                {
                    // Finally we can stop the recursion
                    //----------------------
                    m_points.push(SVGRX.Point(x123, y123))
                    return                 
                }
            }
        }
        else
        {
            // Collinear case
            //-----------------
            dx = x123 - (x1 + x3) / 2
            dy = y123 - (y1 + y3) / 2
            if(dx*dx + dy*dy <= m_distance_tolerance)
            {
                m_points.push(SVGRX.Point(x123, y123))
                return
            }
        }

        // Continue subdivision
        //----------------------
        recursive_bezier(x1, y1, x12, y12, x123, y123, level + 1) 
        recursive_bezier(x123, y123, x23, y23, x3, y3, level + 1) 
    }

    //------------------------------------------------------------------------
    var bezier = function(
        x1, y1,
        x2, y2,
        x3, y3)
    {
        m_points.push(SVGRX.Point(x1, y1))
        recursive_bezier(x1, y1, x2, y2, x3, y3, 0)
        m_points.push(SVGRX.Point(x3, y3))
    }

    init( x1, y1, x2, y2, x3, y3 )
    return m_points

}

/**
    Direct conversion of code from http://antigrain.com/research/adaptive_bezier/#toc0009
*/

var flattenBezier4 = function(
    x1, y1,
    x2, y2,
    x3, y3,
    x4, y4)
{

    //------------------------------------------------------------------------
    const curve_collinearity_epsilon              = 1e-30
    const curve_angle_tolerance_epsilon           = 0.01
    const curve_recursion_limit = 32

    var m_points = []
    var m_count = 0
    var m_approximation_scale = 1.0
    var m_angle_tolerance = 0.0
    var m_cusp_limit = 0.0

    var m_distance_tolerance

    var init = function(
        x1, y1,
        x2, y2,
        x3, y3,
        x4, y4)
    {
        m_points = []
        m_distance_tolerance = 0.5 / m_approximation_scale
        m_distance_tolerance *= m_distance_tolerance
        bezier(x1, y1, x2, y2, x3, y3, x4, y4)
        m_count = 0
    }

    //------------------------------------------------------------------------
    var recursive_bezier = function(
        x1, y1,
        x2, y2,
        x3, y3,
        x4, y4,
        level)
    {
        if(level > curve_recursion_limit) 
        {
            return
        }

        // Calculate all the mid-points of the line segments
        //----------------------
        var x12   = (x1 + x2) / 2
        var y12   = (y1 + y2) / 2
        var x23   = (x2 + x3) / 2
        var y23   = (y2 + y3) / 2
        var x34   = (x3 + x4) / 2
        var y34   = (y3 + y4) / 2
        var x123  = (x12 + x23) / 2
        var y123  = (y12 + y23) / 2
        var x234  = (x23 + x34) / 2
        var y234  = (y23 + y34) / 2
        var x1234 = (x123 + x234) / 2
        var y1234 = (y123 + y234) / 2

        if(level > 0) // Enforce subdivision first time
        {
            // Try to approximate the full cubic curve by a single straight line
            //------------------
            var dx = x4-x1
            var dy = y4-y1

            var d2 = Math.abs(((x2 - x4) * dy - (y2 - y4) * dx))
            var d3 = Math.abs(((x3 - x4) * dy - (y3 - y4) * dx))

            var da1, da2

            if(d2 > curve_collinearity_epsilon && d3 > curve_collinearity_epsilon)
            { 
                // Regular care
                //-----------------
                if((d2 + d3)*(d2 + d3) <= m_distance_tolerance * (dx*dx + dy*dy))
                {
                    // If the curvature doesn't exceed the distance_tolerance value
                    // we tend to finish subdivisions.
                    //----------------------
                    if(m_angle_tolerance < curve_angle_tolerance_epsilon)
                    {
                        m_points.push(SVGRX.Point(x1234, y1234))
                        return
                    }

                    // Angle & Cusp Condition
                    //----------------------
                    var a23 = Math.atan2(y3 - y2, x3 - x2)
                    da1 = Math.abs(a23 - Math.atan2(y2 - y1, x2 - x1))
                    da2 = Math.abs(Math.atan2(y4 - y3, x4 - x3) - a23)
                    if(da1 >= pi) da1 = 2*pi - da1
                    if(da2 >= pi) da2 = 2*pi - da2

                    if(da1 + da2 < m_angle_tolerance)
                    {
                        // Finally we can stop the recursion
                        //----------------------
                        m_points.push(SVGRX.Point(x1234, y1234))
                        return
                    }

                    if(m_cusp_limit != 0.0)
                    {
                        if(da1 > m_cusp_limit)
                        {
                            m_points.push(SVGRX.Point(x2, y2))
                            return
                        }

                        if(da2 > m_cusp_limit)
                        {
                            m_points.push(SVGRX.Point(x3, y3))
                            return
                        }
                    }
                }
            }
            else
            {
                if(d2 > curve_collinearity_epsilon)
                {
                    // p1,p3,p4 are collinear, p2 is considerable
                    //----------------------
                    if(d2 * d2 <= m_distance_tolerance * (dx*dx + dy*dy))
                    {
                        if(m_angle_tolerance < curve_angle_tolerance_epsilon)
                        {
                            m_points.push(SVGRX.Point(x1234, y1234))
                            return
                        }

                        // Angle Condition
                        //----------------------
                        da1 = Math.abs(Math.atan2(y3 - y2, x3 - x2) - Math.atan2(y2 - y1, x2 - x1))
                        if(da1 >= pi) da1 = 2*pi - da1

                        if(da1 < m_angle_tolerance)
                        {
                            m_points.push(SVGRX.Point(x2, y2))
                            m_points.push(SVGRX.Point(x3, y3))
                            return
                        }

                        if(m_cusp_limit != 0.0)
                        {
                            if(da1 > m_cusp_limit)
                            {
                                m_points.push(SVGRX.Point(x2, y2))
                                return
                            }
                        }
                    }
                }
                else
                if(d3 > curve_collinearity_epsilon)
                {
                    // p1,p2,p4 are collinear, p3 is considerable
                    //----------------------
                    if(d3 * d3 <= m_distance_tolerance * (dx*dx + dy*dy))
                    {
                        if(m_angle_tolerance < curve_angle_tolerance_epsilon)
                        {
                            m_points.push(SVGRX.Point(x1234, y1234))
                            return
                        }

                        // Angle Condition
                        //----------------------
                        da1 = Math.abs(Math.atan2(y4 - y3, x4 - x3) - Math.atan2(y3 - y2, x3 - x2))
                        if(da1 >= pi) da1 = 2*pi - da1

                        if(da1 < m_angle_tolerance)
                        {
                            m_points.push(SVGRX.Point(x2, y2))
                            m_points.push(SVGRX.Point(x3, y3))
                            return
                        }

                        if(m_cusp_limit != 0.0)
                        {
                            if(da1 > m_cusp_limit)
                            {
                                m_points.push(SVGRX.Point(x3, y3))
                                return
                            }
                        }
                    }
                }
                else
                {
                    // Collinear case
                    //-----------------
                    dx = x1234 - (x1 + x4) / 2
                    dy = y1234 - (y1 + y4) / 2
                    if(dx*dx + dy*dy <= m_distance_tolerance)
                    {
                        m_points.push(SVGRX.Point(x1234, y1234))
                        return
                    }
                }
            }
        }

        // Continue subdivision
        //----------------------
        recursive_bezier(x1, y1, x12, y12, x123, y123, x1234, y1234, level + 1)
        recursive_bezier(x1234, y1234, x234, y234, x34, y34, x4, y4, level + 1)
    }

    //------------------------------------------------------------------------
    var bezier = function(
        x1, y1, 
        x2, y2, 
        x3, y3, 
        x4, y4)
    {
        m_points.push(SVGRX.Point(x1, y1))
        recursive_bezier(x1, y1, x2, y2, x3, y3, x4, y4, 0)
        m_points.push(SVGRX.Point(x4, y4))
    }

    init( x1, y1, x2, y2, x3, y3, x4, y4 )
    return m_points
}

export { flattenBezier3 }
export { flattenBezier4 }
