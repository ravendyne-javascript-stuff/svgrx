/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { Point } from './Point.js'

import { isNumericArray, isPointArray, pointArrayToNumeric } from '../util/lib.js'

/**
 * Base class for 2nd, 3rd and 4th order Bezier curve classes.
 * Contains general properties and methods.
 * This class is intended to be subclassed and not used directly.
 */
function Bezier( points )
{
    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _order = 0
    var _points = []

    var _start = new Point()
    var _end = new Point()
    var _cpoints = []

    //
    // PRIVATE FUNCTIONS
    //
    var setOrderAndPoints = function( elements )
    {
        if( isPointArray( elements ) && elements.length >= 2 )
        {
            // unpack Point array to array of coordinates
            elements = pointArrayToNumeric( elements )
        }

        if( ! isNumericArray( elements ) )
        {
            return
        }

        if( elements.length < 4 || elements.length > 8 || elements.length % 2 != 0 )
        {
            return
        }

        _order = elements.length / 2
        _points = elements.slice()
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties( this,
    {
        order:
        {
            get: function() { return _order },
        },
        points:
        {
            get: function() { return _points },
        },

        start:
        {
            get: function() { return _start },
            set: function( value ) { _start = new Point( value ) },
        },
        end:
        {
            get: function() { return _end },
            set: function( value ) { _end = new Point( value ) },
        },
        cpoints:
        {
            get: function() { return _cpoints },
            set: function( value ) { if( isPointArray( value ) ) _cpoints = value },
        },
    })

    //
    // constructor code
    //
    setOrderAndPoints( points )
    this.createControlPoints()

}

//
// PUBLIC CLASS METHODS
//
Object.assign( Bezier.prototype,
{
    toString: function()
    {
        let str = 'Bezier' + String(this.order) + ' : '
        if( this.order >= 2 )
        {
            str += this.P0 + ', ' + this.P1
        }
        if( this.order >= 3 )
        {
            str += ', ' + this.P2
        }
        if( this.order >= 4 )
        {
            str += ', ' + this.P3
        }
        return str
    },

    /**
     * P0, P1, P2 and P3 should be declared in subclasses.
     * If they're not, this method will create them,
     * otherwise it will overwrite/update the existing ones.
     */
    createControlPoints: function()
    {
        if( this.order >= 2 )
        {
            // Linear curve
            this.P0 = new Point( this.points[ 0 ], this.points[ 1 ] )
            this.P1 = new Point( this.points[ 2 ], this.points[ 3 ] )

            this.start = this.P0
            this.end = this.P1
            this.cpoints = []
        }
        if( this.order >= 3 )
        {
            // Quadratic curve
            this.P2 = new Point( this.points[ 4 ], this.points[ 5 ] )

            this.end = this.P2
            this.cpoints = [ this.P1 ]
        }
        if( this.order >= 4 )
        {
            // Cubic curve
            this.P3 = new Point( this.points[ 6 ], this.points[ 7 ] )

            this.end = this.P3
            this.cpoints = [ this.P1, this.P2 ]
        }
    },

    // Override in subclass
    _interpolate: function( t )
    {
        return new Point()
    },

    evaluate: function( t )
    {
        if( t < 0 )
        {
            return this.start
        }
        if( t > 1 )
        {
            return this.end
        }

        return this._interpolate( t )
    },

    // Override in subclass
    transform: function( matrix )
    {
        return new Bezier( this.points )
    },
} )

export { Bezier }
