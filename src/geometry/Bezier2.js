/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { Point } from './Point.js'
import { Bezier } from './Bezier.js'

/**
 * Implements linear Bezier curve
 */
function Bezier2( points )
{
    //
    // Case when Bezier2 is called as a function
    //
    if( ! ( this instanceof Bezier2 ) )
    {
        return new Bezier2( points )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _P0 = new Point()
    var _P1 = new Point()

    //
    // PRIVATE FUNCTIONS
    //

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties( this,
    {
        P0:
        {
            enumerable: true,
            get: function() { return _P0 },
            set: function( value ) { _P0 = new Point( value ) },
        },
        P1:
        {
            enumerable: true,
            get: function() { return _P1 },
            set: function( value ) { _P1 = new Point( value ) },
        },
    })

    //
    // constructor code
    //
    Bezier.call( this, points )

}


//
// CLASS INHERITANCE
//
Bezier2.prototype = Object.assign( Object.create( Bezier.prototype ),
{
    constructor: Bezier2,
})


//
// PUBLIC CLASS METHODS
//
Object.assign( Bezier2.prototype,
{
    toString: function()
    {
        return 'Bezier L : ' + this.P0 + ', ' + this.P1
    },

    // Called by Bezier.evaluate(t)s
    // Bernstein polinomial evaluation
    _interpolate: function( t )
    {
        // B( t ) =
        //      ( 1 - t ) * P0 +
        //      t * P1

        // ( 1 - t ) * P0
        var A = this.P0.mul( 1 - t )
        // t * P1
        var B = this.P1.mul( t )

        // B( t ) = A + B
        var pt = A.add( B )

        return pt
    },

    transform: function( matrix )
    {
        return new Bezier2( [
            matrix.mul( this.P0 ),
            matrix.mul( this.P1 ),
        ] )
    },
} )


export { Bezier2 }
