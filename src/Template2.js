/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { Transformable } from './Transformable.js'

function Clazz( param )
{
    Transformable.call( this, 0 )

    //
    // Case when Clazz is called as a function
    //
    if( ! ( this instanceof Clazz ) )
    {
        return new Clazz( param )
    }

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _prop = -1

    //
    // PRIVATE FUNCTIONS
    //

    var parse = function( param )
    {
        ;
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        // private properties getters/setters
        // so we don't have to, i.e. check if assigned values are numbers
        // or to try/catch when working with private props in every other method
        propBar:
        {
            get: function() { return _prop },
            set: function( value ) { if( Number.isFinite( value ) ) _prop = value },
        },
    })

    //
    // constructor code
    //
    if( typeof docText === 'string' )
    {
        _docText = docText
    }
    if( param instanceof XMLDocument )
    {
        parse( param )
    }

    //
    // initialize private property accessors here
    //
    this.baseClassProp = 'fubar'
}

//
// PUBLIC CLASS METHODS
//
Object.assign( Clazz.prototype,
{
    /*
    propBar:
    {
        // property descriptor
        // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty#Description
        configurable: false,
        get: function() { return 10 },
        set: function(value) { console.log('Setter ', value) }
    },
    */
    toString: function()
    {
        return 'Clazz: ' + this.propBar
    },
} )



//
// CLASS INHERITANCE
//
Clazz.prototype = Object.assign( Object.create( Transformable.prototype ),
{
    constructor: Clazz,
    isClazz: true,
    otherMethod: function( param )
    {
    },
})

export { Clazz }
