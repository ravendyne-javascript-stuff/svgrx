/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { GraphicsElement } from './GraphicsElement.js'

import { Bezier2, Matrix } from '../geometry/lib.js'
import { flattenArray, lengthInBaseUnits } from '../util/lib.js'

import { collectMatches, MatchRegex } from '../types/Matchers.js'

/**
 * https://www.w3.org/TR/SVG/shapes.html#PolygonElement
 * param - {...} or Polygon
 */
function Polygon( param )
{
    //
    // Case when Polygon is called as a function
    //
    if( ! ( this instanceof Polygon ) )
    {
        return new Polygon( param )
    }

    GraphicsElement.call( this, param )

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _points = ''
    var _pointsdData = []

    const _decimalNumberRegexStr = MatchRegex.decimalNumber
    const _pointsRegex = new RegExp( `${_decimalNumberRegexStr}\\s?,\\s?${_decimalNumberRegexStr}`, 'g' )

    //
    // PRIVATE FUNCTIONS
    //

    // https://www.w3.org/TR/SVG/shapes.html#PolygonElementPointsAttribute
    var parsePoints = function( data )
    {
        _pointsdData = collectMatches( _pointsRegex, data )
        _pointsdData = _pointsdData.map( ( val ) =>
            {
                let parts = val.split(',')
                return [ parseFloat( parts[0].trim() ) || 0, parseFloat( parts[1].trim() ) || 0 ]
            } )
    }

    var setPropertiesFromAttributes = function( attributes )
    {
        _points = attributes['points'] || _points
        if( typeof _points !== 'string' )
        {
            _points = ''
        }

        parsePoints( _points )
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        points:
        {
            enumerable: true,
            get: function() { return _points },
        },

        pointsData:
        {
            get: function() { return _pointsdData },
        },
    })

    //
    // constructor code
    //
    if( param instanceof Polygon )
    {
        _points = param.points
        parsePoints( _points )
    }
    else if( typeof param === 'object' )
    {
        setPropertiesFromAttributes( this.attrs )
    }
}


//
// CLASS INHERITANCE
//
Polygon.prototype = Object.assign( Object.create( GraphicsElement.prototype ),
{
    constructor: Polygon,
    isPolygon: true,
})


//
// PUBLIC CLASS METHODS
//
Object.assign( Polygon.prototype,
{
    toString: function()
    {
        return `<Polygon ${this.id}>`
    },

    /*
        - perform an absolute moveto operation to the first coordinate pair in the list of points
        - for each subsequent coordinate pair, perform an absolute lineto operation to that coordinate pair
        - perform a closepath command
    */
    convert: function( matrix )
    {
        if( this.pointsData.length == 0 )
        {
            return []
        }

        if( ! ( matrix instanceof Matrix ) )
        {
            matrix = new Matrix()
        }

        let ctm = this.matrix.mul( matrix )
        var result = []

        // perform an absolute moveto operation to the first coordinate pair in the list of points
        let current = this.pointsData[ 0 ]
        let start = current
        let line

        for( let idx = 0; idx < this.pointsData.length; idx++ )
        {
            // for each subsequent coordinate pair, perform an absolute lineto operation to that coordinate pair
            let here = this.pointsData[ idx ]
            line = Bezier2( flattenArray( [ current, here ] ) )

            result.push( line )

            current = here
        }

        // perform a closepath command
        line = Bezier2( flattenArray( [ current, start ] ) )
        result.push( line )

        return result.map( ( el ) => el.transform( ctm ) )
    },
} )


export { Polygon }
