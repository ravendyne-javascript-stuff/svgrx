/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { GraphicsElement } from './GraphicsElement.js'

import { Point, Bezier4, Matrix } from '../geometry/lib.js'
import { flattenArray, pointArrayToNumeric, lengthInBaseUnits } from '../util/lib.js'

/**
 * https://www.w3.org/TR/SVG/shapes.html#CircleElement
 * param - {...} or Circle
 */
function Circle( param )
{
    //
    // Case when Circle is called as a function
    //
    if( ! ( this instanceof Circle ) )
    {
        return new Circle( param )
    }

    GraphicsElement.call( this, param )

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _center = new Point( 0, 0 )
    var _r = 0

    //
    // PRIVATE FUNCTIONS
    //
    var setPropertiesFromAttributes = function( attributes )
    {
        var cx  = lengthInBaseUnits( attributes['cx'] )
        var cy  = lengthInBaseUnits( attributes['cy'] )
        _r      = lengthInBaseUnits( attributes['r'] )
        _center = new Point( cx, cy )
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        cx:
        {
            enumerable: true,
            get: function() { return _center.x },
        },
        cy:
        {
            enumerable: true,
            get: function() { return _center.y },
        },
        r:
        {
            enumerable: true,
            get: function() { return _r },
        },

        center:
        {
            get: function() { return _center },
        },
    })

    //
    // constructor code
    //
    if( param instanceof Circle )
    {
        _center = param.center
        _r = param.r
    }
    else if( typeof param === 'object' )
    {
        setPropertiesFromAttributes( this.attrs )
    }
}


//
// CLASS INHERITANCE
//
Circle.prototype = Object.assign( Object.create( GraphicsElement.prototype ),
{
    constructor: Circle,
    isCircle: true,
})


//
// PUBLIC CLASS METHODS
//
Object.assign( Circle.prototype,
{
    toString: function()
    {
        return `<Circle ${this.id}>`
    },

    // http://spencermortensen.com/articles/bezier-circle/
    convert: function( matrix )
    {
        if( ! ( matrix instanceof Matrix ) )
        {
            matrix = new Matrix()
        }

        let ctm = this.matrix.mul( matrix )
        const c = 0.55191502449

        // circle centerd on (0, 0) with radius 1
        let segmentA = [ Point(  0,  1 ), Point(  c,  1 ), Point(  1,  c ), Point(  1,  0 ) ]
        let segmentB = [ Point(  1,  0 ), Point(  1, -c ), Point(  c, -1 ), Point(  0, -1 ) ]
        let segmentC = [ Point(  0, -1 ), Point( -c, -1 ), Point( -1, -c ), Point( -1,  0 ) ]
        let segmentD = [ Point( -1,  0 ), Point( -1,  c ), Point( -c,  1 ), Point(  0,  1 ) ]

        // scale segments to the circle radius size
        segmentA = segmentA.map( ( val ) => val.mul( this.r ) )
        segmentB = segmentB.map( ( val ) => val.mul( this.r ) )
        segmentC = segmentC.map( ( val ) => val.mul( this.r ) )
        segmentD = segmentD.map( ( val ) => val.mul( this.r ) )

        // translate segments to match the circle center (cx, cy)
        segmentA = segmentA.map( ( val ) => val.add( this.center ) )
        segmentB = segmentB.map( ( val ) => val.add( this.center ) )
        segmentC = segmentC.map( ( val ) => val.add( this.center ) )
        segmentD = segmentD.map( ( val ) => val.add( this.center ) )

        // convert to cubic bezier curves
        segmentA = new Bezier4( pointArrayToNumeric( segmentA ) )
        segmentB = new Bezier4( pointArrayToNumeric( segmentB ) )
        segmentC = new Bezier4( pointArrayToNumeric( segmentC ) )
        segmentD = new Bezier4( pointArrayToNumeric( segmentD ) )

        return [ segmentA.transform( ctm ), segmentB.transform( ctm ), segmentC.transform( ctm ), segmentD.transform( ctm ) ]
    },
} )


export { Circle }
