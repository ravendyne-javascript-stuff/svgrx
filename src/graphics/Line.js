/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { GraphicsElement } from './GraphicsElement.js'

import { Point, Bezier2, Matrix } from '../geometry/lib.js'
import { pointArrayToNumeric, lengthInBaseUnits } from '../util/lib.js'

/**
 * https://www.w3.org/TR/SVG/shapes.html#LineElement
 * param - {...} or Line
 */
function Line( param )
{
    //
    // Case when Line is called as a function
    //
    if( ! ( this instanceof Line ) )
    {
        return new Line( param )
    }

    GraphicsElement.call( this, param )

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _P1 = new Point( 0, 0 )
    var _P2 = new Point( 0, 0 )

    //
    // PRIVATE FUNCTIONS
    //
    var setPropertiesFromAttributes = function( attributes )
    {
        var x1 = lengthInBaseUnits( attributes['x1'] )
        var y1 = lengthInBaseUnits( attributes['y1'] )
        var x2 = lengthInBaseUnits( attributes['x2'] )
        var y2 = lengthInBaseUnits( attributes['y2'] )
        _P1 = new Point( x1, y1 )
        _P2 = new Point( x2, y2 )
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        x1:
        {
            enumerable: true,
            get: function() { return _P1.x },
        },
        y1:
        {
            enumerable: true,
            get: function() { return _P1.y },
        },
        x2:
        {
            enumerable: true,
            get: function() { return _P2.x },
        },
        y2:
        {
            enumerable: true,
            get: function() { return _P2.y },
        },

        P1:
        {
            get: function() { return _P1 },
        },
        P2:
        {
            get: function() { return _P2 },
        },
    })

    //
    // constructor code
    //
    if( param instanceof Line )
    {
        _P1 = param.P1
        _P2 = param.P2
    }
    else if( typeof param === 'object' )
    {
        setPropertiesFromAttributes( this.attrs )
    }
}


//
// CLASS INHERITANCE
//
Line.prototype = Object.assign( Object.create( GraphicsElement.prototype ),
{
    constructor: Line,
    isLine: true,
})


//
// PUBLIC CLASS METHODS
//
Object.assign( Line.prototype,
{
    toString: function()
    {
        return `<Line ${this.id}>`
    },

    convert: function( matrix )
    {
        if( ! ( matrix instanceof Matrix ) )
        {
            matrix = new Matrix()
        }

        let ctm = this.matrix.mul( matrix )
        let element = new Bezier2( pointArrayToNumeric( [ this.P1, this.P2 ] ) )

        return [ element.transform( ctm ) ]
    },
} )


export { Line }
