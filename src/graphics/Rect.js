/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { GraphicsElement } from './GraphicsElement.js'

import { Point, Bezier2, Matrix } from '../geometry/lib.js'

import { flattenArray, pointArrayToNumeric, lengthInBaseUnits } from '../util/lib.js'

/**
 * https://www.w3.org/TR/SVG/shapes.html#RectElement
 * param - {...} or Rect
 */
function Rect( param )
{
    //
    // Case when Rect is called as a function
    //
    if( ! ( this instanceof Rect ) )
    {
        return new Rect( param )
    }

    GraphicsElement.call( this, param )

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _bottomLeft = new Point( 0, 0 )
    var _topRight   = new Point( 0, 0 )
    var _rx = 0
    var _ry = 0

    //
    // PRIVATE FUNCTIONS
    //
    var setPropertiesFromAttributes = function( attributes )
    {
        var x      = lengthInBaseUnits( attributes['x'] )
        var y      = lengthInBaseUnits( attributes['y'] )
        var width  = lengthInBaseUnits( attributes['width'] )
        var height = lengthInBaseUnits( attributes['height'] )
        _rx        = lengthInBaseUnits( attributes['rx'] || _rx )
        _ry        = lengthInBaseUnits( attributes['ry'] || _ry )

        _bottomLeft = new Point( x, y )
        _topRight   = new Point( x + width, y + height )
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        x:
        {
            enumerable: true,
            get: function() { return _bottomLeft.x },
        },
        y:
        {
            enumerable: true,
            get: function() { return _bottomLeft.y },
        },
        width:
        {
            enumerable: true,
            get: function() { return _topRight.x - _bottomLeft.x },
        },
        height:
        {
            enumerable: true,
            get: function() { return _topRight.y - _bottomLeft.y },
        },
        rx:
        {
            enumerable: true,
            get: function() { return _rx },
        },
        ry:
        {
            enumerable: true,
            get: function() { return _ry },
        },

        bottomLeft:
        {
            get: function() { return _bottomLeft },
        },
        topRight:
        {
            get: function() { return _topRight },
        },
    })

    //
    // constructor code
    //
    if( param instanceof Rect )
    {
        _bottomLeft = param.bottomLeft
        _topRight = param.topRight
        _rx = param.rx
        _ry = param.ry
    }
    else if( typeof param === 'object' )
    {
        setPropertiesFromAttributes( this.attrs )
    }
}


//
// CLASS INHERITANCE
//
Rect.prototype = Object.assign( Object.create( GraphicsElement.prototype ),
{
    constructor: Rect,
    isRect: true,
})


//
// PUBLIC CLASS METHODS
//
Object.assign( Rect.prototype,
{
    toString: function()
    {
        return `<Rect ${this.id}>`
    },

    convert: function( matrix )
    {
        if( ! ( matrix instanceof Matrix ) )
        {
            matrix = new Matrix()
        }

        let ctm = this.matrix.mul( matrix )
        let result = []
        let heightAsPt = new Point( 0, this.height )

        let A = this.bottomLeft
        let B = this.bottomLeft.add( heightAsPt )
        let C = this.topRight
        let D = this.topRight.sub( heightAsPt )

        A = matrix.mul( A )
        B = matrix.mul( B )
        C = matrix.mul( C )
        D = matrix.mul( D )

        let left = new Bezier2( pointArrayToNumeric( [ A, B ] ) )
        let top = new Bezier2( pointArrayToNumeric( [ B, C ] ) )
        let right = new Bezier2( pointArrayToNumeric( [ C, D ] ) )
        let bottom = new Bezier2( pointArrayToNumeric( [ D, A ] ) )

        return [ left.transform( ctm ), top.transform( ctm ), right.transform( ctm ), bottom.transform( ctm ) ]
    },
} )


export { Rect }
