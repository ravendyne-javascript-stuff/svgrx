/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { GraphicsElement } from './GraphicsElement.js'

import { Bezier2, Matrix } from '../geometry/lib.js'
import { flattenArray, lengthInBaseUnits } from '../util/lib.js'

import { collectMatches, MatchRegex } from '../types/Matchers.js'

/**
 * https://www.w3.org/TR/SVG/shapes.html#PolylineElement
 * param - {...} or Polyline
 */
function Polyline( param )
{
    //
    // Case when Polyline is called as a function
    //
    if( ! ( this instanceof Polyline ) )
    {
        return new Polyline( param )
    }

    GraphicsElement.call( this, param )

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _points = ''
    var _pointsdData = []

    const _decimalNumberRegexStr = MatchRegex.decimalNumber
    const _pointsRegex = new RegExp( `${_decimalNumberRegexStr}\\s?,\\s?${_decimalNumberRegexStr}`, 'g' )

    //
    // PRIVATE FUNCTIONS
    //

    // https://www.w3.org/TR/SVG/shapes.html#PolylineElementPointsAttribute
    var parsePoints = function( data )
    {
        _pointsdData = collectMatches( _pointsRegex, data )
        _pointsdData = _pointsdData.map( ( val ) =>
            {
                let parts = val.split(',')
                return [ parseFloat( parts[0].trim() ) || 0, parseFloat( parts[1].trim() ) || 0 ]
            } )
    }

    var setPropertiesFromAttributes = function( attributes )
    {
        _points = attributes['points'] || _points
        if( typeof _points !== 'string' )
        {
            _points = ''
        }

        parsePoints( _points )
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        points:
        {
            enumerable: true,
            get: function() { return _points },
        },

        pointsData:
        {
            get: function() { return _pointsdData },
        },
    })

    //
    // constructor code
    //
    if( param instanceof Polyline )
    {
        _points = param.points
        parsePoints( _points )
    }
    else if( typeof param === 'object' )
    {
        setPropertiesFromAttributes( this.attrs )
    }
}


//
// CLASS INHERITANCE
//
Polyline.prototype = Object.assign( Object.create( GraphicsElement.prototype ),
{
    constructor: Polyline,
    isPolyline: true,
})


//
// PUBLIC CLASS METHODS
//
Object.assign( Polyline.prototype,
{
    toString: function()
    {
        return `<Polyline ${this.id}>`
    },

    /*
        - perform an absolute moveto operation to the first coordinate pair in the list of points
        - for each subsequent coordinate pair, perform an absolute lineto operation to that coordinate pair
    */
    convert: function( matrix )
    {
        if( this.pointsData.length == 0 )
        {
            return []
        }

        if( ! ( matrix instanceof Matrix ) )
        {
            matrix = new Matrix()
        }

        let ctm = this.matrix.mul( matrix )
        var result = []

        // perform an absolute moveto operation to the first coordinate pair in the list of points
        let current = this.pointsData[ 0 ]
        let start = current
        let line

        for( let idx = 0; idx < this.pointsData.length; idx++ )
        {
            // for each subsequent coordinate pair, perform an absolute lineto operation to that coordinate pair
            let here = this.pointsData[ idx ]
            line = Bezier2( flattenArray( [ current, here ] ) )

            result.push( line )

            current = here
        }

        return result.map( ( el ) => el.transform( ctm ) )
    },
} )


export { Polyline }
