/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { GraphicsElement } from './GraphicsElement.js'

import { Arc, Point, Bezier2, Bezier3, Bezier4, Matrix } from '../geometry/lib.js'
import { flattenArray } from '../util/lib.js'

import { collectMatches } from '../types/Matchers.js'

/**
 * https://www.w3.org/TR/SVG/paths.html#PathElement
 * param - {...} or Path
 */
function Path( param )
{
    //
    // Case when Path is called as a function
    //
    if( ! ( this instanceof Path ) )
    {
        return new Path( param )
    }

    GraphicsElement.call( this, param )

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _pathData = []
    var _d = ''

    const _pathDataCommands = 'MmZzLlHhVvCcSsQqTtAa'
    // https://www.w3.org/TR/SVG/paths.html#PathDataBNF
    const _pathDataRegexStr = `[-+]?(?:\\d+(?:\\.\\d*)?|\\.\\d+)(?:[eE][-+]?\\d+)?|\\ *[${_pathDataCommands}]\\ *`
    const _pathDataRegex = new RegExp( _pathDataRegexStr, 'g' )

    //
    // PRIVATE FUNCTIONS
    //

    var parseData = function( data )
    {
        // moveto (Mm), closepath (Zz), lineto (Ll), lineto H (Hh), lineto V (Vv)
        // curve cubic (CcSs), curve quadratic (QqTt), arc (Aa)

        _pathData = collectMatches( _pathDataRegex, data )
        var convertPathDataValue = function( val )
        {
            val = val.trim()
            if( ! _pathDataCommands.includes( val ) )
            {
                // if it's not command then it should be a coordinate
                // whatever is there that is not a number will be converted to zero
                val = parseFloat( val ) || 0
            }
            return val
        }
        _pathData = _pathData.map( convertPathDataValue )
    }
    // https://www.w3.org/TR/SVG/paths.html#PathData

    var setPropertiesFromAttributes = function( attributes )
    {
        _d = attributes['d'] || _d
        if( typeof _d !== 'string' )
        {
            _d = ''
        }

        parseData( _d )
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        d:
        {
            enumerable: true,
            get: function() { return _d },
        },

        pathData:
        {
            get: function() { return _pathData },
        },
    })

    //
    // constructor code
    //
    if( param instanceof Path )
    {
        _d = param.d
        parseData( _d )
    }
    else if( typeof param === 'object' )
    {
        setPropertiesFromAttributes( this.attrs )
    }
}


//
// CLASS INHERITANCE
//
Path.prototype = Object.assign( Object.create( GraphicsElement.prototype ),
{
    constructor: Path,
    isPath: true,
})


//
// PUBLIC CLASS METHODS
//
Object.assign( Path.prototype,
{
    toString: function()
    {
        return `<Path ${this.id}>`
    },

    convert: function( matrix )
    {
        if( this.pathData.length == 0 )
        {
            return []
        }

        if( ! ( matrix instanceof Matrix ) )
        {
            matrix = new Matrix()
        }

        let ctm = this.matrix.mul( matrix )
        var result = []

        let pathData = this.pathData

        let startPoint = null
        let currentPoint = [ 0, 0 ]
        let prevControlPoint = currentPoint

        let command = null
        let coordinatesAreAbsolute = true

        let idx = 0
        while( idx < pathData.length )
        {
            let data = pathData[ idx ]

            // string data is a command
            if( typeof data === 'string' )
            {
                command = data
                idx++

                coordinatesAreAbsolute = ( command == command.toUpperCase() )
                command = command.toUpperCase()
            }
            // otherwise it is a number, which is ensured by parseData() in constructor

            // https://www.w3.org/TR/SVG/paths.html#PathDataMovetoCommands
            if( command == 'M')
            {
                let x = pathData[ idx ]
                idx++
                let y = pathData[ idx ]
                idx++
                // let pt = new Point( x, y )

                if( ! coordinatesAreAbsolute )
                {
                    x += currentPoint[ 0 ]
                    y += currentPoint[ 1 ]
                }

                currentPoint = [ x, y ]

                // Reset the subpath start point
                startPoint = currentPoint

                // If there happens to be more coordinates after initial 'M'
                // they are treated as LineTo commands. We set 'command' here
                // and the while loop will fallthrough to the LineTo handling code
                // if current idx points to a number. If not, 'command' will be
                // set to whatever the current command might be.
                command = 'L'
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataLinetoCommands
            else if( 'LHV'.includes( command ) )
            {
                let x = 0
                let y = 0

                // The way to fill in
                // y for 'H' and
                // x for 'V'
                if( coordinatesAreAbsolute )
                {
                    x = currentPoint[ 0 ]
                    y = currentPoint[ 1 ]
                }

                switch( command )
                {
                    case 'L':
                        x = pathData[ idx ]
                        idx++
                        y = pathData[ idx ]
                        idx++
                    break
                    case 'H':
                        x = pathData[ idx ]
                        idx++
                    break
                    case 'V':
                        y = pathData[ idx ]
                        idx++
                    break
                }

                if( ! coordinatesAreAbsolute )
                {
                    x += currentPoint[ 0 ]
                    y += currentPoint[ 1 ]
                }

                let points = flattenArray( [ currentPoint, x, y ] )

                result.push( new Bezier2( points ) )

                currentPoint = [ x, y ]
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataQuadraticBezierCommands
            else if( command == 'Q' )
            {
                // currentPoint = Start point - P0
                // Control point - P1
                let x1 = pathData[ idx ]
                idx++
                let y1 = pathData[ idx ]
                idx++
                // End point - P2
                let x = pathData[ idx ]
                idx++
                let y = pathData[ idx ]
                idx++

                if( ! coordinatesAreAbsolute )
                {
                    x1 += currentPoint[ 0 ]
                    y1 += currentPoint[ 1 ]
                    x += currentPoint[ 0 ]
                    y += currentPoint[ 1 ]
                }

                let points = flattenArray( [ currentPoint, x1, y1, x, y ] )

                result.push( new Bezier3( points ) )

                // Save control point for 'T' command
                prevControlPoint = [ x1, y1 ]

                currentPoint = [ x, y ]
            }
            else if( command == 'T' )
            {
                // currentPoint = Start point - P0

                // prevControlPoint = Control point - P1

                // https://www.w3.org/TR/SVG/implnote.html#PathElementImplementationNotes
                // We need to rotate prevControlPoint 180 degrees around currentPoint
                // to get the control point for this curve.
                // Rotating point (x,y) 180 degrees around point (x0, y0):
                // x' = 2 * x0 - x
                // y' = 2 * y0 - y
                let x1 = 2 * currentPoint[ 0 ] - prevControlPoint[ 0 ]
                let y1 = 2 * currentPoint[ 1 ] - prevControlPoint[ 1 ]

                // End point - P2
                let x = pathData[ idx ]
                idx++
                let y = pathData[ idx ]
                idx++

                if( ! coordinatesAreAbsolute )
                {
                    x += currentPoint[ 0 ]
                    y += currentPoint[ 1 ]
                }

                let points = flattenArray( [ currentPoint, x1, y1, x, y ] )

                result.push( new Bezier3( points ) )

                // Save control point for 'T' command
                prevControlPoint = [ x1, y1 ]

                currentPoint = [ x, y ]
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataCubicBezierCommands
            else if( command == 'C' )
            {
                // currentPoint = Start point - P0
                // Control point - P1
                let x1 = pathData[ idx ]
                idx++
                let y1 = pathData[ idx ]
                idx++
                // Control point - P2
                let x2 = pathData[ idx ]
                idx++
                let y2 = pathData[ idx ]
                idx++
                // End point - P3
                let x = pathData[ idx ]
                idx++
                let y = pathData[ idx ]
                idx++

                if( ! coordinatesAreAbsolute )
                {
                    x1 += currentPoint[ 0 ]
                    y1 += currentPoint[ 1 ]
                    x2 += currentPoint[ 0 ]
                    y2 += currentPoint[ 1 ]
                    x += currentPoint[ 0 ]
                    y += currentPoint[ 1 ]
                }

                let points = flattenArray( [ currentPoint, x1, y1, x2, y2, x, y ] )

                result.push( new Bezier4( points ) )

                // Save control point for 'S' command
                prevControlPoint = [ x2, y2 ]

                currentPoint = [ x, y ]
            }
            else if( command == 'S' )
            {
                // currentPoint = Start point - P0

                // prevControlPoint = Control point - P1

                // https://www.w3.org/TR/SVG/implnote.html#PathElementImplementationNotes
                // We need to rotate prevControlPoint 180 degrees around currentPoint
                // to get the control point for this curve.
                // Rotating point (x,y) 180 degrees around point (x0, y0):
                // x' = 2 * x0 - x
                // y' = 2 * y0 - y
                let x1 = 2 * currentPoint[ 0 ] - prevControlPoint[ 0 ]
                let y1 = 2 * currentPoint[ 1 ] - prevControlPoint[ 1 ]

                // Control point - P2
                let x2 = pathData[ idx ]
                idx++
                let y2 = pathData[ idx ]
                idx++
                // End point - P3
                let x = pathData[ idx ]
                idx++
                let y = pathData[ idx ]
                idx++

                if( ! coordinatesAreAbsolute )
                {
                    x2 += currentPoint[ 0 ]
                    y2 += currentPoint[ 1 ]
                    x += currentPoint[ 0 ]
                    y += currentPoint[ 1 ]
                }

                let points = flattenArray( [ currentPoint, x1, y1, x2, y2, x, y ] )

                result.push( new Bezier4( points ) )

                // Save control point for 'S' command
                prevControlPoint = [ x2, y2 ]

                currentPoint = [ x, y ]
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataEllipticalArcCommands
            // https://www.w3.org/TR/SVG/implnote.html#ArcImplementationNotes
            else if( command == 'A' )
            {
                // Parameters:
                // (rx ry x-axis-rotation large-arc-flag sweep-flag x y)+
                let rx = pathData[ idx ]
                idx++
                let ry = pathData[ idx ]
                idx++

                let xAxisRotation = pathData[ idx ]
                idx++
                let fA = pathData[ idx ]
                idx++
                let fS = pathData[ idx ]
                idx++

                let x = pathData[ idx ]
                idx++
                let y = pathData[ idx ]
                idx++

                if( ! coordinatesAreAbsolute )
                {
                    x += currentPoint[ 0 ]
                    y += currentPoint[ 1 ]
                }

                // Arc = ( x1, y1, rx, ry, phi, fA, fS, x2, y2 )
                var arc = Arc.createFromSVGParameters( currentPoint[ 0 ], currentPoint[ 1 ], rx, ry, xAxisRotation, fA, fS, x, y )
                // A neat trick to 'unpack' the array returned by arc.convert()
                // and not have to use code like
                // result = result.concat( arc.convert() )
                Array.prototype.push.apply( result, arc.convert() )

                currentPoint = [ x, y ]
            }

            // https://www.w3.org/TR/SVG/paths.html#PathDataClosePathCommand
            else if( command == 'Z' )
            {
                let points = flattenArray( [ currentPoint, startPoint ] )

                result.push( new Bezier2( points ) )

                currentPoint = startPoint
            }

            // Not a command
            else
            {
                // idx++
            }

            // Covers the case when initial command
            // in path data string is not 'M'
            if( startPoint === null )
            {
                startPoint = currentPoint
            }

        }

        return result.map( ( el ) => el.transform( ctm ) )
    },
} )


export { Path }
