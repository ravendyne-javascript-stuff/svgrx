/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { GraphicsElement } from './GraphicsElement.js'

import { Point, Matrix, Bezier4 } from '../geometry/lib.js'
import { flattenArray, pointArrayToNumeric, lengthInBaseUnits } from '../util/lib.js'

/**
 * https://www.w3.org/TR/SVG/shapes.html#EllipseElement
 * param - {...} or Ellipse
 */
function Ellipse( param )
{
    //
    // Case when Ellipse is called as a function
    //
    if( ! ( this instanceof Ellipse ) )
    {
        return new Ellipse( param )
    }

    GraphicsElement.call( this, param )

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _center = new Point( 0, 0 )
    var _rx = 0
    var _ry = 0

    //
    // PRIVATE FUNCTIONS
    //
    var setPropertiesFromAttributes = function( attributes )
    {
        var cx  = lengthInBaseUnits( attributes['cx'] )
        var cy  = lengthInBaseUnits( attributes['cy'] )
        _rx     = lengthInBaseUnits( attributes['rx'] )
        _ry     = lengthInBaseUnits( attributes['ry'] )
        _center = new Point( cx, cy )
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        cx:
        {
            enumerable: true,
            get: function() { return _center.x },
        },
        cy:
        {
            enumerable: true,
            get: function() { return _center.y },
        },
        rx:
        {
            enumerable: true,
            get: function() { return _rx },
        },
        ry:
        {
            enumerable: true,
            get: function() { return _ry },
        },

        center:
        {
            get: function() { return _center },
        },
    })

    //
    // constructor code
    //
    if( param instanceof Ellipse )
    {
        _center = param.center
        _rx = param.rx
        _ry = param.ry
    }
    else if( typeof param === 'object' )
    {
        setPropertiesFromAttributes( this.attrs )
    }
}


//
// CLASS INHERITANCE
//
Ellipse.prototype = Object.assign( Object.create( GraphicsElement.prototype ),
{
    constructor: Ellipse,
    isEllipse: true,
})


//
// PUBLIC CLASS METHODS
//
Object.assign( Ellipse.prototype,
{
    toString: function()
    {
        return `<Ellipse ${this.id}>`
    },

    // http://spencermortensen.com/articles/bezier-circle/
    // applied to ellipse by using scale transform on circle
    convert: function( matrix )
    {
        if( ! ( matrix instanceof Matrix ) )
        {
            matrix = new Matrix()
        }

        let ctm = this.matrix.mul( matrix )
        const c = 0.55191502449

        // circle centerd on (0, 0) with radius 1
        let segmentA = [ Point(  0,  1 ), Point(  c,  1 ), Point(  1,  c ), Point(  1,  0 ) ]
        let segmentB = [ Point(  1,  0 ), Point(  1, -c ), Point(  c, -1 ), Point(  0, -1 ) ]
        let segmentC = [ Point(  0, -1 ), Point( -c, -1 ), Point( -1, -c ), Point( -1,  0 ) ]
        let segmentD = [ Point( -1,  0 ), Point( -1,  c ), Point( -c,  1 ), Point(  0,  1 ) ]

        // scale segments to the ellipse axes sizes
        let scaleMatrix = new Matrix().scale( this.rx, this.ry )
        segmentA = segmentA.map( ( val ) => scaleMatrix.mul( val ) )
        segmentB = segmentB.map( ( val ) => scaleMatrix.mul( val ) )
        segmentC = segmentC.map( ( val ) => scaleMatrix.mul( val ) )
        segmentD = segmentD.map( ( val ) => scaleMatrix.mul( val ) )

        // translate segments to match the ellipse center (cx, cy)
        segmentA = segmentA.map( ( val ) => val.add( this.center ) )
        segmentB = segmentB.map( ( val ) => val.add( this.center ) )
        segmentC = segmentC.map( ( val ) => val.add( this.center ) )
        segmentD = segmentD.map( ( val ) => val.add( this.center ) )

        // convert to cubic bezier curves
        segmentA = new Bezier4( pointArrayToNumeric( segmentA ) )
        segmentB = new Bezier4( pointArrayToNumeric( segmentB ) )
        segmentC = new Bezier4( pointArrayToNumeric( segmentC ) )
        segmentD = new Bezier4( pointArrayToNumeric( segmentD ) )

        return [ segmentA.transform( ctm ), segmentB.transform( ctm ), segmentC.transform( ctm ), segmentD.transform( ctm ) ]
    },
} )


export { Ellipse }
