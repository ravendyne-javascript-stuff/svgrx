/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { TransformableElement } from '../core/TransformableElement.js'

/**
 * param - {...} or GraphicsElement
 */
function GraphicsElement( param )
{
    //
    // Case when GraphicsElement is called as a function
    //
    if( ! ( this instanceof GraphicsElement ) )
    {
        return new GraphicsElement( param )
    }

    TransformableElement.call( this, param )

    //
    // PRIVATE OBJECT PROPERTIES
    //

    //
    // PRIVATE FUNCTIONS
    //
    var setPropertiesFromAttributes = function( attributes )
    {
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
    })

    //
    // constructor code
    //
    if( param instanceof GraphicsElement )
    {
        setPropertiesFromAttributes( this.attrs )
    }
    else if( typeof param === 'object' )
    {
        setPropertiesFromAttributes( this.attrs )
    }
}

//
// PUBLIC CLASS METHODS
//
Object.assign( GraphicsElement.prototype,
{
    toString: function()
    {
        return `<GraphicsElement ${this.id}>`
    },
} )


//
// CLASS INHERITANCE
//
GraphicsElement.prototype = Object.assign( Object.create( TransformableElement.prototype ),
{
    constructor: GraphicsElement,
    isGraphicsElement: true,
})


export { GraphicsElement }
