/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

var MatchRegex =
{
    // 10, 10.00, .10, +10, -10
    decimalNumber: '[-+]?(?:\\d+(?:\\.\\d*)?|\\.\\d+)',
    // one of the literal values
    unit: '(?:em|ex|px|in|cm|mm|pt|pc|%)?',
}

var collectMatches = function( regex, str )
{
    var result = []

    var m = regex.exec( str )
    while( m !== null )
    {
        // This is necessary to avoid infinite loops with zero-width matches
        if( m.index === regex.lastIndex )
        {
            regex.lastIndex ++
        }

        // The result can be accessed through the `m`-variable.
        m.forEach(
        ( match, groupIndex ) =>
        {
            result.push( match )
        })

        m = regex.exec( str )
    }

    return result
}

export { collectMatches, MatchRegex }
