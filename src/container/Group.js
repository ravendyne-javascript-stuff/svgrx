/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { TransformableElement } from '../core/TransformableElement.js'

/**
 * https://www.w3.org/TR/SVG/struct.html#GElement
 * param - {} or Group
 */
function Group( param )
{
    //
    // Case when Group is called as a function
    //
    if( ! ( this instanceof Group ) )
    {
        return new Group( param )
    }

    TransformableElement.call( this, param )

    //
    // PRIVATE OBJECT PROPERTIES
    //

    //
    // PRIVATE FUNCTIONS
    //
    var setPropertiesFromAttributes = function( attributes )
    {
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
    })

    //
    // constructor code
    //
    if( param instanceof Group )
    {
        setPropertiesFromAttributes( this.attrs )
    }
    else if( typeof param === 'object' )
    {
        setPropertiesFromAttributes( this.attrs )
    }
}


//
// CLASS INHERITANCE
//
Group.prototype = Object.assign( Object.create( TransformableElement.prototype ),
{
    constructor: Group,
    isGroup: true,
})


//
// PUBLIC CLASS METHODS
//
Object.assign( Group.prototype,
{
    toString: function()
    {
        return `<Group ${this.id}>`
    },

    convert: function( matrix )
    {
        var result = []

        if( this.children.length > 0 )
        {
            var affineTransform = this.matrix.mul( matrix )

            for( let idx = 0; idx < this.children.length; idx++ )
            {
                let c = this.children[ idx ]
                let r = c.convert( affineTransform )
                result = result.concat( r )
            }
        }

        return result
    },
} )


export { Group }
