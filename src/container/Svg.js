/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { Point } from '../geometry/Point.js'
import { Matrix } from '../geometry/Matrix.js'

import { BaseElement } from '../core/BaseElement.js'

/**
 * https://www.w3.org/TR/SVG/struct.html#SVGElement
 */
function Svg( param, docText )
{
    //
    // Case when Svg is called as a function
    //
    if( ! ( this instanceof Svg ) )
    {
        return new Svg( param, docText )
    }

    BaseElement.call( this, param )

    //
    // PRIVATE OBJECT PROPERTIES
    //
    var _x = 0
    var _y = 0
    var _width = 1280
    var _height = 1024
    var _version = '1.1'

    var _docText = ''

    //
    // PRIVATE FUNCTIONS
    //
    var setPropertiesFromAttributes = function( attributes )
    {
        _x          = attributes['x']          || _x
        _y          = attributes['y']          || _y
        _width      = attributes['width']      || _width
        _height     = attributes['height']     || _height
        _version    = attributes['version']    || _version
    }

    //
    // PRIVATE OBJECT PROPERTIES ACCESSORS
    //
    Object.defineProperties(this,
    {
        x:
        {
            get: function() { return _x },
        },
        y:
        {
            get: function() { return _y },
        },
        width:
        {
            get: function() { return _width },
        },
        height:
        {
            get: function() { return _height },
        },
        version:
        {
            get: function() { return _version },
        },

        source:
        {
            get: function() { return _docText },
        },
    })

    //
    // constructor code
    //
    if( typeof docText === 'string' )
    {
        _docText = docText
    }
    else if( typeof param === 'object' )
    {
        setPropertiesFromAttributes( this.attrs )
    }

    //
    // initialize private property accessors here
    //
}


//
// INHERIT FROM CLASS WHICH INHERITS FROM OTHER CLASSES
//
// https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Object/create#Classical_inheritance_with_Object.create()
Svg.prototype = Object.assign( Object.create( BaseElement.prototype ),
{
    constructor: Svg,
    isSvg: true,
} )


//
// PUBLIC CLASS METHODS
//
// https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/Object/create#Using_propertiesObject_argument_with_Object.create()
Object.assign( Svg.prototype,
{
    toString: function()
    {
        return `<Svg ${this.id}>`
    },

    convert: function( matrix )
    {
        var result = []

        if( this.children.length > 0 )
        {
            let ctm = new Matrix()
            ctm = ctm.translate( this.x, this.y )
            ctm = ctm.mul( matrix )

            for( let idx = 0; idx < this.children.length; idx++ )
            {
                let c = this.children[ idx ]
                let r = c.convert( ctm )
                result = result.concat( r )
            }
        }

        return result
    },

    flatten: function( matrix )
    {
        var result = []
        var converted = this.convert( matrix )

        for( let idx = 0; idx < converted.length; idx++ )
        {
            var points = []
            let el = converted[ idx ]
            if( el instanceof SVGRX.Bezier2 )
            {
                // line
                points = [ el.P0, el.P1 ]
            }
            else if( el instanceof SVGRX.Bezier3 )
            {
                // quadratic
                points = SVGRX.flattenBezier3( el.P0.x, el.P0.y, el.P1.x, el.P1.y, el.P2.x, el.P2.y )
            }
            else if( el instanceof SVGRX.Bezier4 )
            {
                // cubic
                points = SVGRX.flattenBezier4( el.P0.x, el.P0.y, el.P1.x, el.P1.y, el.P2.x, el.P2.y, el.P3.x, el.P3.y )
            }

            result.push( points )
        }

        return result
    },
} )


var CreateFromString = function( content )
{
    var createObject = function( tagName, attrs )
    {

        switch( tagName )
        {
            // containers
            case 'g':
                return new SVGRX.Group( attrs )
            break

            case 'svg':
                return new SVGRX.Svg( attrs )
            break

            // graphics
            case 'circle':
                return new SVGRX.Circle( attrs )
            break

            case 'ellipse':
                return new SVGRX.Ellipse( attrs )
            break

            case 'line':
                return new SVGRX.Line( attrs )
            break

            case 'path':
                return new SVGRX.Path( attrs )
            break

            case 'polygon':
                return new SVGRX.Polygon( attrs )
            break

            case 'polyline':
                return new SVGRX.Polyline( attrs )
            break

            case 'rect':
                return new SVGRX.Rect( attrs )
            break

            default:
                return null
            break
        }
    }

    var getAttributes = function( child )
    {
        var attrs = {}

        if( child.hasAttributes() )
        {
            var attributes = child.attributes

            for(let k=0; k<attributes.length; k++)
            {
                attrs[attributes[k].name] = attributes[k].value
            }
        }

        return attrs
    }

    var parseChildren = function( children )
    {
        if( children.length == 0 ) return []

        var result = []

        for( let i = 0; i < children.length; i++ )
        {
            var child = children[i]
            var attrs = getAttributes( child )

            var object = createObject( child.tagName, attrs )

            if( object )
            {
                object.tag = child.tagName
                object.children = parseChildren( child.children )

                result.push( object )
            }
            else
            {
                console.log( 'unsupported tagName: ', child.tagName )
            }
        }

        return result
    }

    var createNodes = function( doc )
    {
        var parsed = parseChildren( doc.children )
        // valid SVG XMLDocument should have only one child (the root node)
        // and that child should be the <svg> element
        if( parsed.length > 0 )
        {
            return parsed[0]
        }

        return {}
    }

    var parser = new DOMParser()
    // returns a SVGDocument, which also is a Document.
    // https://developer.mozilla.org/en-US/docs/Web/API/Document
    var doc = parser.parseFromString(content, "image/svg+xml")
    // console.log(doc.children)

    if( doc instanceof XMLDocument )
    {
        var rootNode = createNodes( doc )
        return rootNode
    }

    return null
}

var CreateFromUrl = function( url )
{
    var result = new Promise(function(resolve, reject)
    {
        var xhr = new XMLHttpRequest()
        // true - async
        // false - sync
        xhr.open( 'GET', url, true )

        // If specified, responseType must be empty string or "document"
        // only can set this for async requests
        // xhr.responseType = 'document'

        // overrideMimeType() can be used to force the response to be parsed as XML
        // xhr.overrideMimeType( 'text/xml' )
        xhr.overrideMimeType( 'text/plain' )

        xhr.onload = function ()
        {
            if (xhr.readyState === xhr.DONE)
            {
                if (xhr.status === 200)
                {
                    var svgNode = CreateFromString( xhr.response )

                    if( svgNode instanceof SVGRX.Svg )
                    {
                        resolve( svgNode )
                    }
                }
            }

            reject( xhr )
        }

        xhr.send( null )
    })

    return result
}

export { Svg, CreateFromString, CreateFromUrl }
