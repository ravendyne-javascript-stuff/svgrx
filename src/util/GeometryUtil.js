/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { Point } from '../geometry/lib.js'

import { isPointArray, flattenArray } from './ArrayUtil.js'

var lengthInBaseUnits = function( v, mode = 'xy', viewport = Point())
{
    var numberRegex = /[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?/g
    var unitRegex =  /em|ex|px|in|cm|mm|pt|pc|%/g
    var unitConversions = {
        '': 1,            // Default unit (same as pixel)
        'px': 1,          // px: pixel. Default SVG unit
        'em': 10,         // 1 em = 10 px FIXME
        'ex': 5,          // 1 ex =  5 px FIXME
        'in': 96,         // 1 in = 96 px
        'cm': 96 / 2.54,  // 1 cm = 1/2.54 in
        'mm': 96 / 25.4,  // 1 mm = 1/25.4 in
        'pt': 96 / 72.0,  // 1 pt = 1/72 in
        'pc': 96 / 6.0,   // 1 pc = 1/6 in
        '%' :  1 / 100.0  // 1 percent
    }

    if( typeof v !== 'string' )
    {
        return v
    }

    var value = 0

    var match = numberRegex.exec( v )
    if( match === null )
    {
        return null
    }
    value = match[0]

    var unit = ''

    var match = unitRegex.exec( v )
    if( match !== null )
    {
        unit = match[0]
    }

    if( unit == '%' )
    {
        if( mode == 'x' )
        {
            return parseFloat( value ) * unitConversions[ unit ] * viewport.x
        }
        if( mode == 'y' )
        {
            return parseFloat( value ) * unitConversions[ unit ] * viewport.y
        }
        if( mode == 'xy' )
        {
            //???
        }
    }

    let result = parseFloat( value ) || 0
    result *= unitConversions[ unit ]

    return result
}

var pointArrayToNumeric = function( array )
{
    if( ! isPointArray( array ) )
    {
        return []
    }

    var coordPoints = array.map( function( pt ) { return [ pt.x, pt.y ] } )

    return flattenArray( coordPoints )
}

export { lengthInBaseUnits }
export { pointArrayToNumeric }
