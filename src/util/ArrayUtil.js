/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { Point } from '../geometry/Point.js'
import { TransformableElement } from '../core/TransformableElement.js'
import { BaseElement } from '../core/BaseElement.js'

var isBaseElementArray = function( array )
{
    if( array instanceof Array )
    {
        if( array.length == 0 )
        {
            return true
        }

        for( let idx = 0; idx < array.length; idx++ )
        {
            if( typeof array[idx] !== 'object'
                && ! ( array[idx] instanceof BaseElement ) )
            {
                return false
            }
        }

        return true
    }

    return false
}

var isNumericArray = function( array )
{
    if( array instanceof Array )
    {
        if( array.length == 0 )
        {
            return true
        }

        for( let idx = 0; idx < array.length; idx++ )
        {
            if( typeof array[idx] !== 'number'
                && ! ( array[idx] instanceof Number ) )
            {
                return false
            }
        }

        return true
    }

    return false
}

var isPointArray = function( array )
{
    if( array instanceof Array )
    {
        if( array.length == 0 )
        {
            return true
        }

        for( let idx = 0; idx < array.length; idx++ )
        {
            if( typeof array[idx] !== 'object'
                && ! ( array[idx] instanceof Point ) )
            {
                return false
            }
        }

        return true
    }

    return false
}

var isTransformableElementArray = function( array )
{
    if( array instanceof Array )
    {
        if( array.length == 0 )
        {
            return true
        }

        for( let idx = 0; idx < array.length; idx++ )
        {
            if( typeof array[idx] !== 'object'
                && ! ( array[idx] instanceof TransformableElement ) )
            {
                return false
            }
        }

        return true
    }

    return false
}

// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/Reduce?v=example
const flattenArray = function( array )
{
    return array.reduce( function ( acc, val )
        {
            return acc.concat( Array.isArray( val ) ? flattenArray( val ) : val )
        },
        []
    )
}

export { isNumericArray }
export { isPointArray }
export { isTransformableElementArray }
export { isBaseElementArray }
export { flattenArray }
