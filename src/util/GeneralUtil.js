/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

// https://tools.ietf.org/html/rfc4122
// http://www.boost.org/doc/libs/1_47_0/boost/uuid/uuid.hpp
// https://en.wikipedia.org/wiki/Universally_unique_identifier
var GUID = function()
{
    var quad = function( zeroMask = 0xFFFF, oneMask = 0x0000 )
    {
        // Get random value between 0x10000 and 0x20000.
        // This will ensure that we have leading zeroes later on
        // when we do conversion to hex string
        var randomValue = Math.floor( ( 1 + Math.random() ) * 0x10000 )
        randomValue = randomValue & ( 0xF0000 | zeroMask ) | oneMask

        return ''+
            randomValue
            // convert to hex string ( base == 16 ),
            // this string will be 5 characters long
            // since generated value is between 0x10000 and 0x20000.
            .toString( 16 )
            // strip leading character so we are left
            // with 4-character string
            .substring( 1 )
    }

    return '' +
        // time_low
        quad() + quad() + '-' +
        // time_mid
        quad() + '-' +
        // time_hi
        // bits 12-15 to 4-bit verion (v4)
        quad( 0x0FFF, 0x4000 ) + '-' +
        // clock_seq
        // bits 6-7 to 01
        quad( 0x3FFF, 0x8000 ) + '-' +
        // node
        quad() + quad() + quad()
}

export { GUID }
