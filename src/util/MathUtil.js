/**
 * @author zagortenej / https://gitlab.com/zagortenej
 */

import { Point } from '../geometry/Point.js'
import { isPointArray } from './ArrayUtil.js'

/**
 * Rounds numbers to twice the number of decimals specified
 * in Point.precision and rounds to 0 numbers that are
 * smaller than +/- 10^(-Point.precision).
 */
var roundToPrecision = function( value )
{
    // we only handle near zero values
    var epsilon = Math.pow( 10, - Point.precision)
    if( - epsilon < value && value < epsilon )
        return 0

    // return value
    return Number( value.toFixed( Point.precision * 2 ) )
}

var minX = function( array )
{
    if( ! isPointArray( array ) )
    {
        return 0
    }

    return array.reduce(function( acc, val, idx )
    {
        if( val.x < acc ) return val.x
        return acc
    }, 0)
}

var minY = function( array )
{
    if( ! isPointArray( array ) )
    {
        return 0
    }

    return array.reduce(function( acc, val, idx )
    {
        if( val.y < acc ) return val.y
        return acc
    }, 0)
}

var maxX = function( array )
{
    if( ! isPointArray( array ) )
    {
        return 0
    }

    return array.reduce(function( acc, val, idx )
    {
        if( val.x > acc ) return val.x
        return acc
    }, 0)
}

var maxY = function( array )
{
    if( ! isPointArray( array ) )
    {
        return 0
    }

    return array.reduce(function( acc, val, idx )
    {
        if( val.y > acc ) return val.y
        return acc
    }, 0)
}

export { roundToPrecision, }
export { minX, minY, maxX, maxY }
